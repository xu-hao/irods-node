# iRODS Node.js client #

## integration with CI ##

Run tests:

```
#!bash
mocha --compilers ls:LiveScript -t 60000 test/_test_l.ls
```

Code coverage:

```
lsc -c src test
./node_modules/.bin/istanbul cover _mocha -- -t 120000 test/_test_l.js
```

## Use it from Node.js ##
### setting up ###
1. Install nodejs
2. Install livesript
3. Modify `config.js`

### hello world ###
Save the following file in the top level src directory as `helloworld.ls`
```
#!livescript
{runCont, bind, ret} = require "./plugins/monad_l.ls"
{model} = require "./plugins/model_l.ls"
trans = (require "./plugins/transport/tcp_l.ls").plugin
prot = (require "./plugins/protocol/xml_l.ls").plugin
auth = (require "./plugins/auth/basic-auth_l.ls").plugin
{rodsEnv, rodsCred} = require "./config.js"

mod = model prot, trans, rodsEnv, auth, rodsCred

dataObj = mod.object {
    type: "DataObject"
    ref: "irods:/#{rodsEnv.clientRcatZone}/home/#{rodsEnv.clientUser}/helloworld"
    content: new mod.BufferContent new Buffer "HelloWorld"
}

cont = (x) -> console.log "file created"
(runCont dataObj.create()) cont
```
Then run
```
lsc helloworld.ls
```
### effectful vs effectless functions ###

We use a monadic programming style, where we distinguish between effectful functions and effectless functions.

To run effectful functions, we first construct an effectful program, and then run it. We use the following syntax to construct an effectful program:

1.Calling an effectful function is itself an effectful program for example, the `readProp` function is effectful because it may require network I/O in iRODS:
```
#!livescript
object1.readProp "p"
```

2.Effectless code can always be embedded into effectful code using the ```ret``` function. For example, if we have a string `x`, we can "lift" it into an effect program
```
ret x
```
It simply returns `x`.

3.Effectful programs can be sequentially composed using `bind` and `<-`: Given effectful program p1 and p2, they can be composed as follows:
```
#!livescript
x <- bind p1
p2
```
where ```x``` stores the return value of ```p1``` when it succeeds, and is avaible in ```p2```

For example,
```
#!livescript
x <- bind object1.readProp "p"
ret x
```
(This is in fact equivalent to `object1.readProp "p"`)

There may be multiple such effectful operations in the program.
```
#!livescript
x <- bind object1.readProp "p"
_ <- bind object1.updateProp "p", {oval: x, nval: "new val"}
ret x
```
We can also write functions that construct effect programs.
```
#!livescript
update = (object1, p, newval) ->
    x <- bind object1.readProp p
    _ <- bind object1.updateProp p, {oval: x, nval: newval}
    ret x
update object1, "p", "new val"
```

To run an effectful program,
```
#!livescript
(runCont (...)) (x) -> ...
```
where the first `...` is the effectful program and the second `...` is regular code that is executed after the effect program is executed.
For example,
```
#!livescript
p = (
    x <- bind object1.readProp "p"
    _ <- bind object1.updateProp "p", {oval: x, nval: "new val"}
    ret x
)
(runCont p) (x) -> console.log x
```


Effectless functions are just regular code.

### Representing entities ###
iRODS concepts such as local file, local directory, data object, collection, and AVU are represented by objects. Operations on objects are mapped to operates on iRODS entities. There are two types of objects:

* Reference objects refer to an irods entity by a unique identifier, such as path. However this identifier may change over time.

* Value objects refer to an irods entity by its value properties, such as AVU.

Referenced objects can be constructed by calling the `model.object` function. We must specify a type and a ref property (the identifier).

```
#!livescript
object = model.object {
    type: "DataObject"
    ref: "irods:/tempZone/home/rods/test1"
}
```
If the object type can be `File`, `Directory`, `DataObject`, `Collection`, `CollectionAVU`, `DataObjectAVU`, `AdaptiveObject`, or `PathObject`.
(`PathObject` let the system determine the object type of an existing object, whereas `AdaptiveObject` let the system determine the object type of a non-existing object.)

Value objects can be constructed in a similar manner but by specifying the values properties.
```
#!livescript
object = model.object {
    type: "DataObjectAVU"
    attribute: "a"
    value: "v"
    unit: "u"
}
```


### Creating entities ###
To create an standalone entity, simply call the create function
```
#!livescript
buf = new Buffer "HelloWorld"
object = model.object {
    type: "DataObject"
    ref: "irods:/tempZone/home/rods/test1"
    content: new BufferContent buf
}
object.create()
```
One can create multiple objects at the same time
```
#!livescript
buf = new Buffer "HelloWorld"
object = model.object {
    type: "Collection"
    ref: "irods:/tempZone/home/rods"
    content: [
        model.object {
            type: "DataObject"
            ref: "irods:/tempZone/home/rods/test1"
            content: new BufferContent buf
        }
    ]
}
object.create()
```
To create an entity that is attached to another entity such as AVU, one use the updateProp function on the attached-to entity
```
#!livescript
avu = model.object {
    type: "CollectionAVU"
    attribute: "collectionId"
    value: "rods"
    unit: "irods:collectionId"
}
object = model.object {
    type: "Collection"
    ref: "irods:/tempZone/home/rods"
}
object.updateProp "metadata", {oval: none, nval: avu}
```

where `oval` and `nval` are the old value and the new value, respectively. Both properties can be any value, here `oval: none` indicates that we don't want to modify any value, we just want to add `nval`.

### Reading properties ###
Properties of the entities can be read using the readProp function on the object.

```
#!livescript
name <- bind object.readProp "name"  
...
```
or

```
#!livescript
avuContent <- bind object.readProp "metadata"  
...
```
Here avuContent will be an object that contains an array of AVU objects. To retrieve the array, one can call
```
#!livescript
array <- bind avuContent.getContent()
...
```
Similarly for subcollections and data objects within a collection,
```
#!livescript
subContent <- bind object.readProp "contents"  
array <- bind sub.getContent()
...
```
A caching mechanism is implemented such that after retrieving a property, subsequent reads are always the cached value. The cache can be cleared by setting the property to `null`
```
#!livescript
avuContent <- bind object.readProp "metadata"
avuContentCached <- bind object.readProp "metadata" # always the same as the first one
object.metadata = null
avuContentNew <- bind object.readProp "metadata" # may be new
...
```

Properties can also be queried where it is supported. For example, one can query a subset of AVUs or data objects by passing in a pattern object:
```
#!livescript
avuContent <- bind object.readProp2 "metadata", {
    attribute: {
        o: "="
        v: "a"
    }
}
...
```
or
```
#!livescript
subContent <- bind object.readProp2 "content", {
    name: {
        o: "="
        v: "test1"
    }
}  
...
```
### Updating properties ###
To update a property, one call the update function on the representation object of the entity the property of which one want to update, passing in a diff object

```
#!livescript
avu1 = model.object {
    type: "DataObjectAVU"
    attribute: "a"
    value: "b"
    unit: "c"
}
avu2 = model.object {
    type: "DataObjectAVU"
    attribute: "a2"
    value: "b2"
    unit: "c2"
}
object.updateProp "metadata", {oval: avu1, nval: avu2}
```
We can also package updates to multiple properties in one diff
```
#!livescript
avu1 = model.object {
    type: "DataObjectAVU"
    attribute: "a"
    value: "b"
    unit: "c"
}
avu2 = model.object {
    type: "DataObjectAVU"
    attribute: "a2"
    value: "b2"
    unit: "c2"
}
diff = new StructuralDiff {
    ref: all
    metadata: avu1
}, {
    ref: "irods:/tempZone/home/rods/test2"
    metadata: avu2
}
object.update diff
```
Here, `all` indicates that we want to update the old value, whatever it is.

### Deleting entities ###
For standalone entities, simply call the delete function on the object one wants to delete
```
#!livescript
object.delete()
```
For attached entities, one use `update` similar to creating entities, by setting `nval` to `none`

```
#!livescript
avu = model.object {
    type: "CollectionAVU"
    attribute: "collectionId"
    value: "rods"
    unit: "irods:collectionId"
}
object = model.object {
    type: "Collection"
    ref: "irods:/tempZone/home/rods"
}
object.updateProp "metadata", {oval: avu, nval: none}
```
### Copying entities ###
Standalone entities can be copied using the `update` if target already exists or `create` if target does not exist. For example, given data object `object1`, copy it to new data object `irods:/tempZone/home/rods/test2`
```
#!livescript
cnt <- bind object1.readProp "content"
object2 = model.object {
    type: "DataObject"
    ref: "irods:/tempZone/home/rods/test2"
    content: cnt
}
object2.create
```
Given data object `object1`, copy it to existing data object `irods:/tempZone/home/rods/test2`
```
#!livescript
cnt <- bind object1.readProp "content"
object2 = model.object {
    type: "DataObject"
    ref: "irods:/tempZone/home/rods/test2"
}
object2.updateProp "content", {oval: all, nval: cnt}
```
Similarly, we can copy metadata,
```
#!livescript
meta <- bind object1.readProp "metadata"
object2 = model.object {
    type: "DataObject"
    ref: "irods:/tempZone/home/rods/test2"
}
object2.updateProp "metadata", {oval: none, nval: meta}
```
### Moving entities ###
Moving entities can be done by calling the ```move``` function:

```
#!livescript
move: (src, src_prop, src_val, tgt, tgt_prop, tgt_val) -> ...
```
```tgt_val``` can be set to ```null```.

It moves ```src_val``` of ```src```'s ```src_prop``` to ```tgt```'s ```tgt_prop```. Currently only AVU, File, Directory, DataObject, and Collection can be moved.

### Catching errors

The `catchm` function can be used to construct an effectful expression that catches errors
```
#!livescript
(catchm (...)) (e) -> ...
```
If the execution of the first `...` throws an error then the second `...` is executed.

## use it from command line ##

```
lsc inode_l.ls (create|read|update|delete|copy|move) arg
```
where `arg` is given by the following object syntax:

An object is given a by a set of properties. A property is given by a name an a value, the value itself can also be an object. Property name starts with a number of ~ matching the nested level.

Create coll1
```
create ~ref irods:/tempZone/home/rods/coll1
```
List /tempZone/home/rods/test1
```
read ~ref irods:/tempZone/home/rods/test1
```
List content of coll
```
read ~ref irods:/tempZone/home/rods/coll ~content
```
List metadata of test1
```
read ~ref irods:/tempZone/home/rods/test1 ~metadata
```
List content and metadata of coll
```
read ~ref irods:/tempZone/home/rods/coll ~content ~metadata
```
Update metadata
```
update ~from ~~ref irods:/tempZone/home/rods/test1 ~~metadata ~~~attribute a ~~~value v ~~~unit u ~to ~~metadata ~~~attribute a2 ~~~value v2 ~~~unit u2
```
Rename test1 to test2
```
update ~from ~~ref irods:/tempZone/home/rods/test1 ~to ~~ref irods:/tempZone/home/rods/test2
```
Delete test1
```
delete ~ref irods:/tempZone/home/rods/test1
```
Copy test1 to test2 metadata is also copied
```
copy ~from irods:/tempZone/home/rods/test1 ~to irods:/tempZone/home/rods/test2
```
Move metadata of test1 to test2
```
move ~from ~~ref irods:/tempZone/home/rods/test1 ~~metadata ~to ~~ref irods:/tempZone/home/rods/test2 ~~metadata
```
Move test1 to coll
```
move ~from ~~ref irods:/tempZone/home/rods ~~content ~~~ref = irods:/tempZone/home/rods/test1 ~to ~~ref irods:/tempZone/home/rods/coll ~~content
```
Move metadata of test1 whose attribute = a to test2
```
move ~from ~~ref irods:/tempZone/home/rods/test1 ~~metadata ~~~attribute = a ~to ~~ref irods:/tempZone/home/rods/test2 ~~metadata
```

## use it from browser ##

# theory

System states are modeled by a labeled directed graph, where nodes are entities or primitive values. Each group of outgoing edge with the same label is a property. For example, the node representing entity "data object A" may have out going edges "ref", "metadata", etc.

There are 6 basic operations: exists, create, read, update, delete, and move.

We first develop a theory without arrays and value objects. For primitive value cannot be created or deleted.
```
exists: ref -> M bool
create: ref -> M ()
read: ref * prop -> M ref
update: ref * prop * (ref | ⊥) -> M ()
delete: ref -> M ()
```
A structural object simply delegates these operations on its properties. And a structural property simply delegates these operations on its targets. An array property simply delegates these operations on each of its edges.

For each type of entity, one can define `exists`, `create`, `read`, `update`, and `delete`.

* `create`: If the obj already exists, then error.
* `read`: If the obj doesn't exist, then error. Returns ⊥ for undefined properties.
* `update`: If the obj doesn't exist, then error. From ⊥ to non-⊥ creates a property, from non-⊥ to ⊥ deletes a property. If the first value in the diff doesn't match the current value, then error.
* `delete`: If the obj doesn't exist, then error.

For each pair of entities, one can define `move`.

`move` must satisfy
```
move (o1, p1, o2, p2) ==
read (o1, p1) >>=  λ x. update (o2, p2, x) >> update (o1, p1, ⊥)
```

http://groups.inf.ed.ac.uk/bx/effectful-tr.pdf

Therefore, we don't consider move from now on.
These operations satisfy
```
f = create r >> exists r =>               f == f >> ret true
f = create r >> read (r, p) =>            f == f >> ret ⊥
f = update (r1, p, r2) >> read (r1, p) => f == f >> ret r2
```
