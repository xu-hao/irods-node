{parse: toObj, serialize: toCmd} = require "../src/plugins/cmd_l"
should = require "should"

Object.defineProperty Object.prototype, "which", {
    value: -> this
}

cmd = "cmd"
prop1 = "prop1"
prop2 = "prop2"
val = "val"

eqn = (x, y = x) -> (toObj toCmd x).should.eql y

its = it

describe "cmd", ->
    its "cmd1", -> eqn {
        cmd: cmd
        prop1: val
    }

    its "cmd2", -> eqn {
        cmd: cmd
        prop1: val
    }, {
        prop1: val
        cmd: cmd
    }

    its "cmd3", -> eqn {
        cmd: cmd
        prop1:
            prop2: val
    }

    its "cmd4", -> eqn {
        cmd: cmd
        prop1:
            o: "="
            v: val
    }

    its "cmd5", -> eqn {
        cmd: cmd
        prop1: {}
        prop2: {}
    }

    its "cmd6", -> eqn {
        prop1: {}
        prop2: {}
    }
