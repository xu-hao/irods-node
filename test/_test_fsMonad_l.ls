f = require "fs"
fs = require "../src/plugins/fsMonad_l"
{runCont, bind, ret, throwm, catchm, Exception} = require "../src/plugins/monad_l"
{assert,assertEqual,testIt, testErrorIt} = require "./_test_framework_l"


testDir = "/tmp/testDir3"
testFile = "testFile"
buf = new Buffer "HelloWorld"

its = it

# runTests ->
describe "fsMonad", ->
    if ! f.existsSync "/tmp"
        f.mkdirSync "/tmp"

    its "testing mkdir", testIt ->
        _ <- bind fs.mkdir testDir
        stat <- bind fs.stat testDir
        assert stat.isDirectory()

    its "testing cannot mkdir twice", testErrorIt ->
        fs.mkdir testDir

    its "testing rmdir", testIt ->
        _ <- bind fs.rmdir testDir
        assert ! f.existsSync testDir

    its "testing cannot rmdir nonexisting", testErrorIt ->
        fs.rmdir testDir

    its "testing cannot list nonexisting dir", testErrorIt ->
        fs.readdir testDir

    its "testing mkdir again", testIt ->
        _ <- bind fs.mkdir testDir
        stat <- bind fs.stat testDir
        assert stat.isDirectory()
        
    its "cannot delete nonexisting file", testErrorIt ->
        fs.unlink "#testDir/#testFile"
        
    its "cannot rename nonexisting file", testErrorIt ->
        fs.rename "#testDir/#testFile", "#testDir/#testFile"
    
    its "create file", testIt ->
        fs.create "#testDir/#testFile"
    
    its "cannot create file again", testErrorIt ->
        fs.create "#testDir/#testFile"
    
    its "delete file", testIt ->
        fs.unlink "#testDir/#testFile"
        
    its "testing write and read file", testIt ->
        _ <- bind fs.writeFile "#testDir/#testFile", buf
        bs <- bind fs.readFile "#testDir/#testFile"
        assertEqual buf.toString(), bs.toString()

    its "testing readdir", testIt ->
        files <- bind fs.readdir testDir
        _ <- bind assertEqual files.length, 1
        assertEqual files[0], testFile

    its "testing cannot rmdir nonempty", testErrorIt ->
        fs.rmdir testDir

    its "testing delete file", testIt ->
        _ <- bind fs.unlink "#testDir/#testFile"
        assert ! f.existsSync "#testDir/#testFile"

    its "cleaning up", testIt ->
        _ <- bind fs.rmdir testDir
        assert ! f.existsSync testDir
