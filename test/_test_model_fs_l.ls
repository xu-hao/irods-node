/* generic model tests */

trans = (require "../src/plugins/transport/tcp_l").plugin
prot = (require "../src/plugins/protocol/xml_l").plugin
{rodsEnv, rodsCred} = require "../src/config.js"
auth = (require "../src/plugins/auth/basic-auth_l").plugin
{runCont, bind, ret, throwm, catchm, Exception, newStack} = require "../src/plugins/monad_l"
{ObjType} = require "../src/plugins/const_l"
{log, assert,assertEqual,testIt,testErrorIt} = require "./_test_framework_l"
{model, BufferContent, RangeContent} = require "../src/plugins/model_l"
{ArrayBackedContent, none, all, PropDiff} = require "../src/plugins/metamodel_l"
{rec, mapM, transpose} = require "../src/plugins/arrays_l"
Error.stackTraceLimit = Infinity
mod = model prot, trans, rodsEnv, auth, rodsCred

testDir = "file:///tmp/testDirZ"
testFile = "testFile"
testColl = "irods:///tempZone/home/rods/testDirZ"
testData = "testFile"

dirObj0 = mod.object {
    type: "Directory"
    ref: "file:///tmp"
}

dirObj = mod.object {
    type: "Directory"
    ref: testDir
}

dirObj2 = mod.object {
    type: "Directory"
    ref: "#testDir%2"
}

dirObj3 = mod.object {
    type: "Directory"
    ref: "#testDir%2/testDirZ"
}

fileObj = mod.object {
    type: "File"
    ref: "#testDir/#testFile"
}

fileObj2 = mod.object {
    type: "File"
    ref: "#testDir/#testFile@2"
}

fileObj3 = mod.object {
    type: "File"
    ref: "#testDir%2/#testFile"
}

collectionObj0 = mod.object {
    type: "Directory"
    ref: "irods:///tempZone/home/rods"
}

collectionObj = mod.object {
    type: "Collection"
    ref: testColl
}

collectionObj2 = mod.object {
    type: "Collection"
    ref: "#testColl%2"
}

collectionObj3 = mod.object {
    type: "Collection"
    ref: "#testColl%2/testDirZ"
}

dataObjectObj = mod.object {
    type: "DataObject"
    ref: "#testColl/#testData"
}

adaptive = (obj) -> mod.object ({} <<< obj <<< {
    type: "AdaptiveObject"
})

dataObjectObj2 = mod.object {
    type: "DataObject"
    ref: "#testColl/#testData@2"
}

dataObjectObj3 = mod.object {
    type: "DataObject"
    ref: "#testColl%2/#testData"
}

resc = mod.object {
    type: "Resource"
    rescName: "testResc"
    rescType: "unix file system"
    rescHost: "localhost"
    rescPath: "/tmp/testResc"
}
            
resc2 = mod.object {
    type: "Resource"
    rescName: "testResc"
}

replica = mod.object {
    type: "Replica"
    path: "/tempZone/home/rods/testDirZ/#testData"
    rescName: "testResc"
}
        
user = mod.object {
    type: "User"
    ref: "t\#tempZone"
    userType: "rodsuser"
}

avuObj = mod.object {
    type: "DataObjectAVU"
    attribute: "att"
    value: "val"
    unit: "uni"
}

avuObj2 = mod.object {
    type: "DataObjectAVU"
    attribute: "att2"
    value: "val2"
    unit: "uni2"
}

cavu = mod.object {
    type: "CollectionAVU"
    attribute: "a"
    value: "v"
    unit: "u"
}

gen = (fileObj) -> [mod.object {
    type: fileObj.type
    ref: fileObj.ref + "#i"
    content: new mod.BufferContent buf
} for i from 1 to 30]

genavu = (avu) -> [mod.object {
    type: avu.type
    attribute: "attr #i"
    value: "val #i"
    unit: "unit #i"
} for i from 1 to 1000]

objList = (gen fileObj) ++ (gen dataObjectObj) ++ [ 
    dataObjectObj
    dataObjectObj2
    dataObjectObj3
    collectionObj3
    collectionObj
    collectionObj2
    fileObj
    fileObj2
    fileObj3
    dirObj3
    dirObj
    dirObj2
    resc
]

cleanup =
    rec objList, (o) ->
        ext <- bind o.exists
        if ext
            log "cleaning up #{o.ref}"
            o.del
        else
            ret null
            
buf = new Buffer "HelloWorld"
            
create = (objs) ->
    rec objs, (obj) ->
        log "creating #{obj.ref}"
        ext <- bind obj.exists
        if ext
            ret null
        else
            obj.create
clone = (obj) ->
    mod.object ({} <<< obj)
        

its = it
describe "model", ->


    beforeEach testIt ->
        cleanup
        
    assertProp = (obj, prop, bx) ->
        obj[prop] = null
        cnt <- bind obj.readProp prop
        _ <- bind assert cnt?
        ax <- bind if cnt?.instanceOf? and cnt.instanceOf "Content" or cnt.type == "ArrayBackedContent"
            cnt.getContent
        else
            ret cnt
        assertEqualDeep ax, bx
    
    assertEqualDeep = (a, b) ->
        log "vvv"
        log JSON.stringify a
        log JSON.stringify b
        log "^^^"
        if Array.isArray a and Array.isArray b
            _ <- bind assertEqual a.length, b.length
            if a.length > 0 and b.length > 0
                if a[0].ref? and b[0].ref # try sorting by ref first
                    cmp = (a,b) -> a.ref.localeCompare b.ref
                    a.sort cmp
                    b.sort cmp
                if a[0].attribute? and b[0].attribute # then try sorting by attribute first
                    cmp = (a,b) -> a.attribute.localeCompare b.attribute
                    a.sort cmp
                    b.sort cmp
                
                
            rec (transpose [a, b]), ([ax, bx]) ->
                newStack assertEqualDeep ax, bx
        else if not Array.isArray a and not Array.isArray b and a instanceof Object and b instanceof Object
            _ <- assert a.readProp?
            rec [prop2 for own prop2, val of b], (prop2) ->
                cnt <- bind a.readProp prop2
                ax <- bind if cnt?.instanceOf? and cnt.instanceOf "Content"
                    cnt.getContent
                else
                    ret cnt
                newStack assertEqualDeep ax, b[prop2]
        else
            if a == "CollectionAVU" and b == "DataObjectAVU" or a == "DataObjectAVU" and b == "CollectionAVU"
                throwm {error: -1, errormsg: "wrong avu type"}
            else
                assertEqual a, b
            

    runTests = (name, dirObj, fileObj) ->
        dirObj = clone dirObj
        fileObj = clone fileObj
        describe "###### #name ######", ->
            its "testing mkdir", testIt ->
                _ <- bind dirObj.create
                type = dirObj.type
                dirObj.type = "PathObject"
                log "check if dir #{dirObj.ref} exists"
                ext <- bind dirObj.exists
                assert ext && dirObj.type == type

            its "testing cannot mkdir twice", testErrorIt ->
                _ <- bind dirObj.create
                dirObj.create

            its "testing rmdir", testIt ->
                _ <- bind dirObj.create
                _ <- bind dirObj.del
                ext <- bind dirObj.exists
                assert !ext

            its "testing cannot rmdir nonexisting", testErrorIt ->
                dirObj.del
                
            its "testing write and read file", testIt ->
                _ <- bind dirObj.create
                fileObj.content = new mod.BufferContent buf
                _ <- bind fileObj.create
                fileObj.content = null
                cnt <- bind fileObj.readProp "content"
                bs <- bind cnt.readFully
                assertEqual buf.toString(), bs.toString()
                
            its "partial read", testIt ->
                _ <- bind dirObj.create
                fileObj.content = new mod.BufferContent buf
                _ <- bind fileObj.create
                fileObj.content = null
                cnt <- bind fileObj.readProp2 "content", {start: 1, end: 5}
                bs <- bind cnt.readFully
                # console.log bs
                assertEqual (buf.slice 1, 5).toString(), bs.toString()
            
            its "can read out of range", testIt ->
                _ <- bind dirObj.create
                fileObj.content = new mod.BufferContent buf
                _ <- bind fileObj.create
                fileObj.content = null
                fileObj.readProp2 "content", {start: 1, end: 10000}
                

            its "partial write by copy", testIt ->
                _ <- bind dirObj.create
                fileObj.content = new mod.BufferContent buf
                _ <- bind fileObj.create
                
                buf2 = new Buffer "ELLO"
                bb = new mod.BufferContent buf2
                _ <- bind mod.copy null, null, bb, fileObj, "content", {start: 1, end: 5}
                
                fileObj.content = null
                cnt <- bind fileObj.readProp2 "content", {start: 1, end: 5}
                bs <- bind cnt.readFully
                assertEqual buf2.toString(), bs.toString()

            its "partial write by update", testIt ->
                _ <- bind dirObj.create
                fileObj.content = new mod.BufferContent buf
                _ <- bind fileObj.create
                
                buf2 = new Buffer "ELLO"
                bb = new mod.BufferContent buf2
                cnt <- bind fileObj.readProp2 "content", {start: 1, end: 5}
                _ <- bind fileObj.updateProp "content", new PropDiff cnt, bb 
                
                fileObj.content = null
                cnt <- bind fileObj.readProp2 "content", {start: 1, end: 5}
                bs <- bind cnt.readFully
                assertEqual buf2.toString(), bs.toString()
                
            its "testing cannot rmdir nonempty", testErrorIt ->
                _ <- bind dirObj.create
                fileObj.content = new mod.BufferContent buf
                _ <- bind fileObj.create
                dirObj.del

            its "testing delete file", testIt ->
                _ <- bind dirObj.create
                fileObj.content = new mod.BufferContent buf
                _ <- bind fileObj.create
                fileObj.content = null
                cnt <- bind fileObj.readProp "content"
                bs <- bind cnt.readFully
                _ <- bind fileObj.del
                ext <- bind fileObj.exists
                assert ! ext

            its "testing delete immediately after create file", testIt ->
                _ <- bind dirObj.create
                fileObj.content = new mod.BufferContent buf
                _ <- bind fileObj.create
                fileObj.del

            its "testing create file and dir at the same time and immediate delete", testIt ->
                fileObj.content = new mod.BufferContent buf # content has to go before metadata so that file gets created first
                dirObj.content = new ArrayBackedContent [fileObj]
                _ <- bind dirObj.create
                _ <- bind fileObj.del
                dirObj.del

            its "testing mkdir and create file at the same time", testIt ->
                fileObj.content = new mod.BufferContent buf
                dirObj.content = new ArrayBackedContent [fileObj]
                log "creating dir content"
                _ <- bind dirObj.create
                type = dirObj.type
                dirObj.type = "PathObject"
                log "reading dir content"
                ext <- bind dirObj.exists
                _ <- bind assert (ext && dirObj.type == type)
                log "reading file content"
                fileObj.content = null
                ext <- bind fileObj.exists
                _ <- bind assert ext
                cnt <- bind fileObj.readProp "content"
                bs <- bind cnt.readFully
                assertEqual buf.toString(), bs.toString()

            its "can open more than 10 files", testIt ->
                dobjs = gen fileObj
                dirObj.content = dobjs
                log "creating objects"
                _ <- bind create [dirObj]
                _ <- bind rec dobjs, (o) -> 
                    log "reading from " + o.ref
                    cnt <- bind o.readProp2 "content", {start:0, end:1}
                    cnt.readFully
                _ <- bind rec dobjs.reverse(), (o) -> 
                    log "reading from " + o.ref
                    cnt <- bind o.readProp2 "content", {start:0, end:1}
                    cnt.readFully
                _ <- bind rec dobjs, (o) -> 
                    log "reading from " + o.ref
                    cnt <- bind o.readProp2 "content", {start:0, end:1}
                    cnt.read 0, 1
                rec dobjs, (o) ->
                    o.del
                    
            its "files are closed properly when deleted", testIt ->
                dirObj.content = null
                fileObj.content = new mod.BufferContent buf
                _ <- bind create [dirObj, fileObj]
                _ <- bind fileObj.del
                mod.cleanup # if not closed properly this will error out
            

            its "testing create file with no content", testIt ->
                dirObj.content = null
                delete fileObj.content
                _ <- bind create [dirObj, fileObj]
                cnt <- bind fileObj.readProp "content"
                buf <- bind cnt.readFully
                assertEqual buf.length, 0

            its "testing can read 0 bytes", testIt ->
                dirObj.content = null
                delete fileObj.content
                _ <- bind create [dirObj, fileObj]
                cnt <- bind fileObj.readProp "content"
                buf <- bind cnt.readFully
                assert (buf != null)
                
            its "testing can read 0 bytes", testIt ->
                dirObj.content = null
                delete fileObj.content
                _ <- bind create [dirObj, fileObj]
                cnt <- bind fileObj.readProp2 "content", {start:0, end:0}
                buf <- bind cnt.readFully
                assert (buf != null)
                
    runTests "fs" dirObj, fileObj

    runTests "irods", collectionObj, dataObjectObj

    runTests2 = (sObj0, ssObj0, tObj0, ttObj0) ->
        its "copy file by create", testIt ->
            dirObj = clone sObj0
            fileObj = clone ssObj0
            collectionObj = clone tObj0
            dataObjectObj = clone ttObj0
            fileObj.content = new mod.BufferContent buf
            ext <- bind fileObj.exists
            assertEqual ext, false
            _ <- bind create [dirObj, collectionObj, fileObj]
            
            delete fileObj.content
            cnt <- bind fileObj.readProp "content"
            dataObjectObj.content = cnt
            log "creating target object from source object"
            ext <- bind dataObjectObj.exists
            assertEqual ext, false
            _ <- bind dataObjectObj.create
            log "reading content from target object"
            dataObjectObj.content = null
            cnt2 <- bind dataObjectObj.readProp "content"
            bs <- bind cnt2.readFully
            assertEqual buf.toString(), bs.toString()

    runTests3 = (sObj0, ssObj0, tObj0) ->
        its "copy dir by create", testIt ->
            sObj = clone sObj0
            ssObj = clone ssObj0
            tObj = clone tObj0
            ssObj.content = new mod.BufferContent buf
            _ <- bind create [sObj, ssObj]
            
            delete sObj.content
            cnt <- bind sObj.readProp "content"
            tObj.content = cnt
            log "creating target object from source object"
            _ <- bind tObj.create
            
            log "reading content from target directory"
            delete tObj.content
            cnt2 <- bind tObj.readProp "content"
            
            log "read content"
            arr <- bind cnt2.getContent
            _ <- bind assertEqual arr.length, 1
            
            f = arr[0]
            log "reading content from target file"
            cntTT <- bind f.readProp "content"
            bs <- bind cntTT.readFully
            assertEqual buf.toString(), bs.toString()
            
            
    its "lookupOperRule", testIt ->
        try
            r = mod.lookupOperRule "transfer", ["File", "DataObject"]
            assert false
        catch e
            assert true
        
    runTests4 = (sCol0, sObj0, tCol0, tObj0, id = "") -> 
        its "copy file/dir by copy to sink #id", testIt ->
            sObj = clone sObj0
            sCol = clone sCol0
            tObj = clone tObj0
            tCol = clone tCol0
            _ <- bind create [sCol, sObj, tCol]
            _ <- bind mod.copy null, null, {
                type: if sCol.type == "Directory" then "DirectoryContent" else "CollectionContent"
                path: do
                    {path: spath} <- bind sCol.readProp "refobj"
                    ret spath
                getContent: ret [sObj]
            }, tCol, "content", null
            ext <- bind tObj.exists
            assert ext

    runTests5 = (sCol0, sObj0, tCol0, tObj0, id = "") -> 
        its "move file/dir #id", testIt ->
            sObj = clone sObj0
            sCol = clone sCol0
            tObj = clone tObj0
            tCol = clone tCol0
            _ <- bind create [sCol, sObj, tCol]
            _ <- bind mod.move sCol, "content", sObj, tCol, "content", tObj
            exts <- bind sObj.exists
            _ <- bind assert !exts
            extt <- bind tObj.exists
            assert extt
            
    describe "movers and shakers", ->
        describe "from local to remote", ->
            runTests2 dirObj, fileObj, collectionObj, dataObjectObj
            runTests3 dirObj, fileObj, collectionObj, dataObjectObj
            runTests4 dirObj, fileObj, collectionObj, dataObjectObj
            runTests4 dirObj0, dirObj, collectionObj0, collectionObj
            runTests5 dirObj, fileObj, collectionObj, dataObjectObj
            runTests5 dirObj0, dirObj, collectionObj0, collectionObj
            
        describe "from remote to local", ->
            runTests2 collectionObj, dataObjectObj, dirObj, fileObj
            runTests3 collectionObj, dataObjectObj, dirObj, fileObj, "zzz"
            runTests4 collectionObj, dataObjectObj, dirObj, fileObj, "fff"
            runTests4 collectionObj0, collectionObj, dirObj0, dirObj
            runTests5 collectionObj, dataObjectObj, dirObj, fileObj
            runTests5 collectionObj0, collectionObj, dirObj0, dirObj

        describe "from local to local", ->
            runTests2 dirObj, fileObj, dirObj2, fileObj3
            runTests3 dirObj, fileObj, dirObj2, fileObj3
            runTests4 dirObj, fileObj, dirObj2, fileObj3, "vvv"
            runTests4 dirObj0, dirObj, dirObj2, dirObj3, "ooo"
            runTests5 dirObj, fileObj, dirObj2, fileObj3
            runTests5 dirObj0, dirObj, dirObj0, dirObj2
            
        describe "from remote to remote", ->
            runTests2 collectionObj, dataObjectObj, collectionObj2, dataObjectObj3
            runTests3 collectionObj, dataObjectObj, collectionObj2, dataObjectObj3
            runTests4 collectionObj, dataObjectObj, collectionObj2, dataObjectObj3, "nnn"
            runTests4 collectionObj0, collectionObj, collectionObj2, collectionObj3, "n2n"
            runTests5 collectionObj, dataObjectObj, collectionObj2, dataObjectObj3
            runTests5 collectionObj0, collectionObj, collectionObj0, collectionObj2
        
    its "testing metadata and data object at the same time", testIt ->
        cObj = clone collectionObj
        dObj = clone dataObjectObj
        _ <- bind cObj.create
        dObj.metadata = new ArrayBackedContent [avuObj]
        dObj.content = new mod.BufferContent buf
        _ <- bind dObj.create
        assertProp dObj, "metadata", [avuObj]

    its "testing create metadata, data object, and collection at the same time", testIt ->
        cObj = clone collectionObj
        dObj = clone dataObjectObj
        dObj.content = new mod.BufferContent buf # content has to go before metadata so that file gets created first
        dObj.metadata = new ArrayBackedContent [avuObj]
        cObj.content = new ArrayBackedContent [dObj]
        # console.log "crated"
        _ <- bind cObj.create
        assertProp dObj, "metadata", [avuObj]
        
    its "testing reading metadata, data object at the same time", testIt ->
        cObj = clone collectionObj
        dObj = clone dataObjectObj
        dObj.content = new mod.BufferContent buf # content has to go before metadata so that file gets created first
        
        cObj.metadata = new ArrayBackedContent [cavu]
        cObj.content = new ArrayBackedContent [dObj]
        _ <- bind cObj.create
        
        cObj2 = mod.object {
            type: cObj.type
            ref: cObj.ref
            metadata: {}
            content: {}
        }
        ext <- bind cObj2.read ["metadata", "content"]
        arr <- bind ext.content.getContent
        _ <- bind assertEqualDeep arr, [dObj]
        arr2 <- bind ext.metadata.getContent
        assertEqualDeep arr2, [cavu]
        
    its "testing del coll with metadata", testIt ->
        cobj = clone collectionObj
        cobj.metadata = new ArrayBackedContent [avuObj]
        cobj.content = null
        cobj.create

    its "testing coll metadata copy", testIt ->
        cobj = clone collectionObj
        cobj.metadata = new ArrayBackedContent [cavu]
        cobj.content = null
        _ <- bind cobj.create
        cobj2 = mod.object ({} <<< collectionObj2)
        cobj2.metadata = null
        cobj2.content = null
        _ <- bind cobj2.create
        avuCnt <- bind cobj.readProp "metadata"
        _ <- bind mod.transferContent avuCnt, "metadata", cobj2
        log "verifying copy"
        assertProp cobj2, "metadata", [cavu]
        
    its "testing coll metadata move", testIt ->
        cobj = clone collectionObj
        cobj.metadata = new ArrayBackedContent [cavu]
        cobj.content = null
        _ <- bind cobj.create
        cobj2 = clone collectionObj2
        cobj2.metadata = null
        cobj2.content = null
        _ <- bind cobj2.create
        avuCnt <- bind cobj.readProp "metadata"
        _ <- bind mod.move cobj, "metadata", avuCnt, cobj2, "metadata", null
        _ <- bind assertProp cobj2, "metadata", [cavu]
        assertProp cobj, "metadata", []
        
    its "test coll metadata update", testIt ->
        cobj = clone collectionObj
        cobj.metadata = null
        cobj.content = null
        _ <- bind cobj.create
        _ <- bind cobj.updateProp "metadata", new PropDiff none, new ArrayBackedContent [cavu]
        assertProp cobj, "metadata", [cavu]
            

    its "testing del with metadata", testIt ->
        cObj = clone collectionObj
        dObj = clone dataObjectObj
        dObj.metadata = new ArrayBackedContent [avuObj]
        dObj.content = new mod.BufferContent buf
        cObj.content = new ArrayBackedContent [dObj]
        _ <- bind cObj.create
        _ <- bind dObj.del
        cObj.del

    its "testing metadata copy", testIt ->
        cObj = clone collectionObj
        dObj = clone dataObjectObj
        dObj2 = clone dataObjectObj2
        
        dObj.metadata = new ArrayBackedContent [avuObj]
        dObj.content = new mod.BufferContent buf
        dObj2.content = new mod.BufferContent buf
        cObj.content = new ArrayBackedContent [dObj, dObj2]
        _ <- bind cObj.create
        dObj.metadata = null
        avuCnt <- bind dObj.readProp "metadata"
        _ <- bind mod.transferContent avuCnt, "metadata", dObj2
        log "verifying copy"
        assertProp dObj2, "metadata", [avuObj]
    
    
    its "metadata update", testIt ->
        cObj = clone collectionObj
        dObj = clone dataObjectObj
        dObj.metadata = new ArrayBackedContent [avuObj]
        dObj.content = new mod.BufferContent buf
        cObj.content = new ArrayBackedContent [dObj]
        _ <- bind cObj.create
        
        _ <- bind dObj.updateProp "metadata", new PropDiff avuObj, avuObj2
        assertProp dObj, "metadata", [avuObj2]
        

    its "testing metadata move", testIt ->
        cObj = clone collectionObj
        dObj = clone dataObjectObj
        dObj2 = clone dataObjectObj2
        
        dObj.metadata = new ArrayBackedContent [avuObj]
        dObj.content = new mod.BufferContent buf
        dObj2.content = new mod.BufferContent buf
        cObj.content = new ArrayBackedContent [dObj, dObj2]
        _ <- bind cObj.create
        dObj.metadata = null
        avuCnt <- bind dObj.readProp "metadata"
        _ <- bind mod.move dObj, "metadata", avuCnt, dObj2, "metadata", null
        _ <- assertProp dObj2, "metadata", [avuObj]
        assertProp dObj2, "metadata", []
        
    its "add 1000 avus", testIt ->
        cobj = clone collectionObj
        dobj = clone dataObjectObj
        dobj.metadata = avus = genavu avuObj
        _ <- bind create [cobj, dobj]
        assertProp dobj, "metadata", avus
        
        
    its "get modify time", testIt ->
        _ <- bind collectionObj.create
        _ <- bind dataObjectObj.create
        t <- bind dataObjectObj.readProp "modifyTime"
        log t
        ret null
    
    its "get modify time of nonexisting object", testErrorIt ->
        dataObjectObj.modifyTime = null
        dataObjectObj.readProp "modifyTime"
    
    its "get coll owner name", testIt ->
        _ <- bind collectionObj.create
        t <- bind collectionObj.readProp "ownerName"
        assertEqual t, "rods"
    
    its "get owner name of nonexisting coll", testErrorIt ->
        collectionObj.ownerName = null
        collectionObj.readProp "ownerName"     
        
    describe "resc model", ->
            
        its "create resc", testIt ->
            res = clone resc
            _ <- bind res.create
            ext <- bind res.exists
            assertEqual ext, true
            
        its "resc prop", testIt ->
            res = clone resc
            _ <- bind res.create
            delete res.rescClass
            rc <- bind res.readProp "rescClass"
            assertEqual rc, "cache"
            
        its "delete resc", testIt ->
            res = clone resc
            _ <- bind res.create
            res2 = clone resc2
            _ <- bind res2.del
            ext <- bind res.exists
            assertEqual ext, false
            
        its "modify resc", testIt ->
            res = clone resc
            _ <- bind res.create
            _ <- bind res.updateProp "rescPath", new PropDiff "/tmp/testResc", "/tmp/testRes"
            ret null
            
        its "resc eq", testIt ->
            res = clone resc
            assert res.eq res
            
        its "resc name", testIt ->
            res = clone resc
            name <- bind res.readProp "rescName"
            assertEqual name, res.rescName
            
        its "mod resc name", testIt ->
            res = clone resc
            _ <- bind res.create
            _ <- bind res.updateProp "rescName", new PropDiff all, "testRes"
            name <- bind res.readProp "rescName"
            _ <- bind assertEqual name, "testRes"
            e <- bind res.exists
            _ <- bind assert e
            res.updateProp "rescName", new PropDiff all, "testResc"
            

    describe "replica model", ->

        
        dObj = clone dataObjectObj
        cObj = clone collectionObj

        replicaS = mod.object {
            type: "Replica"
            ref: "#testColl/#testData?rescName=testResc"
        }
        beforeEach testIt ->
            res = clone resc
            create [cObj, dObj, resc]
            
        afterEach testIt ->
            ext <- bind replicaS.exists
        
            if ext 
                replicaS.del
            else
                ret null
                
            
        its "replica prop", testIt ->
            _ <- bind dObj.createProp "replicas", replica
            n <- bind replica.readProp "replNum"
            assertEqual n, "1"
        
        its "replica eq", testIt ->
            assert replica.eq replica
            
        its "replica list", testIt ->
            assertProp dObj, "replicas", [{
                type: "Replica"
                rescName: "demoResc"
            }]
            
        its "replica path", testIt ->
            _ <- bind dObj.createProp "replicas", replica
            n <- bind replicaS.readProp "path"
            assertEqual "irods://#n", "#testColl/#testData"
        
        its "replica resc", testIt ->
            _ <- bind dObj.createProp "replicas", replica
            n <- bind replicaS.readProp "resource"
            assert n.eq resc
            
        its "replica ref", testIt ->
            refobj <- bind replicaS.readProp "refobj"
            _ <- bind assertEqual refobj.path, "#{testColl.substring 8}/#testData"
            rescName <- bind replica.readProp "rescName"
            assertEqual rescName, "testResc"
            
        its "repl", testIt ->
            _ <- bind dObj.createProp "replicas", replica
            ext <- bind replicaS.exists
            assert ext 
            
        its "trim", testIt ->
            _ <- bind dataObjectObj.createProp "replicas", replica
            _ <- bind dataObjectObj.delProp "replicas", replica
            ext <- bind replicaS.exists
            assert !ext
            
    describe "user model", ->
        its "auth user", testIt -> 
            rods = mod.object {
                type: "User",
                ref: "rods\#tempZone"
                password: "rods"
            }
            ext <- bind rods.exists
            assert ext
        
        its "auth user wrong password", testIt -> 
            rods = mod.object {
                type: "User"
                ref: "rods\#tempZone"
                password: "rods2"
            }
            ext <- bind rods.exists
            assert !ext
            
        its "read user type by read object", testIt ->
            rods = mod.object {
                type: "User"
                ref: "rods\#tempZone"
                userType: {}
            }
            ext <- bind rods.read ["userType"]
            assertEqual ext.userType, "rodsadmin"
            
        its "read user type by read prop", testIt ->
            rods = mod.object {
                type: "User"
                ref: "rods\#tempZone"
            }
            type <- bind rods.readProp "userType"
            assertEqual type, "rodsadmin"
            
        its "nonexisting user type", testErrorIt ->
            rods2 = mod.object {
                type: "User"
                ref: "rods2\#tempZone"
            }
            rods2.readProp "userType"
            
        its "create user", testIt ->
            
            _ <- bind user.create
            ext <- bind user.exists
            assertEqual ext, true
            
        its "delete user", testIt ->
            user = mod.object {
                type: "User"
                ref: "t\#tempZone"
            }
            user.del
            
        its "modify user", testIt ->
            user = mod.object {
                type: "User"
                ref: "t\#tempZone"
                userType: "rodsuser"
            }
            _ <- bind user.create
            user.updateProp "userType", new PropDiff "rodsuser", "rodsadmin"
            
    describe "diffing", ->
        its "structured object diff 1", testIt ->
            cobj = clone collectionObj
            dobj = clone dataObjectObj
            dobj.metadata = [ avuObj ]
            _ <- bind create [cobj, dobj]
            _ <- bind cobj.update new PropDiff { 
                content: [
                    mod.object {
                        type: dobj.type
                        ref: dobj.ref
                        metadata: [ avuObj ]
                    }
                ]
            }, {
                content: [
                    mod.object {
                        type: dobj.type
                        ref: dobj.ref
                        metadata: [ avuObj2 ]
                    }
                ]
            }
            ext <- bind dobj.exists
            _ <- bind assert ext
            assertProp dobj, "metadata", [avuObj2]
            
        its "structured object diff 2", testIt ->
            cobj = clone collectionObj
            dobj = clone dataObjectObj
            dobj.metadata = [ avuObj ]
            _ <- bind create [cobj, dobj]
            _ <- bind cobj.update new PropDiff { 
                content: [
                    mod.object {
                        type: dobj.type
                        ref: dobj.ref
                        metadata: [ avuObj ]
                    }
                ]
            }, {
                content: [
                    mod.object {
                        type: dobj.type
                        ref: dobj.ref
                        metadata: [ ]
                    }
                ]
            }
            ext <- bind dobj.exists
            _ <- bind assert ext
            assertProp dobj, "metadata", []
            
        its "structured object diff 3", testIt ->
            cobj = clone collectionObj
            dobj = clone dataObjectObj
            dobj.metadata = [ avuObj ]
            _ <- bind create [cobj, dobj]
            _ <- bind cobj.update new PropDiff { 
                content: [
                    mod.object {
                        type: dobj.type
                        ref: dobj.ref
                        metadata: [ avuObj ]
                    }
                ]
            }, {
                content: [
                    mod.object {
                        type: dobj.type
                        ref: dobj.ref
                        metadata: [ avuObj, avuObj2 ]
                    }
                ]
            }
            ext <- bind dobj.exists
            _ <- bind assert ext
            assertProp dobj, "metadata", [ avuObj, avuObj2 ]
            
    describe "buffer content", ->
        b1 = new Buffer "test"
        b2 = new Buffer "test2"
        b3 = new Buffer "txt"
        its "buffer content read/write", testIt ->
            bc = new mod.BufferContent new Buffer b1
            _ <- bind bc.writeFully b2
            buf <- bind bc.readFully
            assertEqual b2.toString(), buf.toString()
            
        its "buffer content read/write 2", testIt ->
            bc = new mod.BufferContent new Buffer b1
            _ <- bind bc.writeFully b3
            buf <- bind bc.readFully
            assertEqual b3.toString(), buf.toString()
            
        its "cannot read out of range from RangeContent", testErrorIt ->
            bc = new mod.RangeContent (new mod.BufferContent b1), 1, 2
            bc.read 1, 10000

        its "buffer content subrange read", testIt ->
            bc = new mod.BufferContent b1
            rng = new mod.RangeContent bc, 1, 3
            buf <- bind rng.readFully
            assertEqual "es", buf.toString()
            
        its "buffer content subrange write", testIt ->
            bc = new mod.BufferContent new Buffer b1
            rng = new mod.RangeContent bc, 1, 3
            _ <- bind rng.writeFully b3
            buf <- bind bc.readFully
            assertEqual "ttxt", buf.toString()
            
    after testIt ->
        mod.cleanup
