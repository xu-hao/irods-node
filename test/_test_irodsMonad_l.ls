f = require "fs"
irodsMonad = require "../src/plugins/irodsMonad_l"
irodsMonadTools = require "../src/plugins/irodsMonadTools_l"
trans = (require "../src/plugins/transport/tcp_l").plugin
prot = (require "../src/plugins/protocol/xml_l").plugin
{rodsEnv, rodsCred} = require "../src/config.js"
auth = (require "../src/plugins/auth/basic-auth_l").plugin
{runCont, bind, ret, throwm, catchm, Exception} = require "../src/plugins/monad_l"
{ObjType} = require "../src/plugins/const_l"
irods = irodsMonad prot, trans, rodsEnv, auth, rodsCred
irodsTools = irodsMonadTools irods
{log,assert,assertEqual,testIt,testErrorIt} = require "./_test_framework_l"

beq = (buf1, buf2) -> 
    if buf1.length != buf2.length
        false
    else
        retval = true
        for i from 0 til buf1.length
            if buf1[i] != buf2[i]
                retval := false
                break
        retval
        
testDir = "/tempZone/home/rods/testDir"
testFile = "testFile"
buf = new Buffer "HelloWorld"
obj =
    refobj:
        path: "#testDir/#testFile"
    readProp: (prop) -> ret obj[prop]

coll =
    refobj:
        path: "#testDir"
    readProp: (prop) -> ret coll[prop]


val =
    attribute: "a"
    value: "v"
    unit: "u"
    readProp: (prop) -> ret val[prop]
val2 =
    attribute: "z"
    value: "w"
    unit: "x"
    readProp: (prop) -> ret val2[prop]

its = it

describe "irodsMonad", ->
    its "testing mkdir", testIt ->
        _ <- bind irods.mkdir testDir
        irods.stat testDir

    its "testing cannot mkdir twice", testErrorIt ->
        irods.mkdir testDir

    its "testing rmdir", testIt ->
        irods.rmdir testDir

    its "testing rmdir removed dir", testErrorIt ->
        irods.stat testDir

    its "testing cannot rmdir nonexisting", testErrorIt ->
        irods.rmdir testDir

    its "testing mkdir again", testIt ->
        _ <- bind irods.mkdir testDir
        irods.stat testDir

    its "testing write and read file", testIt ->
        _ <- bind irods.put buf, "", {gid:0, uid:0, mode:0,ctime: new Date, mtime: new Date}, "#testDir/#testFile"
        {bs} <- bind irods.get "#testDir/#testFile", 0
        assertEqual buf.toString(), bs.toString()

    its "testing readdir", testIt ->
        files <- bind irodsTools.readdir testDir
        _ <- bind assertEqual files.length, 1
        assertEqual files[0].name, testFile

    its "testing readdir with criteria", testIt ->
        files <- bind irodsTools.readdir testDir, {name: {o:"=",v:testFile}}
        _ <- bind assertEqual files.length, 1
        assertEqual files[0].name, testFile

    its "testing readdir with criteria 2", testIt ->
        files <- bind irodsTools.readdir testDir, {name: {o:"=",v:testFile+"not"}}
        assertEqual files.length, 0


    its "testing cannot rmdir nonempty", testErrorIt ->
        irods.rmdir testDir, false

    its "testing add data object AVU", testIt ->
        irods.modAVU obj, "add", "-d", val

    its "testing query data object AVU", testIt ->
        arr <- bind irodsTools.readDataObjectAVU obj.refobj.path
        _ <- bind assertEqual arr.length, 1
        _ <- bind assertEqual arr[0].attribute, "a"
        _ <- bind assertEqual arr[0].value, "v"
        assertEqual arr[0].unit, "u"

    its "testing delete data object AVU", testIt ->
        _ <- bind irods.modAVU obj, "rm", "-d", val
        arr <- bind irodsTools.readDataObjectAVU obj.refobj.path
        # console.log arr
        assertEqual arr.length, 0

    its "testing add collection AVU", testIt ->
        irods.modAVU coll, "add", "-C", val

    its "testing query collection AVU", testIt ->
        arr <- bind irodsTools.readCollectionAVU coll.refobj.path
        _ <- bind assertEqual arr.length, 1
        _ <- bind assertEqual arr[0].attribute, "a"
        _ <- bind assertEqual arr[0].value, "v"
        assertEqual arr[0].unit, "u"

    its "testing delete collection AVU", testIt ->
        _ <- bind irods.modAVU coll, "rm", "-C", val
        arr <- bind irodsTools.readCollectionAVU coll.refobj.path
        assertEqual arr.length, 0

    its "testing delete file", testIt ->
        irods.unlink "#testDir/#testFile"

    its "testing file deleted", testErrorIt ->
        irods.stat "#testDir/#testFile"

    its "cleaning up", testIt ->
        irods.rmdir testDir

    its "testing cleaning up done", testErrorIt ->
        irods.stat testDir
        
        
    describe "user", ->
    
        cleanup = do
            users <- bind irodsTools.listUsers { userName: { o:"=", v:"t"} }
            if users.length == 1
                irods.deleteUser "t"
            else
                ret null
            
        beforeEach testIt ->
            cleanup
        
        afterEach testIt ->
            cleanup
            
        its "list user", testIt ->
            users <- bind irodsTools.listUsers {}
            log JSON.stringify users
            assertEqual [u for u in users when u.userName == "rods"].length, 1
            
        its "add user", testIt ->
            _ <- bind irods.createUser "t", "rodsuser"
            users <- bind irodsTools.listUsers { userName: {o: "=", v: "t"} }
            assertEqual users.length, 1
        
        its "del user", testIt ->
            _ <- bind irods.createUser "t", "rodsuser"
            _ <- bind irods.deleteUser "t"
            ret null
            
        its "mod user", testIt ->
            _ <- bind irods.createUser "t", "rodsuser"
            _ <- bind irods.modifyUser "t", "type", "rodsadmin"
            ret null
            
        /*its "add user fq", testIt ->
            _ <- bind irods.createUser "t\#tempZone", "rodsuser"
            users <- bind irodsTools.listUsers { userName: {o: "=", v: "t"} }
            assertEqual users.length, 1
        
        its "del user fq", testIt ->
            _ <- bind irods.createUser "t\#tempZone", "rodsuser"
            _ <- bind irods.deleteUser "t\#tempZone"
            ret null
            
        its "mod user fq", testIt ->
            _ <- bind irods.createUser "t\#tempZone", "rodsuser"
            _ <- bind irods.modifyUser "t\#tempZone", "type", "rodsadmin"
            ret null*/
        
    describe "resource", ->
    
        cleanup = do
            resc <- bind irodsTools.listResc { rescName: {o: "=", v: "tempResc"} }
            if resc.length == 1
                irods.deleteResource "tempResc"
            else
                ret null
                
        beforeEach testIt ->
            cleanup

        its "add resource", testIt ->
            _ <- bind irods.createResource "tempResc", "unix file system", "localhost", "/tmp/tempResc"
            resc <- bind irodsTools.listResc { rescName: {o: "=", v: "tempResc"} }
            assertEqual resc.length, 1
            
        its "del resource", testIt ->
            _ <- bind irods.createResource "tempResc", "unix file system", "localhost", "/tmp/tempResc"
            _ <- bind irods.deleteResource "tempResc"
            resc <- bind irodsTools.listResc { rescName: {o: "=", v: "tempResc"} }
            assertEqual resc.length, 0
            
        its "mod resource path", testIt ->
            _ <- bind irods.createResource "tempResc", "unix file system", "localhost", "/tmp/tempResc"
            _ <- bind irods.modifyResource "tempResc", "path", "/tmp/tmpRes"
            ret null
            
        describe "replica", ->
            beforeEach testIt ->
                _ <- bind irods.createResource "tempResc", "unix file system", "localhost", "/tmp/tempResc"
                _ <- bind irods.mkdir "#testDir"
                irods.put buf, "", {gid:0, uid:0, mode:0,ctime: new Date, mtime: new Date}, "#testDir/#testFile"
            
            afterEach testIt ->
                # console.log "delete #testDir"
                list <- bind irodsTools.listReplicas "#testDir/#testFile", { rescName: {o: "=", v: "tempResc"} }
                
                _ <- bind if list.length == 1 then irods.trim "#testDir/#testFile", "tempResc" else ret null
                _ <- bind irods.rmdir "#testDir", true
                irods.deleteResource "tempResc"
            
            its "repl", testIt ->
                _ <- bind irods.repl "#testDir/#testFile", "tempResc"
                list <- bind irodsTools.listReplicas "#testDir/#testFile", { rescName: { o: "=", v: "tempResc" } }
                assertEqual list.length, 1
                
            its "trim", testIt ->
                _ <- bind irods.repl "#testDir/#testFile", "tempResc"
                _ <- bind irods.trim "#testDir/#testFile", "tempResc"
                list <- bind irodsTools.listReplicas "#testDir/#testFile", { rescName: { o: "=", v: "tempResc" } }
                assertEqual list.length, 0
            
        afterEach testIt ->
            cleanup
            
            
    describe "para opr", ->
        its "para put user", testIt ->
            now = new Date
            buf = new Buffer 100000000
            tgt = "/tempZone/home/rods/testFile"
            _ <- bind irods.put buf, "/tmp/testFile", {uid:0, gid:0, mode: 0, ctime: now, mtime: now}, tgt, 4
            #{bs:buf2} <- bind irods.get tgt, 0
            #_ <- bind assert beq buf, buf2 
            irods.unlink tgt
        its "para put server", testIt ->
            now = new Date
            buf = new Buffer 100000000
            tgt = "/tempZone/home/rods/testFile"
            _ <- bind irods.put buf, "/tmp/testFile", {uid:0, gid:0, mode: 0, ctime: now, mtime: now}, tgt, 0
            #{bs:buf2} <- bind irods.get tgt, 0
            #_ <- bind assert beq buf, buf2 
            irods.unlink tgt
        its "put no threading", testIt ->
            now = new Date
            buf = new Buffer 100000000
            tgt = "/tempZone/home/rods/testFile"
            _ <- bind irods.put buf, "/tmp/testFile", {uid:0, gid:0, mode: 0, ctime: now, mtime: now}, tgt, -1
            #{bs:buf2} <- bind irods.get tgt, 0
            #_ <- bind assert beq buf, buf2 
            irods.unlink tgt
    after testIt ->
        irods.cleanup
