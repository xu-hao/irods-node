{UNION, setUnion} = require "../src/plugins/arrays_l"
should = require "should"

its = it

obj = (v) -> {
    val: v
    eq: (b) -> 
        (@val %% 2) == (b.val %% 2)
}

describe "array", ->
    its "eq", ->
        [1, 2, 3].should.be.eql [1,2,3]

    its "union", ->
        ([1, 2, 3] `UNION` [2, 3, 4, 5]).should.be.eql [1,2,3,4,5]
        
    its "setUnion", ->
        
        ([obj(1), obj(2)] `setUnion` [obj(3), obj(4)]).length.should.be.eql 2
    
    
    