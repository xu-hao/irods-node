f = require "fs"
irodsMonad = require "../src/plugins/irodsMonad_l"
irodsMonadTools = require "../src/plugins/irodsMonadTools_l"
trans = (require "../src/plugins/transport/tcp_l").plugin
prot = (require "../src/plugins/protocol/xml_l").plugin
{rodsEnv, rodsCred} = require "../src/config.js"
auth = (require "../src/plugins/auth/basic-auth_l").plugin
{runCont, bind, ret, throwm, catchm, Exception} = require "../src/plugins/monad_l"
{ObjType} = require "../src/plugins/const_l"
{argv} = process
cont = (a) -> console.log a + "\n" + summary()
log = (require "debug")("test")
success = 0
failure = 0
tests = []

it = it ? (msg, f) ->
    tests.push (cont) ->
        log msg
        done = (x) ->
            if x?
                log "FAILED"
                console.log x
                failure++
                cont()
            else
                log "SUCCEEDED"
                success++
                cont()
        f done

runTests = (f) ->
    f ()
    cont = ->
        if tests.length == 0
            console.log summary()
        else
            test0 = tests[0]
            tests := tests.slice 1
            test0 cont
    cont()

testIt = (f) ->
    (done) ->
        (runCont (f.run ? f)()) (a) ->
            if a.val != undefined
                done()
            else
                done new Error JSON.stringify a

testErrorIt = (f) ->
    (done) ->
        (runCont (f.run ? f)()) (a) ->
            if a.val != undefined
                done new Error "expected error"
            else
                done()

assert = (test) ->
    if !test
        throwm "test failed"
    else
        ret null

assertEqual = (a, b) ->
    if a != b
        throwm "test failed [#a] != [#b]"
    else
        ret null

summary = ->
    "S:#success/F:#failure"

test = (f) ->
    if argv.length > 2 && argv[2] == "list"
        log "#{f.id ? "test"}"
        ret null
    else if argv.length > 2 && argv[2] != f.id
        log "skipped #{f.id ? "test"}"
        ret null
    else
        (catchm (
            _ <- bind (f.run ? f)()
            success++
            log "test #{f.id} SUCCEEDED #{summary()}"
            ret null
        )) (ex)->
            failure++
            log "test #{f.id} FAILED #{summary()}"
            console.error ex
            ret null

testError = (f) ->
    if argv.length > 2 && argv[2] == "list"
        log "#{f.id ? "test"}"
        ret null
    else if argv.length > 2 && argv[2] != f.id
        log "skipped #{f.id ? "test"}"
        ret null
    else
        (catchm (
            _ <- bind (f.run ? f)()
            failure++
            log "test #{f.id} FAILED #{summary()}"
            ret null
            # throwm "expected error, but no error encountered"
        )) (ex)->
            success++
            log "test #{f.id} SUCCEEDED #{summary()}"
            ret null

module.exports = {test, testError, assert, assertEqual, summary, cont, log, runTests, testIt, testErrorIt, it}
