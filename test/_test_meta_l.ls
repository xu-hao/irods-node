should = require "should"
{Rules} = require "../src/plugins/metamodel_l"


rules = Rules()

AA = -> "AA"
BB = -> "BB"
AB = -> "AB"
hier =
    Object:
        opers:
            f:
              * pt: <[ B B ]>
                run: BB
              * pt: <[ A B ]>
                run: AB
              * pt: <[ A A ]>
                run: AA
        subtypes:
            A:
                subtypes:
                    B: {}

rules.addRules hier

its = it
describe "meta", ->
    its "meta1", -> (rules.lookupOperRule <[ A A ]>, "f")().should.be.eql "AA"
    its "meta2", -> (rules.lookupOperRule <[ A B ]>, "f")().should.be.eql "AB"
    its "meta3", -> (rules.lookupOperRule <[ B B ]>, "f")().should.be.eql "BB"
    its "meta4", -> (rules.lookupOperRule <[ B A ]>, "f")().should.be.eql "AA"
