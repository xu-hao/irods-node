/*
  The collection object
*/

{bind, ret, throwm, catchm} = require "../monad_l"
{display} = require "../arrays_l"
{model} = require "../model_l"
{PropDiff, ArrayBackedContent, none} = require "../metamodel_l"
{stdout} = require "../stdio_l"
path = require "path"

module.exports =
    name: "copy"
    category: "client"
    playbook: (model) ->
        (arg) -> do
            # console.log src + " to " + tgt
            src = arg.from
            tgt = arg.to
            objs = model.object {
                ref: src
                type: "PathObject"
            }

            objt = model.object {
                ref: tgt
                type: "AdaptiveObject"
            }

            exts <- bind objs.exists
            if !exts
                throwm {error:-1, errormsg: "#src does not exist"}
            else
                extt <- bind objt.exists
                #console.log extt
                if extt
                    if objt.instanceOf "CollectiveObject"
                        objsr <- bind model.retargetUnder objs, objt
                        extsr <- bind objsr.exists
                        if !extsr
                            console.log "copying"
                            objt.updateProp "content", new PropDiff none, new ArrayBackedContent [objs]
                        else
                            throwm {error:-1, errormsg: "#{objsr.ref} already exists"}
                    else
                        throwm {error:-1, errormsg: "#tgt already exists"}
                else
                    objt.content <- bind objs.readProp "content"
                    objt.create
