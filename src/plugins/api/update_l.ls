{bind, ret, throwm, catchm} = require "../monad_l"
{display} = require "../arrays_l"
{model} = require "../model_l"
{PropDiff, ArrayBackedContent, none} = require "../metamodel_l"
{stdout} = require "../stdio_l"
path = require "path"

module.exports =
    name: "update"
    category: "client"
    playbook: (model) ->
        (arg) -> do
            src = arg.from
            tgt = arg.to
            objs = model.object ({
                type: "PathObject"
            } <<< src)

            objt = model.object ({
                type: "AdaptiveObject"
            } <<< tgt)

            exts <- bind objs.exists
            if !exts
                throwm {error:-1, errormsg: "#src does not exist"}
            else
                objs.update new StructuralObjDiff objs, objt
