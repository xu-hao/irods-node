{bind, ret, throwm, catchm} = require "../monad_l"
{display, transpose, rec} = require "../arrays_l"
{model} = require "../model_l"
{PropDiff, ArrayBackedContent, none} = require "../metamodel_l"
{stdout} = require "../stdio_l"
path = require "path"

module.exports =
    name: "move"
    category: "client"
    playbook: (model) ->
        (arg) -> do
            objs = model.object ({
                type: "PathObject"
            } <<< arg.from)

            objt = model.object ({
                type: "PathObject"
            } <<< arg.to)

            proprs = [ [prop, cc] for own prop, cc of arg when prop != "ref" && prop != "type" ]
            proprt = [ prop for own prop, cc of arg when prop != "ref" && prop != "type" ]
            exts <- bind objs.exists
            if !exts
                throwm {error:-1, errormsg: "#{objs.ref} does not exist"}
            else
                extt <- bind objt.exists
                if !extt
                    throwm {error:-1, errormsg: "#{objt.ref} does not exist"}
                else
                    switch
                    | proprs.length == proprt.length > 0 => # move props from s to t
                        proprst = transpose ((transpose props) ++ [propt])
                        rec proprst, ([ps, cs, pt]) ->
                            sval <- bind objs.readProp ps, cs
                            model.move objs, ps, sval, objt, pt, null
                    | _ =>
                        throwm {error:-1, errormsg: "wrong number of properties"}
                    /*if objt.instanceOf "CollectiveObject"
                        objsr <- bind model.retargetUnder objs, objt
                        extsr <- bind objsr.exists
                        if !extsr
                            objs.updateProp "ref", new PropDiff src, objsr.ref
                        else
                            throwm {error:-1, errormsg: "#{objsr.ref} already exists"}
                    else
                else
                    objs.updateProp "ref", new PropDiff src, tgt*/
