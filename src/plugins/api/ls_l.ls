/*
  The collection object
*/

{bind, ret, throwm, catchm, ite} = require "../monad_l"
{display, display2, rec,mapM} = require "../arrays_l"
{model} = require "../model_l"
columnsMap =
    File: ["name", "type", "ownerName", "objSize"]
    Directory: ["name", "type", "ownerName", "objSize"]
    DataObject: ["name", "type", "ownerName", "ownerZone", "objSize"]
    Collection: ["name", "type", "ownerName", "ownerZone", "objSize"]
    CollectionAVU: ["attribute", "value", "unit"]
    DataObjectAVU: ["attribute", "value", "unit"]
fs = require "../fsMonad_l"

getColumns = (combined) ->
    if combined.length == 0
        ret []
    else
        type = combined[0].type
        cols = columnsMap[type]
        if cols?
            ret cols
        else
            throwm {
                error: -1
                errormsg: "cannot find columns to display for object type " + combined[0].type
            }

displayOne = (obj, dirname) ->
    combined = [ obj ]
    cols <- bind getColumns combined
    display combined, dirname, cols

displayVal = (val, prop, dirname = val) ->
    console.log val
    console.log prop
    if val?
        if val instanceof Object && val.getContent?
            combined <- bind val.getContent
            cols <- bind getColumns combined
            display combined, "#{dirname.ref} #prop", cols
        else
            if prop?
                displayOne val, "#{dirname.ref} #prop"
            else
                displayOne val, "#{dirname.ref}"
    else
        display2 [{(prop): "null"}], "#{dirname.ref} #prop", [prop]

module.exports =
    name: "read"
    category: "client"
    playbook: (model) ->
        (arg) -> do
            obj = model.object ({
                type: "PathObject"
            } <<< arg)

            ext <- bind obj.exists
            if ext
                nobj <- bind obj.filter
                proprs = [ prop for prop, cc of arg when prop != "ref" ]

                if proprs.length == 0
                    displayVal nobj
                else
                    rec proprs, (prop) ->
                        #console.log arg
                        val <- bind nobj.readProp prop
                        displayVal val, prop, nobj
            else
                throwm {
                    error: -1
                    errormsg: "unrecognized object type " + obj.type
                }
