{Query, Constants:C} = require "../genquery_l"
{Constants:A} = require "../api_l"
{bind, bind2, ret, throwm, catchm} = (require "../monad_helpers_l") (require "../monad_l")
{stdout} = require "../stdio_l"
{Constants:E} = require "../error_l"
{ObjType} = require "../const_l"

_cols = ["name", "type", "ownerName", "ownerZone", "size"]

display = (arr, header, cols = _cols) ->
    width = {}
    totalwidth = 0
    for col in cols
        maxlen = col.length
        for elem in arr
            ec = elem[col]
            if ec? then
                len = (if ec?.name? then ec.name else "#ec").length
                if maxlen < len then
                    maxlen = len
        width[col] = maxlen
    for col in _cols
        totalwidth += width[col]

    totalwidth += cols.length - 1
    headerlen = header.length
    if totalwidth < headerlen
        totalwidth = headerlen

    out = ""
    out += "=" * totalwidth + "\n"
    out += header + "\n"

    out += "-" * totalwidth + "\n"
    for col in cols
        out += col + (" " * (width[col] + 1 - col.length))
    out += "\n"

    out += "-" * totalwidth + "\n"
    for elem in arr
        for col in cols
            ec = elem[col]
            if ec?
                if ec.name? then ec = ec.name
                str = "#ec"
                w = width[col] + 1
                str += " " * (w - str.length)
                out += str
        out += "\n"
    out += "=" * totalwidth + "\n"
    stdout out


transpose = (arr) ->
    # console.log arr
    mat = []
    for i from 0 til arr[0].length
        row = []
        for j from 0 til arr.length
            row[j] = arr[j][i]
        mat[i] = row
    # console.log mat
    mat

toObjects = (arr, cols) ->
    mat = []
    for i from 0 til arr.length
        row = {}
        for j from 0 til arr[i].length
            row[cols[j]] = arr[i][j]
        mat[i] = row
    mat

module.exports =
    name: "ils"
    category: "client"
    playbook: (transport) ->
        {writeMsg, readMsg, api} = transport
        getResults = (msg) ->
            (catchm (do
                retmsg <- bind api A.GEN_QUERY_AN, msg
                ret retmsg.msg.GenQueryOut_PI.SqlResult_PI)) (err) ->
                if err.error == E.CAT_NO_ROWS_FOUND
                    # not an error
                    ret [{value:[]}]
                else
                    throwm err
        statObj = (dirname) -> do
            msg =
                DataObjInp_PI:
                    objPath: dirname
                    createMode: 0
                    openFlags: 0
                    offset: 0
                    dataSize: 0
                    numThreads: 0
                    oprType: 0
                    KeyValPair_PI:
                        ssLen: 0
            # console.log msg
            retmsg <- bind api A.OBJ_STAT_AN, msg
            # console.log retmsg
            ret retmsg.msg.RodsObjStat_PI

        (dirname) -> do
            stat <- bind statObj dirname
            switch parseInt stat.objType
            | ObjType.DATA_OBJECT.id => do
                combined = [
                    name: dirname
                    type: ObjType.DATA_OBJECT
                    size: stat.objSize
                    ownerName: stat.ownerName
                    ownerZone: stat.ownerZone
                ]
                <- bind display combined, dirname
                ret combined
            | ObjType.COLLECTION.id => do # collection
                absPath = dirname
                query = new Query
                    ..select C.DATA_NAME
                    ..and C.DATA_SIZE
                    ..and C.D_OWNER_NAME
                    ..and C.D_OWNER_ZONE
                    ..where C.COLL_NAME, "=", absPath
                msg = query.build()
                res <- bind getResults msg
                mat = [col.value for col in res[0 til 4]] ++ [[ObjType.DATA_OBJECT] * res[0].value.length]
                retVal1 = toObjects (transpose mat), ["name", "size", "ownerName", "ownerZone", "type"]

                query = new Query
                    ..select C.COLL_NAME
                    ..and C.COLL_OWNER_NAME
                    ..and C.COLL_OWNER_ZONE
                    ..where C.COLL_PARENT_NAME, "=", absPath
                msg = query.build()
                res <- bind getResults msg
                mat = [col.value for col in res[0 til 3]] ++ [[ObjType.COLLECTION] * res[0].value.length]
                retVal2 = toObjects (transpose mat), ["name", "ownerName", "ownerZone", "type"]

                combined = retVal1 ++ retVal2
                <- bind display combined, dirname
                ret combined
            | _ => do
                throwm do
                    error: -1
                    errormsg: "unrecognized object type #that"
                    msg: null
