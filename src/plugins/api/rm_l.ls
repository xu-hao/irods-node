/*
  The collection object
*/

{bind, ret, throwm, catchm} = require "../monad_l"
{display, rec} = require "../arrays_l"
{model} = require "../model_l"
{PropDiff, StructuralObjDiff, ArrayBackedContent, none} = require "../metamodel_l"
{stdout} = require "../stdio_l"
path = require "path"
{regex, seq, alt} = require "packrattle"

module.exports =
    name: "del"
    category: "client"
    playbook: (model) ->
        (arg) -> do
            #console.log arg
            obj = model.object {
                type: "AdaptiveObject"
                ref: arg.ref
            }

            arg = model.objectDeep arg

            ext <- bind obj.exists
            if ext
                proprs = [ [prop, cc] for own prop, cc of arg when prop != "ref" && prop != "type" ]
                if proprs.length == 0
                    obj.del
                else
                    obj.update new StructuralObjDiff arg, {}
            else
                throwm {error:-1, errormsg: "#{arg.ref} does not exist"}
