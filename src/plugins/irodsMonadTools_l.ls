/*
  The model object
*/

{Constants:C} = require "./genquery_l"
{bind, ret, throwm, catchm, lazy, e, cont} = require "./monad_l"
{toObjects, mapM} = require "./arrays_l"

################################
#  irods specific code         #
#  maps irods model to         #
#  generic model               #
################################

irodsTools = (irods) ->
    encodeAVU = (id) -> "avu_id=#id"

    criteriaToWhereClauses = (type, criteria) ->
        #console.log criteria
        if criteria?
            props = QueryIndex[type]
            #console.log [[p, v] for p, v of criteria]
            mapM [[p, o, v] for own p, {o, v} of criteria when p != "ref" && p != "type"], ([p, o, v]) ->
                if props[p]?
                    ret [props[p], o, v]
                else
                    throwm {
                        error: -1
                        errormsg: "criteria #p #o #v doesn't match any query index"
                    }
        else
            ret []

    # bidirectional mapping from conanical prop names to query indices
    # use pack struct name, if pack struct name exists
    QueryIndex =
        Collection:
            name: C.COLL_NAME
            ownerName: C.COLL_OWNER_NAME
            ownerZone: C.COLL_OWNER_ZONE
        DataObject:
            name: C.DATA_NAME
            ownerName: C.D_OWNER_NAME
            ownerZone: C.D_OWNER_ZONE
            objSize: C.DATA_SIZE
            createTime: C.D_CREATE_TIME
            modifyTime: C.D_MODIFY_TIME
            checksum: C.D_DATA_CHECKSUM
        Resource:
            rescName: C.R_RESC_NAME
            rescPath: C.R_VAULT_PATH
            contextString: C.R_RESC_CONTEXT
            status: C.R_RESC_STATUS
            parent: C.R_RESC_PARENT
            children: C.R_RESC_CHILDREN
            rescType: C.R_TYPE_NAME
            zone: C.R_ZONE_NAME
            rescClass: C.R_CLASS_NAME
            rescHost: C.R_LOC
        Replica:
            rescName: C.D_RESC_NAME
            replNum: C.DATA_REPL_NUM
            replStatus: C.D_REPL_STATUS
        User:
            userName: C.USER_NAME
            userZone: C.USER_ZONE
            userType: C.USER_TYPE
        DataObjectAVU:
            metadataId: C.META_DATA_ATTR_ID
            attribute: C.META_DATA_ATTR_NAME
            value: C.META_DATA_ATTR_VALUE
            unit: C.META_DATA_ATTR_UNITS
        CollectionAVU:
            metadataId: C.META_COLL_ATTR_ID
            attribute: C.META_COLL_ATTR_NAME
            value: C.META_COLL_ATTR_VALUE
            unit: C.META_COLL_ATTR_UNITS

    ConanicalPropName = {}

    for type, props of QueryIndex
        for name, inx of props
            ConanicalPropName[inx] = name

    queryResToDataObject = (base) -> (o) ->
        o <<< {
            type: "DataObject"
            ref: base + "/" + o.name
        }

    queryResToUser = (o) ->
        o <<< {
            type: "User"
            ref: o.userName + "#" + o.userZone
        }

    queryResToReplica = (path) -> (o) ->
        o <<< {
            type: "Replica"
            ref: path + "?rescName=" + o.rescName
        }

    queryResToResource = (o) ->
        o <<< {
            type: "Resource"
        }

    queryResToCollection = (o) ->
        o <<< {
            type: "Collection"
            ref: o.name
        }
    queryResToDataObjectAVU = (base) -> (o) ->
        o <<< {
            type: "DataObjectAVU"
            ref: base + "?" + (encodeAVU o.metadataId)
        }
    queryResToCollectionAVU = (base) -> (o) ->
        o <<< {
            type: "CollectionAVU"
            ref: base + "?" + (encodeAVU o.metadataId)
        }

    resultsToObjs = (mat, cols, addProps) ->
        res = toObjects mat, [ConanicalPropName[col] ? col for col in cols]
        [addProps obj for obj in res]

    queryObjs = (cols, conds, addProps = (o) -> ) ->
        mat <- bind irods.query cols, conds
        ret resultsToObjs mat, cols, addProps

    readdir = (path, criteria = {}) ->
        addCriteria1 <- bind criteriaToWhereClauses "DataObject", criteria
        addCriteria2 <- bind criteriaToWhereClauses "Collection", criteria
        res1 <- bind queryObjs [C.DATA_NAME, C.DATA_SIZE, C.D_OWNER_NAME, C.D_OWNER_ZONE], [[C.COLL_NAME, "=", path]] ++ addCriteria1, queryResToDataObject path
        res2 <- bind queryObjs [C.COLL_NAME, C.COLL_OWNER_NAME, C.COLL_OWNER_ZONE], [[C.COLL_PARENT_NAME, "=", path]] ++ addCriteria2, queryResToCollection
        ret res1 ++ res2

    # list all replicas of a data object
    listReplicas = (path, criteria) ->
        addCriteria <- bind criteriaToWhereClauses "Replica", criteria
        [collName, dataName] = splitPath path
        queryObjs [C.DATA_REPL_NUM, C.D_REPL_STATUS, C.R_RESC_NAME], [[C.DATA_NAME, "=", dataName], [C.COLL_NAME, "=", collName]] ++ addCriteria, queryResToReplica path

    listResc = (criteria) ->
        addCriteria <- bind criteriaToWhereClauses "Resource", criteria
        queryObjs [C.R_RESC_NAME], addCriteria, queryResToResource

    listUsers = (criteria) ->
        addCriteria <- bind criteriaToWhereClauses "User", criteria
        queryObjs [C.USER_NAME, C.USER_TYPE, C.USER_ZONE], addCriteria, queryResToUser

    splitPath = (path) ->
        i = path.lastIndexOf "/"
        [(path.substring 0, i), (path.substring i+1)]

    readDataObjectAVU = (path, criteria) ->
        [collName, dataName] = splitPath path
        addCriteria <- bind criteriaToWhereClauses "DataObjectAVU", criteria
        queryObjs [C.META_DATA_ATTR_NAME, C.META_DATA_ATTR_VALUE, C.META_DATA_ATTR_UNITS, C.META_DATA_ATTR_ID],
            [[C.DATA_NAME, "=", dataName], [C.COLL_NAME, "=", collName]] ++ addCriteria, queryResToDataObjectAVU path

    readCollectionAVU = (path, criteria) ->
        addCriteria <- bind criteriaToWhereClauses "CollectionAVU", criteria
        queryObjs [C.META_COLL_ATTR_NAME, C.META_COLL_ATTR_VALUE, C.META_COLL_ATTR_UNITS, C.META_COLL_ATTR_ID],
            [[C.COLL_NAME, "=", path]] ++ addCriteria, queryResToCollectionAVU path

    readDataObjectProp = (obj, prop, criteria) ->
        inx <- bind getQueryIndex obj, prop
        {path} <- bind obj.readProp "refobj"
        [collName, dataName] = splitPath path
        irods.query [inx], [[C.COLL_NAME, "=", collName], [C.DATA_NAME, "=", dataName]]

    readReplicaProp = (obj, prop, criteria) ->
        inx <- bind getQueryIndex obj, prop
        path <- bind obj.readProp "path"
        rescName <- bind obj.readProp "rescName"
        [collName, dataName] = splitPath path
        irods.query [inx], [[C.COLL_NAME, "=" collName], [C.DATA_NAME, "=", dataName], [C.D_RESC_NAME, "=", rescName]]
        
    readResourceProp = (obj, prop, criteria) ->
        inx <- bind getQueryIndex obj, prop
        rescName <- bind obj.readProp "rescName"
        irods.query [inx], [[C.R_RESC_NAME, "=", rescName]]

    readCollectionProp = (obj, prop, criteria) ->
        inx <- bind getQueryIndex obj, prop
        {path} <- bind obj.readProp "refobj"
        irods.query [inx], [[C.COLL_NAME, "=", path]]

    getQueryIndex = (obj, prop) ->
        if (inx = QueryIndex[obj.type][prop])?
            ret inx
        else
            throwm {error:-1, errormsg: "no query index for #{obj.type}.#prop"}

    {
        readdir, readReplicaProp, readDataObjectProp, readCollectionProp, readResourceProp, queryObjs, readDataObjectAVU, readCollectionAVU,
        listResc, listReplicas, listUsers
    }

module.exports = irodsTools
