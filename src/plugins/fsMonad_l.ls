{bind, ret, throwm, catchm, runCont, lazy, e, cont} = require "./monad_l"
{Constants:E} = require "./error_l"
fs = require "fs"

# pipe_type = obj! x obj! -> cont a, e a
pipe = (srcRS, tgtWS) -> (cont) !->
    tgtWS.on "finish" ->
        #console.log "finished"
        (runCont ret null) cont
    tgtWS.on "error" (err) ->
        (runCont throwm {error: -1, errormsg: "cannot transfer file "+err}) cont
    srcRS.pipe tgtWS
    # don't call need to call this any more srcRS.resume() # enable streaming mode


createReadStream = (p) -> ret fs.createReadStream p

createWriteStream = (p) -> ret fs.createWriteStream p

readFile = (p) ->
            (cont) !-> fs.readFile p, (err, data) !->
                console.log data.toString()
                (runCont if err?
                    throwm {error: -1, errormsg: "can't read file " + p}
                else
                    ret data) cont

writeFile = (p, buf) ->
            (cont) !-> fs.writeFile p, buf, (err) !->
                (runCont if err?
                    throwm {error: -1, errormsg: "can't write file " + p}
                else
                    ret null) cont

open = (p, flags) ->
            (cont) !-> fs.open p, flags, (err, fd) !->
                (runCont if err?
                    throwm {error: -1, errormsg: "can't open file " + p}
                else
                    ret fd) cont
close = (fd) ->
            (cont) !-> fs.close fd, (err, fd) !->
                (runCont if err?
                    throwm {error: -1, errormsg: "can't close file " + fd}
                else
                    ret null) cont
                    
read = (fd, start, end) ->
            (cont) !-> 
                len = end - start
                buf = new Buffer len
                if len == 0
                    (runCont ret buf) cont
                else 
                    fs.read fd, buf, 0, len, start, (err, bytesRead, data) !->
                        (runCont if err?
                            throwm {error: -1, errormsg: "can't read file " + fd}
                        else
                            ret data) cont

write = (fd, start, end, buf) ->
            (cont) !-> 
                len = end - start
                fs.write fd, buf, 0, len, start, (err) !->
                    (runCont if err?
                        throwm {error: -1, errormsg: "can't write file " + fd}
                    else
                        ret null) cont

readdir = (path) -> (cont) !->
    fs.readdir path, (err, files) !->
        if err?
            (runCont throwm {error: -1, errormsg: "can't read dir #path"}) cont
        else
            (runCont ret files) cont


stat = (path) -> (cont) !->
    fs.stat path, (err, stats) !->
        if err?
            if err.code == "ENOENT"
                (runCont throwm {error: E.OBJ_PATH_DOES_NOT_EXIST, errormsg: "#path does not exist" }) cont
            else
                (runCont throwm {error: -1, errormsg: err}) cont
        else
            stats.name = path.substring 1 + path.lastIndexOf "/"
            stats.ownerName = stats.uid
            stats.ownerZone = ""
            stats.objSize = stats.size
            (runCont ret stats) cont # need to fill in fields so that they match irods stats, but current we only need to know if file exits so it works

unlink = (path) -> (cont) !->
            fs.unlink path, (err) !->
                if err?
                    (runCont throwm {error: -1, errormsg: "can't delete file " + path}) cont
                else
                    (runCont ret null) cont

rmdir = (path) -> (cont) !->
            fs.rmdir path, (err) !->
                if err?
                    (runCont throwm {error: -1, errormsg: "can't delete directory " + path}) cont
                else
                    (runCont ret null) cont

mkdir = (path) -> (cont) !->
            fs.mkdir path, (err) !->
                if err?
                    (runCont throwm {error: -1, errormsg: "can't create directory " + path}) cont
                else
                    (runCont ret null) cont

rename = (srcPath, tgtPath) -> (cont) !->
            fs.rename srcPath, tgtPath, (err) !->
                (runCont if err?
                    throwm {error:-1, errormsg: "can't rename #srcPath to #tgtPath"}
                else
                    ret null) cont
create = (path) -> (cont) !->
            fs.open path, "ax", (err, fd) !->
                if err?
                    (runCont throwm {error:-1, errormsg: "can't create #path"}) cont
                else
                    fs.close fd, (err) !->
                        if err?
                            (runCont throwm {error:-1, errormsg: "can't close #path"}) cont
                        else
                            (runCont ret null) cont

module.exports = {rmdir, mkdir, create, rename, unlink, createReadStream, createWriteStream, readFile, writeFile, pipe, stat, readdir, open, close, read, write}
