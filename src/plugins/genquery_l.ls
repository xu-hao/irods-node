{header, cache} = require "./header_l"
fs = require "fs"

cachefn = "./genquery_const_l"
Constants = header "rodsGenQuery.hpp", cachefn, (x) ->
    if x.match /^COL_/
        x.substring 4
    else
        x


class Query
    # the following doesn't work
    # sel: []
    # whe: []
    # would end up in the prototype, not moved to instance until you update the variable
    # even if the object is mutable, causing multiple instances to share one mutable variable
    ->
        @sel = []
        @whe = []
    selecting: true
    select: (col, op = 1) ->
        @sel.push [col, op]
    where: (col, op, val) ->
        @whe.push [col, op, val]
    and: (col, op = 1, val = null) ->
        if val? then
            @whe.push [col, op, val]
        else
            @sel.push [col, op]

    build: (inp = {}) ->
        GenQueryInp_PI:
            maxRows: inp.maxRows ? Constants.MAX_SQL_ROWS
            continueInx: inp.continueInx ? 0
            partialStartIndex: inp.rowOffset ? 0
            options: inp.options ? 0
            KeyValPair_PI: inp.KeyValPair_PI ? { ssLen: 0 }
            InxIvalPair_PI:
                iiLen: @sel.length
                inx: [col for [col, op] in @sel]
                ivalue: [op for [col, op] in @sel]
            InxValPair_PI:
                isLen: @whe.length
                inx: [col for [col, op, val] in @whe]
                svalue: ["#op '#val'" for [col, op, val] in @whe]  # special characters

module.exports =
    {Constants, Query}

/*<<< Constants
    query = new Query
      ..select @DATA_NAME
      ..and @DATA_SIZE
      ..where @COLL_NAME, "=", "/tempZone/home/rods"

    console.dir query.build()*/
