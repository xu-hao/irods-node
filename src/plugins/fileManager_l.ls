{rec} = require "./arrays_l"
{bind, ret, throwm} = require "./monad_l"
FileManager = (fs, maxOpen) ->

    list = []
    
    map = {}
    
    extractFd = (fp) ->
        if fp.fd? then fp.fd else fp
        
    openFile = (path) -> do
        fp <- bind fs.open path, "r+"
        state = {
            path: path
            free: false
            fp: fp
            queue: []
        }
        list.push state
        map.{}[path][extractFd fp] = state
        ret fp
        
    closeFileAll = (path) -> do
        rec [ i for state, i in list when state.path == path ].reverse(), (i) -> closeFile i
        
    closeFile = (close) -> do
        state = list[close]
        list.splice close, 1
        mapval = map[state.path]
        delete mapval[extractFd state.fp]
        fs.close state.fp
        
    acquireFd = (path) ->
        # console.log path + list.length
        if map[path]? and (state = [ s for fd, s of map[path] when s.free ][0])?
            state.free = false
            ret state.fp
        else
            
            if list.length >= maxOpen
                close = [ i for state, i in list when state.free ][0]
                if close?
                    _ <- bind closeFile close
                    openFile path
                else
                    throwm {error: -1, errormsg: "max open file reached"}
            else
                openFile path
                

                
    releaseFd = (path, fp) ->
        map[path][extractFd fp].free = true
        ret null
        
        
    acquireFdDo = (path, act) ->
        fp <- bind acquireFd path
        x <- bind act fp
        _ <- bind releaseFd path, fp
        ret x
        
    closeAllFds = do
        rec list, (state) ->
            fs.close state.fp
        
    unlink = (path) ->
        mapval = map[path]
        _ <- bind rec [state for own fd, state of mapval], (state) ->
            if !state.free
                throwm {error:-1, errormsg: "cannot close file which is in use"}
            else
                closeFileAll state.path
        fs.unlink path
        
    {acquireFd, releaseFd, closeAllFds, acquireFdDo, unlink}

module.exports = FileManager
