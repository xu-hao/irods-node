{regex, drop, seq, alt, optional, reduce, seqIgnore, repeatIgnore} = require "packrattle"
{foldl} = require "./arrays_l"
#fs = require "fs"
separator = "////"
pp = "~"

serialize = (v0, n = 1) ->
    if v0 instanceof Object
        if v0.o? && v0.v?
            [v0.o] ++ serialize v0.v, n
        else
            list = []
            for p, v of v0
                list := list ++ ["#pp" * n + p]
                list := list ++ (serialize v, n+1)
            list
    else
        [v0]

reducer = (total, [p, [o, v]]) ->
    vv = if Array.isArray v
        #console.log [p, [o, v]]
        toObj v
    else
        v
    if o == ""
        total[p] = vv
    else
        total[p] = {o,v:vv}
    total

toObj = (arr) ->
    foldl reducer, {}, arr

parse = (params) ->

    str = params.join separator

    # path = (regex new RegExp("((?!#separator).)+")).onMatch (m) -> m[0] # cover most cases
    prop = (n) -> (regex new RegExp("#pp{#n}(?!#pp)((?!#separator).)*")).onMatch (m) -> m[0].substring n
    cmd = (regex /[a-zA-Z0-9]+/).onMatch (m) -> m[0]
    string = (regex new RegExp("(?!#pp)((?!#separator).)+")).onMatch (m) -> m[0]
    value = (n) -> alt string, -> object n+1
    cons = (n) -> seqIgnore separator, (optional alt "=", "like"), (value n)

    object = (n) ->
        repeatIgnore separator, (
            (seqIgnore separator, (prop n), (cons n))
        )

    cmdParser = object 1

    toObj cmdParser.run str

    # console.log "filtering result using criteria " + JSON.stringify cc

module.exports = {serialize, parse}
