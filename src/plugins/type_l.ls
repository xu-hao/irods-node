esprima = require "esprima"

CHECK_TYPE = false

class Disjunction
    (@pt, @rt) ->
class Conjunction
    (@pt, @rt) ->

###############################################
#    Utilities                                #
###############################################

originToMsg = (origin) ->
    msg = ""
    if origin?
        for ori, i in origin
            msg := msg + "\norigin[" + i + "]: " + ori[0] + " : " + (ori[1] $$$)
    msg

valToMsg = (obj) ->
    if typeof obj == "object"
        "{"+([prop + ": " + (if obj[prop] instanceof Function then "[Function]" else obj[prop]) for prop of obj].join ",")+"}"
    else
        obj + ""

$$$ = (ty) -> (toTypeChecker ty) $$$ # toStringToken

checkType = (type, typexception) -> (val, origin) ->
    if val == $$$
        type
    else
        if val? && ((type? && typeof val == type) || (typexception? && ((typexception instanceof Function && val instanceof typexception) || val.constructor.name == typexception)))
            # console.log val + " is a " + type
            val
        else
            msg = "not a(n) " + type + ": " + val?.constructor.name + "/" + val + "/" + (JSON.stringify val) + (originToMsg origin)
            throw new Error msg

___ = (val) -> # pass this to a function wrapper it passes back the wrapped function
   fval = val
   if !(fval + "").match /function/
       val
   else
       while true
           txt = "a = " + fval
           ast = esprima.parse(txt);
           wrapper = false
           for stmt in ast?.body[0]?.expression?.right?.body?.body ? []
               if stmt?.expression?.left?.name == "__wrapper__"
                   wrapper := true
           if !wrapper
               break
           fval := fval ___
       fval

# a wrapper function is defined by __wrapper__ = true as the first statement in the function
checkArity = (n) -> (val, origin) ->
    if !val instanceof Function
        msg = "unable to check arity for nonfunction: " + val?.constructor.name + " " + val + (originToMsg origin)
        throw new Error msg
    else
        fval = val
        if ("" + fval).match /functions\[0\][.]apply\(.*\)|\[key\][.]apply\(.*\)/
            val # the compose$ or bind$ function skip
        else
            fval = ___ val
            txt = "a = " + fval
            ast = esprima.parse(txt);
            if (ar = ast.body[0].expression.right.params.length) == n
                val
            else
                msg = "wrong arity #ar for function: #fval, expected #n" + (originToMsg origin)
                throw new Error msg

check =
    obj: (spec = {}) -> (val, origin) -> # use obj! for unspec'ed object
        if val == $$$
            if [prop for prop of spec].length == 0
                "obj!"
            else
                "obj { "+ ([prop + ": " +($$$ spec[prop]) for prop of spec].join ", ") + " }"
        else
            (checkType "object") val
            for prop, t in spec
                if val[prop]? then
                    val[prop] = (toTypeChecker t) val[prop], origin
                else
                    throw "property " + prop + " does not exist" + (originToMsg origin)
            val
    num: checkType "number", Number
    str: checkType "string", String
    bool: checkType "boolean", Boolean
    array: (spec = []) -> (val, origin) -> # use array! for unspec'ed array
        if val == $$$
            if Array.isArray spec
                if spec.length == 0
                    "array!"
                else
                    "array [ " + ([$$$ ty for ty in spec].join ", ") + " ]"
            else
                "array " + ($$$ spec)
        else
            if Array.isArray val
                if Array.isArray spec # tuple
                    for t, i in spec
                        val[i] = (toTypeChecker t) val[i], origin
                else # list
                    for v, i in val
                        val[i] = (toTypeChecker spec) v, origin
                val
            else
                throw new Error "not an array: " + val + (originToMsg origin)
    arrow: (pt, rt) -> (val, origin = []) ->
        # console.log "checking " + val + " : " + "(" + ([t $$$ for t in pt].join " x ") + " -> " + (rt $$$) + ")"
        if val == $$$
            "(" + (if pt.length == 0 then "unit" else ([t $$$ for t in pt].join " x ")) + " -> " + (rt $$$) + ")"
        else
            ff = ___ val
            v = (checkType "function", Function) ff, [[
                "checking function " + ff,
                -> "(" + ([t $$$ for t in pt].join " x ") + " -> " + (rt $$$) + ")"
            ]] ++ origin
            v = (checkArity pt.length) ff, [[
                "checking function " + ff,
                -> "(" + ([t $$$ for t in pt].join " x ") + " -> " + (rt $$$) + ")"
            ]] ++ origin
            fn = (...args) ->
                __wrapper__ = true
                if args.length == 1 && args[0] == ___
                    v
                else
                    if args.length != pt.length
                        throw new Error("wrong number of arguments for function " + ff + ", expected " + pt.length + " given " + args.length + (originToMsg origin))
                    nargs = []
                    for chk, i in pt
                        nargs.push chk args[i], [["parameter " + i + ": " + args[i] + " when calling function " + ff, chk]] ++ origin

                    retval = v.apply null, nargs
                    rt retval, [["return value when calling function " + ff, rt]] ++ origin
            fn

    disjunction: (at, bt) -> (val, origin) ->
        # console.log val + "" + origin
        if val == $$$
            "(" + (at $$$) + " U " + (bt $$$) + ")"
        else
            try
                at val, [["disjunction check " + val, ->(at $$$) + " U " + (bt $$$) ]] ++ origin
            catch
                bt val, [["disjunction check " + val, ->(at $$$) + " U " + (bt $$$) ]] ++ origin
    conjunction: (at, bt) -> (val, origin) ->
        # console.log val + "" + origin
        if val == $$$
            "(" + (at $$$) + " N " + (bt $$$) + ")"
        else
            at val, [["conjunction check " + val, ->(at $$$) + " N " + (bt $$$) ]] ++ origin
            bt val, [["conjunction check " + val, ->(at $$$) + " N " + (bt $$$) ]] ++ origin
    nul: (val, origin) ->
        if val == $$$ then
            "nul"
        else
            if val?
                throw new Error "not a null value: " + (valToMsg val) + (originToMsg origin)
            else
                val
    c: (cl) -> checkType cl.name, cl

checkArrow = (pt, rt) ->
    if rt?
        check.arrow pt, rt
    else
        pt

# give a type -> ... -> pt -> rt with 0 or more preceding arrows, extract the typechecer
extractTailType = (tailx) ->
    tailc = tailx()
    if tailc instanceof Function # maybe -> ... -> pt -> rt
        checkArrow [], extractTailType tailc
    else
        {pt, rt} = tailc
        checkArrow pt, rt

incomplete = (pt, rt) -> (tail) ->
    switch
    | tail instanceof Disjunction =>
        disj = (x) -> (checkArrow pt, rt) `check.disjunction` x
        if tail.rt?
              [h, ...t] = tail.pt
              incomplete [disj h] ++ t, tail.rt
        else
              incomplete disj tail.pt
    | tail instanceof Conjunction =>
        conj = (x) -> (checkArrow pt, rt) `check.conjunction` x
        if tail.rt?
              [h, ...t] = tail.pt
              incomplete [conj h] ++ t, tail.rt
        else
              incomplete conj tail.pt
    | tail? =>
        tailx = tail()
        if tailx instanceof Function # (#pt -> #rt) -> tailx
            incomplete [checkArrow pt, rt], extractTailType tailx
        else # (#pt -> #rt) x #tailpt -> #tailrt
            {pt:tailpt, rt:tailrt} = tailx
            incomplete ([checkArrow pt, rt] ++ tailpt), tailrt
    | _ => # (#pt -> #rt)
        {pt, rt}

toTypeCheckerConstructor = $$ = incomplete

toTypeChecker = extractTailType

ϵ = type = (val, ty) ->
    if CHECK_TYPE?
        (toTypeChecker ty) val
    else
        val

# Object.prototype.ϵ = Object.prototype.type = (type) -> type this, type

typeCheck = (obj) ->
    if CHECK_TYPE?
        for prop, val of obj when typedProp = (prop.match /^(.*)_type$/)
            p = typedProp[1]
            if obj[p]?
                #console.log obj[p] + " : " + (type $$$, val)
                #console.log val + ""
                try
                    res = (toTypeChecker val) $$$
                catch
                    throw new Error type + " is not a type specification"
                if typeof res != "string"
                    throw new Error type + " is not a type specification"

                obj[p] = (toTypeChecker val) obj[p]
                delete obj[prop]
            else
                console.error "warning: type specification " + prop + " doesn't match any property in object " + valToMsg(obj)
    obj

###############################################
#    Basic Operators                          #
#    arrow or -> : function type              #
#    unit : use in ( unit -> ... ) -> ...     #
#    x : product type                         #
#    bot : bottom type
#    U : union type                           #
#    a : any                                  #
#    N : intersection type                    #
###############################################
# put arrow instead of -> after ( ... ) to avoid parsing error

x = (ty) -> ty
arrow = (ty) -> -> ty

U = (tail) ->
    {pt, rt} = tail()
    new Disjunction pt, rt

N = (tail) ->
    {pt, rt} = tail()
    new Conjunction pt, rt

unit = (tail) ->
    $$ [], toTypeChecker tail()

a = $$ (val) -> if val == $$$ then "a" else val

bot = $$ (val) -> if val == $$$ then "bot" else throw "bot type should not be inhabited"

######################################
#   array!                           #
#   (array t)                        #
#   (array [t, ..., t])              #
#   obj!                             #
#   (obj {p:t, ..., p:t})            #
#   bool                             #
#   str                              #
#   num                              #
#   nul                              #
#   c : constructor                  #
######################################

bool = $$ check.bool
str = $$ check.str
num = $$ check.num
nul = $$ check.nul
array = $$ . check.array
obj = $$ . check.obj
c = $$ . check.c

module.exports = {
    arrow : arrow
    x : x
    U : U
    N : N
    a : a
    bot : bot
    str : str
    num : num
    array : array
    obj : obj
    bool: bool
    nul: nul
    c : c
    type : type
    typeCheck : typeCheck
    unit : unit
}
/*console.log "a -> a: "
console.log "" + (((x) -> x ) `ϵ` a -> a)
console.log "go"

console.log "a -> cont a, e: "
console.log "" + (((x) -> x ) `ϵ` a -> cont a, e)
console.log "go"

console.log "a -> (a -> a) -> a: "
console.log "" + (((x) -> x ) `ϵ` a -> (a -> a) arrow a)
console.log "go"
console.log "array U str -> array! U str: "
console.log "" + ((x)->x `type` array! U str -> array! U str)
console.log "go"
console.log "array U str -> array! U str: "
console.log "" + (fn = (->"str") `type` -> str)
console.log fn!
console.log "go"
console.log "array U str -> array! U str: "
console.log "" + ("str" `type` str U obj!)

console.log "go"*/
