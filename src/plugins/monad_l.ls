{a, arrow, x, obj, c, U, N, unit, typeCheck, type} = require "./type_l"

cont = (r, a) -> ( a -> r ) arrow r
e = (spec) -> (c Exception) U (c Success) N (obj {val: spec})

# Cont r a = ( a -> r ) -> r
# ret :: a -> Cont r a
# bind :: Cont r a -> (a -> Cont r b) -> Cont r b
# callCC :: ( ( a -> Cont r b ) -> Cont r a ) -> Cont r a
Cont =
    ret_type: a -> cont a, a
    ret: (x) -> (k) -> k x

    bind_type: (cont a, a) x (a -> cont a, a) arrow cont a, a
    bind: (sc, fc) -> (k) -> sc (x) -> (fc x) k

    # !!! bind has to be defined as a binary func in order for infix and backcall to work !!!
    # if Cont a = forall r. (a->r)->r, we can simply (sc fc) k
    # the reason we can simplify here is because forall r. makes
    # sc not able to do anything with the return value of fc except
    # passing it around, whereas without forall r. sc can analyze
    # the return value of (fc x) k
    callCC: (f) -> (k) -> f ((x) -> (_) -> k x ) k
    # to understand this notice
    # k :: a -> r i.e. k is the low-level continuation
    # to understand what (x) -> (_) -> k x does, look at bind
    # if we substitute (x) -> (_) -> k x for fc, we see that
    # it applies countinuation at the "callCC" site to x while
    # discarding the continuation at the "bind" site
    runCont_type: (cont a, a) arrow (a -> a) arrow a
    runCont: ((sc) -> (k) -> sc k)
    # this function is technically not necessary in JavaScript,
    # because we don't have to define a type isomorphism with a tag
    # however we keep this function here so that it is clear when we need to use it

typeCheck Cont
# ret :: a -> Cont (m r) a
# bind :: Cont (m r) a -> (a -> Cont (m r) b) -> Cont (m r) b
ContT = (m) -> m with
    ret:  Cont.ret
    bind: Cont.bind
    callCC: Cont.callCC

# ret :: a -> m (Cont r a)
# bind :: m (Cont r a) -> (a -> m (Cont r b)) -> m (Cont r b)
ContT2 = (m) -> m with
    ret:  m.ret . Cont.ret
    bind: (smc) -> (fmc) ->  smc `m.bind` (sc) -> sc fmc

# Exceptional e a = Success a | Exception e
class Exception
    (@msg) ->
class Success
    (@val) ->

Exceptional =
    ret: (x) -> new Success x
    bind: (se, fe) ->
        | se instanceof Exception => se
        | otherwise               => fe se.val
    throwm: (msg) ->
        # console.log("exception " + msg)
        new Exception msg
    catchm: (se) -> (h) ->
        | s instanceof Exception => h se.msg
        | otherwise              => se

ExceptionalT = (m) -> m with
    ret_type: a -> cont a, e a
    ret: m.ret . Exceptional.ret

    bind_type: (cont a, e a) x (a -> cont a, e a) arrow cont a, e a
    bind: (sem, fem) ->
        # if !fem then console.log("binding " + sem + " to undefined action"); abort
        sem `m.bind` (se) ->
            | se instanceof Exception =>
                #console.log sem + "error with exception "
                #console.dir se
                m.ret se
            | otherwise               =>
                #console.log sem + "finished with retval "
                #console.dir se.val
                fem se.val

    throwm_type: a -> cont a, e a
    throwm: m.ret . Exceptional.throwm

    catchm_type: (cont a, e a) arrow (a -> cont a, e a) arrow cont a, e a
    catchm: (sem) -> (h) ->
        sem `m.bind` (se) ->
            | se instanceof Exception => h se.msg
            | otherwise               => m.ret se

# ContTExceptionalT = ContT . ExceptionalT

# Cont r (Exceptional a)
# ret :: Cont r (Exceptional a)
# bind :: Cont r (Exceptional a) -> ( a -> Cont r ( Exceptional b ) ) -> Cont r ( Exceptional b )

ExceptionalTCont = ExceptionalT Cont
typeCheck ExceptionalTCont

#lazy_type = (unit -> (cont a, e a)) arrow (cont a, e a)
lazy = (action) -> (cont) !-> (ExceptionalTCont.runCont action()) cont

newStack = (act) -> (cont) !->
    setImmediate -> (ExceptionalTCont.runCont act) cont

module.exports = ExceptionalTCont <<< {
    cont : cont
    e : e
    lazy : lazy
    newStack: newStack
}
