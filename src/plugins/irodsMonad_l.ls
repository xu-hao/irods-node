{Query, Constants:C} = require "./genquery_l"
{Constants:A} = require "./api_l"
{bind, ret, throwm, catchm, lazy, runCont} = require "./monad_l"
{transpose, mapM} = require "./arrays_l"
ConnectionManager = require "./connectionManager_l"
{Constants:E} = require "./error_l"
{htonl} = require "network-byte-order"

irodsMonad = (prot, trans, rodsEnv, auth, rodsCred) ->
    # bidirectional mapping from conanical prop names to query indices
    # use pack struct name, if pack struct name exists

    connectionManager = new ConnectionManager prot, trans, rodsEnv, auth, rodsCred
    getResults = (msg, cols, n = 0) ->
        (catchm (do
            retmsg <- bind connectionManager.connectionDo (conn) ->
                conn.api A.GEN_QUERY_AN, msg
            mat = [col.value for col in retmsg.msg.GenQueryOut_PI.SqlResult_PI[0 til cols.length]]
            ret [((parseInt retmsg.msg.GenQueryOut_PI.continueInx) == 1), (transpose mat)]
            
        )) (err) ->
            if err.error == E.CAT_NO_ROWS_FOUND
                # not an error
                ret [false, []]
            else
                throwm err

    assemble = (def, mod) ->
        prop = [p for p of def][0]
        def[prop] <<< mod
        def

    DataObjInp_PI_Obj = (mod) ->
        obj =
          DataObjInp_PI:
            objPath: null
            createMode: 0
            openFlags: 2
            offset: 0
            dataSize: 0
            numThreads: 0
            oprType: 0 # put_opr
            KeyValPair_PI:
                ssLen: 0
        assemble obj, mod


    query = recursive = (cols, conds, offset = 0) ->
        # console.log cols + conds
        query = new Query
        for col in cols
            query.select col
        for [col, op, val] in conds
            query.where col, op, val
        msg = query.build do
            rowOffset: offset
            continueInx: if offset == 0 then 0 else 1
        [cont, mat] <- bind getResults msg, cols
        tail <- bind if cont
            recursive cols, conds, (offset + mat.length) 
        else
            ret []
        ret (mat ++ tail)

    ModAVUMetadataInp_PI_Obj = (mod) ->
        obj =
          ModAVUMetadataInp_PI:
            arg0: ""
            arg1: ""
            arg2: ""
            arg3: ""
            arg4: ""
            arg5: ""
            arg6: ""
            arg7: ""
            arg8: ""
            arg9: ""

        assemble obj, mod


    KeyValPair_PI_Obj = (obj) ->
        # console.log "aaa"
        mat = [[k, v] for k, v of obj]
        if mat.length == 0
            [kw, sv] = [[],[]]
        else
            [kw, sv] = transpose mat
        msg =
            KeyValPair_PI:
                ssLen: kw.length
                keyWord: kw
                svalue: sv
    DataObjCopyInp_PI_Obj = (srcInp, destInp) -> {
        
        DataObjCopyInp_PI: 
            DataObjInp_PI: [srcInp.DataObjInp_PI, destInp.DataObjInp_PI]
    }

    
    GeneralAdminInp_PI_Obj = (mod) ->
        obj = 
            generalAdminInp_PI:
                arg0:""
                arg1:""
                arg2:""
                arg3:""
                arg4:""
                arg5:""
                arg6:""
                arg7:""
                arg8:""
                arg9:""
                
        assemble obj, mod
    OpenedDataObjInp_PI_Obj = (mod) ->
        obj = OpenedDataObjInp_PI:
            l1descInx: 0
            len: 0
            whence: 0
            oprType: 0
            offset: 0
            bytesWritten: 0
            KeyValPair_PI:
                ssLen: 0
        assemble obj, mod
        
    stat = (dirname) -> do
        connectionManager.connectionDo (conn) ->
            msg = DataObjInp_PI_Obj {
                objPath: dirname
            }
            # console.log msg
            retmsg <- bind conn.api A.OBJ_STAT_AN, msg
            # console.log retmsg
            ret retmsg.msg.RodsObjStat_PI

    rename = (src, tgt, oprType) ->
        {path:tgtPath} <- bind tgt.readProp "refobj"
        {path:srcPath} <- bind src.readProp "refobj"
        srcInp = DataObjInp_PI_Obj {
            objPath: srcPath
        }
        destInp = DataObjInp_PI_Obj {
            objPath: tgtPath
            oprType: oprType # rename data object
        }
        msg =
            DataObjCopyInp_PI:
                DataObjInp_PI: [srcInp.DataObjInp_PI, destInp.DataObjInp_PI]
        # console.log JSON.stringify msg
        connectionManager.connectionDo (conn) ->
            conn.api A.DATA_OBJ_RENAME_AN, msg

    modAVU = (obj, oper, rodsDataType, val, val2 = null) ->
            readOptionalProp = (val, prop, dec) -> if val?
                aa <- bind val.readProp prop
                ret if aa?
                    "#dec#aa"
                else
                    ""
            else
                ret ""
            objref <- bind obj.readProp "refobj"
            a <- bind val.readProp "attribute"
            v <- bind val.readProp "value"
            u <- bind readOptionalProp val, "unit", ""
            a2 <- bind readOptionalProp val2, "attribute", "n:"
            v2 <- bind readOptionalProp val2, "value", "v:"
            u2 <- bind readOptionalProp val2, "unit", "u:"

            path = objref.path
            msg = ModAVUMetadataInp_PI_Obj {
                arg0: oper #""
                arg1: rodsDataType #""
                arg2: path
                arg3: a
                arg4: v
                arg5: u
                arg6: a2
                arg7: v2
                arg8: u2
            }
            _ <- bind connectionManager.connectionDo (conn) ->
                conn.api A.MOD_AVU_METADATA_AN, msg
            ret val

    create = (path) ->
        msg = DataObjInp_PI_Obj do
            objPath: path
            openFlags: 2
        retmsg <- bind connectionManager.connectionDo (conn) ->
              conn.api A.DATA_OBJ_CREATE_AN, msg
        l1descInx = parseInt retmsg.header.MsgHeader_PI.intInfo

        msg2 = OpenedDataObjInp_PI_Obj do
                l1descInx: l1descInx
        connectionManager.connectionDo (conn) ->
              conn.api A.DATA_OBJ_CLOSE_AN, msg2
              
    # spawn n "threads"          
    spawn = (list, f) -> (cont) !->
        len = list.length
        results = [null] * len
        contThread = (i) -> (x) -> 
            results[i] = x
            len--
            if len == 0
                (runCont mapM results, (r) ->
                    (k) -> k r) cont
        for el, i in list
            (runCont f el) (contThread i)
            
    readUInt64 = (sock) ->
        h <- bind sock.readUInt32
        if h != 0
            throwm {error: -1, errormsg: "integer too large"}
        else
            sock.readUInt32
            
            
    put = (srcBuf, srcPath, o, tgtPath, numThreads = -1) ->
        keyWord = [
            "dataType"
            "defRescName"
            "fileUid"
            "fileGid"
            "fileOwner"
            "fileGroup"
            "fileMode"
            "fileCtime"
            "fileMtime"
            "fileSourcePath"
            "forceFlag" # always update content
        ] ++ if numThreads == -1 then ["dataIncluded"] else []
        svalue = [
            "generic"
            rodsEnv.defaultResc
            o.uid
            o.gid
            "" # set to empty until we've found a way to get it
            "" # set to empty until we've found a way to get it
            o.mode
            o.ctime.getTime()
            o.mtime.getTime()
            srcPath
            ""
        ] ++ if numThreads == -1 then [""] else []
        msg =
            DataObjInp_PI_Obj do
                objPath: tgtPath
                openFlags: 2
                dataSize: srcBuf.length
                oprType: 1 # put_opr
                numThreads: numThreads
                KeyValPair_PI:
                    ssLen: keyWord.length
                    keyWord: keyWord
                    svalue: svalue
        if numThreads == -1 # no threading, append buffer
            connectionManager.connectionDo (conn) ->
                conn.api A.DATA_OBJ_PUT_AN, msg, srcBuf
        else
            connectionManager.connectionDo (conn) ->
                retval <- bind conn.api A.DATA_OBJ_PUT_AN, msg
                console.log JSON.stringify retval
                {msg: PortalOprOut_PI: portal} = retval
                
                sendOprComplete = (st) ->
                    msg = INT_PI: myInt: st 
                    conn.api A.OPR_COMPLETE_AN, msg
                
                numThreads2 = parseInt portal.numThreads
                if numThreads != 0 and numThreads2 != numThreads
                    console.log "warning: server returned a different number of threads from requested, #numThreads != #numThreads2"
                    
                hdlr = (e) -> 
                    _ <- sendOprComplete e.error
                    throwm e
                    
                (catchm (
                    _ <- bind if numThreads2 <= 0 # no threading just do a write using the same connection
                        write {fd: portal.l1descInx, conn: conn}, srcBuf, 0, srcBuf.length
                        
                    else # multithread
                        
                        spawn [0 til numThreads2], (i) ->
                            console.log "writing thread #i, connecting to #{portal.PortList_PI[0].hostAddr}:#{portal.PortList_PI[0].portNum}"
                            sock = trans do
                                host: portal.PortList_PI[0].hostAddr
                                port: parseInt portal.PortList_PI[0].portNum
                            _ <- bind sock.establish # ignore windowSize for now
                            
                            buf = new Buffer 4
                            htonl buf, 0, portal.PortList_PI[0].cookie
                            _ <- bind sock.writeBuf buf
                            
                            oprType <- bind sock.readUInt32
                            flags <- bind sock.readUInt32
                            offset <- bind readUInt64 sock
                            length <- bind readUInt64 sock
                            console.log "#offset | #length"
                            _ <- bind sock.writeBuf srcBuf.slice offset, (offset + length)
                            
                            sock.destroy
                    sendOprComplete portal.l1descInx)) hdlr
                        
            
    get = (srcPath, objSize) ->
        msg = DataObjInp_PI_Obj {
            objPath: srcPath
            dataSize: objSize
            oprType: 2 # get_opr
            numThreads: -1 # currently only support single thread mode
        }
        # console.log JSON.stringify msg
        connectionManager.connectionDo (conn) ->
             conn.api A.DATA_OBJ_GET_AN, msg

    cleanup = connectionManager.closeAllConnections
    
    trim = (path, rescName) ->
        repl <- bind query [C.DATA_REPL_NUM], [[C.D_RESC_NAME, "=", rescName]]
        
        [[replNum]] <- bind if repl.length != 0 then ret repl else throwm {error: -1, errormsg: "replica #path doesn't exist on resource #rescName"}
        kvp = KeyValPair_PI_Obj {
            replNum: replNum
        } 
        msg =
            DataObjInp_PI_Obj { 
                objPath: path
                KeyValPair_PI: kvp.KeyValPair_PI
            }
        connectionManager.connectionDo (conn) ->
            conn.api A.DATA_OBJ_UNLINK_AN, msg
    
    
    repl = (path, rescName) ->
        kvp = KeyValPair_PI_Obj {
            destRescName: rescName
        }
        msg = DataObjInp_PI_Obj {
            objPath: path
            KeyValPair_PI: kvp.KeyValPair_PI
            oprType: 6
            openFlags: 0
            createMode: 0
        }
        connectionManager.connectionDo (conn) ->
            conn.api A.DATA_OBJ_REPL_AN, msg

    trim2 = (path, replNum, copies) ->
        
        kvp = KeyValPair_PI_Obj {
            replNum: replNum
            copies: copies
        }
        msg = DataObjInp_PI_Obj {
            objPath: path
            KeyValPair_PI: kvp.KeyValPair_PI
            openFlags: 0
        }
        connectionManager.connectionDo (conn) ->
            conn.api A.DATA_OBJ_TRIM_AN, msg

    rmdir = (path, recursive = false) ->
        opts = if recursive
            recursiveOpr: ""
        else
            {}
        kvp = KeyValPair_PI_Obj opts
        msg =
            CollInpNew_PI:
                collName: path
                flags: 0
                oprType: 0
                KeyValPair_PI:
                    kvp.KeyValPair_PI

        connectionManager.connectionDo (conn) ->
            conn.api A.RM_COLL_AN, msg


    mkdir = (path) ->
        msg =
            CollInpNew_PI:
                collName: path
                flags: 0
                oprType: 0
                KeyValPair_PI:
                    ssLen: 0
        connectionManager.connectionDo (conn) ->
              conn.api A.COLL_CREATE_AN, msg

    unlink = (path) ->
        msg =
            DataObjInp_PI_Obj { objPath: path }
        connectionManager.connectionDo (conn) ->
            conn.api A.DATA_OBJ_UNLINK_AN, msg

    cp = (srcPath, tgtPath) ->
        srcInp = DataObjInp_PI_Obj {
            objPath: srcPath
            dataSize: -1
        }
        
        destInp = DataObjInp_PI_Obj {
            openFlags: 1
            objPath: tgtPath
            oprType: 9
        }     
        
        msg = DataObjCopyInp_PI_Obj srcInp, destInp

        connectionManager.connectionDo (conn) ->
            conn.api A.DATA_OBJ_COPY_AN, msg

    createResource = (rescName, rescType, rescHost, rescPath, contextString = "", zone = rodsEnv.clientRcatZone) ->
        msg =
            GeneralAdminInp_PI_Obj do
                arg0:"add"
                arg1:"resource"
                arg2:rescName
                arg3:rescType
                arg4:rescHost
                arg5:rescPath
                arg6:contextString
                arg7:zone
        _ <- bind connectionManager.connectionDo (conn) ->
            conn.api A.GENERAL_ADMIN_AN, msg
        connectionManager.closeAllConnections # close all connections refresh, this bug should be fixed in irods

    deleteResource = (rescName) ->
        msg =
            GeneralAdminInp_PI_Obj do
                arg0:"rm"
                arg1:"resource"
                arg2:rescName
        connectionManager.connectionDo (conn) ->
            conn.api A.GENERAL_ADMIN_AN, msg

    modifyResourceDataPath = (rescName, oldpath, newpath, user = "") ->
        msg =
            GeneralAdminInp_PI_Obj do
                arg0:"modify"
                arg1:"resourcedatapaths"
                arg2:rescName
                arg3:oldpath
                arg4:newpath
                arg5:user
        connectionManager.connectionDo (conn) ->
            conn.api A.GENERAL_ADMIN_AN, msg

    modifyResource = (rescName, option, newvalue) ->
        msg =
            GeneralAdminInp_PI_Obj do
                arg0:"modify"
                arg1:"resource"
                arg2:rescName
                arg3:option
                arg4:newvalue
        connectionManager.connectionDo (conn) ->
            conn.api A.GENERAL_ADMIN_AN, msg
            
    createUser = (userName, userType) ->
        msg =
            GeneralAdminInp_PI_Obj do
                arg0:"add"
                arg1:"user"
                arg2:userName
                arg3:userType
        connectionManager.connectionDo (conn) ->
            conn.api A.GENERAL_ADMIN_AN, msg

    deleteUser = (userName) ->
        msg =
            GeneralAdminInp_PI_Obj do
                arg0:"rm"
                arg1:"user"
                arg2:userName
        connectionManager.connectionDo (conn) ->
            conn.api A.GENERAL_ADMIN_AN, msg


    modifyUser = (userName, option, newvalue) ->
        msg =
            GeneralAdminInp_PI_Obj do
                arg0: "modify"
                arg1: "user"
                arg2: userName
                arg3: option
                arg4: newvalue
                
        connectionManager.connectionDo (conn) ->
            conn.api A.GENERAL_ADMIN_AN, msg
            
            
    open = (path) ->
        msg = 
            DataObjInp_PI_Obj do
                objPath: path
                openFlags: 2
        conn <- bind connectionManager.acquireConnection
        
        hdlr = (e) ->
            _ <- bind connectionManager.releaseConnection conn
            throwm e
        
        (catchm (
            retmsg <- bind conn.api A.DATA_OBJ_OPEN_AN, msg
            ret {
                fd: parseInt retmsg.header.MsgHeader_PI.intInfo 
                conn: conn
            })) hdlr
        
    close = (fp) ->
        msg =
            OpenedDataObjInp_PI_Obj do
                l1descInx: fp.fd
        _ <- bind fp.conn.api A.DATA_OBJ_CLOSE_AN, msg
        connectionManager.releaseConnection fp.conn
            
    seek = (fp, pos) ->
        msg =
            OpenedDataObjInp_PI_Obj do
                l1descInx: fp.fd
                whence: 0
                offset: pos
        fp.conn.api A.DATA_OBJ_LSEEK_AN, msg
            
    read = (fp, start, end) ->
        _ <- bind seek fp, start
        msg = 
            OpenedDataObjInp_PI_Obj do
                l1descInx: fp.fd
                len: end - start
        {bs} <- bind fp.conn.api A.DATA_OBJ_READ_AN, msg
        ret (bs || new Buffer 0)
        
    
    write = (fp, start, end, buf) ->
        _ <- bind seek fp, start
        msg = 
            OpenedDataObjInp_PI_Obj do
                l1descInx: fp.fd
                len: end - start
        fp.conn.api A.DATA_OBJ_WRITE_AN, msg, buf
            
    {
        mkdir, unlink, create, put, get, rmdir, modAVU, rename, stat, query, cp, cleanup, trim2, repl, createResource, deleteResource, modifyResource, modifyResourceDataPath,
        createUser, deleteUser, modifyUser, trim, open, close, seek, read, write
    }


module.exports = irodsMonad
