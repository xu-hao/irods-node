fs = require "fs"
regex = /#define\s+([a-zA-Z_]+)\s+([^\s]+)/g
numR = /-?[0-9]+/g
strR = /\"([^\"]*)\"/g

parse = (fn, cachefn0, attr_t = (x)->x, val_t = (x)->x) ->
  cachefn = "#__dirname/" + cachefn0 + ".ls"
  cachefnjs = "#__dirname/" + cachefn0 + ".js"
  if (fs.existsSync cachefn) || (fs.existsSync cachefnjs)
      try
          require cachefn0
      catch
          console.log "warning: cache found but error loading"
          parse2 fn, cachefn, attr_t, val_t
  else
      console.log "cache not found, load from header file"
      parse2 fn, cachefn, attr_t, val_t

parse2 = (fn, cachefn, attr_t = (x)->x, val_t = (x)->x) ->
      data = fs.readFileSync fn, encoding: "utf8"
      ret = {}
      while (matc = regex.exec data)
          [full, attr, val] = matc
          # console.log "#attr = #val"
          attr = attr_t attr
          val = val_t val
          ret[attr] =
              if val.match numR
                  parseInt val
              else if val.match strR
                  strR.exec val
              else
                  val
      cache ret, cachefn
      ret

cache = (obj, fn) ->
    src = '''
module.exports =

'''
    for a, b of obj
        src += "  #a: " + (if typeof b == "number" then b else "\""+b.replace("#","\\#")+"\"") + "\n"
    fs.writeFileSync(fn, src);

module.exports =
    header: parse
    cache: cache
