{bind, ret, throwm, catchm, runCont, lazy, e, cont} = require "./monad_l"
{x, a, typeCheck, obj, unit, nul, arrow, type, str, array} = require "./type_l"
{ObjType} = require "./const_l"
{Rules, CompositeProp, ImmutableVolatileObjRule, ImmutableVolatilePropRule, Obj, StructuralObj, Prop, ArrayProp, CacheProp, CascadingProp, ArrayStructuralProp, ArrayBackedContent} = require "./metamodel_l"
{rec, mapM} = require "./arrays_l"
fsMonad = require "./fsMonad_l"
irodsMonad = require "./irodsMonad_l"
irodsMonadTools = require "./irodsMonadTools_l"
url = require "url"
{Constants:E} = require "./error_l"
FileManager = require "./fileManager_l"
log = (require "debug")("plugins/model_l.ls")

ArrayContentType =
    getObj_type: cont a, e obj!
    setObj_type: obj! -> cont a, e nul
    type_type: str
    path_type: cont a, e str
    ref_type: cont a, e str
    pack_type: cont a, e array [str, str]
    getContent_type: cont a, e array!

ObjectTypeTable =
    "irods:":
        IndividualObject: "DataObject"
        CollectiveObject: "Collection"
    "file:":
        IndividualObject: "File"
        CollectiveObject: "Directory"

determineAdaptiveObjectTypeFromSrc = (o, tgt) ->
    {protocol} <- bind tgt.readProp "refobj"
    if (subtable = ObjectTypeTable[protocol])?
        tt = [t2 for t, t2 of subtable when o.instanceOf t]
        if tt.length > 0
            if tt.length > 1
                throwm {error:-1, errormsg: "ambiguous content type " + o.type}
            else
                ret tt[0]
        else
            throwm {error:-1, errormsg: "unsupported content type " + o.type}

    else
        throwm {error:-1, errormsg: "unsupported content object protocol " + tgt.ref}

model = (prot, trans, rodsEnv, auth, rodsCred) ->

    rules = Rules()
    irods = irodsMonad prot, trans, rodsEnv, auth, rodsCred
    irodsTools = irodsMonadTools irods
    fs = fsMonad

    #################################
    #   generic code                #
    #################################

    BufferContent = (buf) !->
        buf0 = buf
        @type = "BufferContent"
        @getObj = ret rules.object {
            type: "IndividualObject"
        }
        
        @read = (start, end) -> lazy ->
            buf = new Buffer (end - start)
            buf0.copy buf, 0, start, end
            ret buf
            
        @write = (start, end, buf) -> lazy ->
            buf.copy buf0, start, 0, end - start
            ret null
            
        @readFully = lazy -> ret buf0
        @writeFully = (buf2) -> lazy ->
            buf0 := new Buffer buf2
            ret null
            
    # an object that contains and array of objects to be retargeted when retrieved
    # this is useful the lazy retargeting
    RetargetedContent = (cnt, srcBasePath, tgtBasePath) !->
        @type = "RetargetedContent"
        @getContent = do
            arr <- bind cnt.getContent
            mapM arr, changePath srcBasePath, tgtBasePath

    AbstractObjectBackedContent = (obj) !->
        @getObj = ret obj
        @path = path = do
            {path} <- bind obj.readProp "refobj"
            ret path
        @objref = objref = do
            ret obj.ref
        @pack = do
            ref <- bind objref
            path <- bind path
            ret [ref, path]

                
    fileManager = FileManager fs, 10
    dataObjectManager = FileManager irods, 10
    
    FileContent = (obj) !->
        this <<< new AbstractObjectBackedContent obj
        path = @path
        @type = "FileContent"
        @readStream = do
            p <- bind path
            fs.createReadStream p
        @writeStream = do
            p <- bind path
            fs.createWriteStream p
        @readFully = do
            p <- bind path
            #console.log "reading from #p"
            fs.readFile p
            
        @writeFully = (buf) -> do
            #console.log buf.toString()
            p <- bind path
            fs.writeFile p, buf
            
        @read = (start, end) -> do
            p <- bind path
            fileManager.acquireFdDo p, (fd) -> fs.read fd, start, end
            
        @write = (start, end, buf) -> do
            p <- bind path
            fileManager.acquireFdDo p, (fd) -> fs.write fd, start, end, buf
            
    RangeContent = (fullContent, start, end) !->
        translate = (s, e, startr, endr) ->
            starta = s + startr
            enda = s + endr
            if enda < starta or starta < s or enda < s or starta > e or enda > e
                throwm {error: -1, errormsg: "subrange [#startr, #endr] is out of range of [#s, #e]"}
            else
                ret [starta, enda]
                
        @type = "RangeContent"
        @readStream = do
            throwm {error: -1, "readStream for RangeContent has not been implemented yet"}
        @writeStream = do
            throwm {error: -1, "writeStream for RangeContent has not been implemented yet"}
        @readFully = do
            fullContent.read start, end
        @writeFully = (buf) -> do
            fullContent.write start, end, buf
        @read = (startr, endr) -> do
            [starta, enda] <- bind translate start, end, startr, endr
            fullContent.read starta, enda
        @write = (startr, endr, buf) -> do
            if buf.length() != endr - startr
                throwm {error: -1, "buffer size doesn't match range"}
            else
                [starta, enda] <- bind translate start, end, startr, endr
                fullContent.write buf, starta, enda
        

    DirectoryContent = (obj, criteria) !->
        this <<< new AbstractObjectBackedContent obj
        pack = @pack
        @type = "DirectoryContent"
        @getContent = do
            [ref, path] <- bind pack
            files <- bind fs.readdir path
            arr = [rules.object {
                ref: ref + "/" + file
                type: "AdaptiveObject"
            } for file in files when !criteria?.name? || file == criteria.name]
            _ <- bind rec arr, (o) -> o.exists # stat to get the exact type of each object
            ret arr

    DataObjectContent = (obj) !->
        this <<< new AbstractObjectBackedContent obj
        path = @path
        @type = "DataObjectContent"
        @readFully = do
            p <- bind path
            {bs} <- bind irods.get p, 0
            ret (bs || new Buffer 0)
        @writeFully = (buf, srcPath = "", uid, gid, mode, ctime, mtime) ->
            p <- bind path
            irods.put buf, srcPath, {
                uid: uid ? 0
                gid: gid ? 0
                mode: mode ? 0
                ctime: (ctime ? new Date)
                mtime: (mtime ? new Date)
            }, p
        @read = (start, end) -> do
            p <- bind path
            dataObjectManager.acquireFdDo p, (fp) -> irods.read fp, start, end
        @write = (start, end, buf) -> do
            p <- bind path
            dataObjectManager.acquireFdDo p, (fp) -> irods.write fp, start, end, buf

    CollectionContent = (obj, criteria) !->
        this <<< new AbstractObjectBackedContent obj
        path = @path
        @type = "CollectionContent"
        @getContent = do
            p <- bind path
            res <- bind irodsTools.readdir p, criteria
            ret rules.objectDeep res

    DataObjectAVUContent = (obj, criteria) !->
        this <<< new AbstractObjectBackedContent obj
        path = @path
        @type = "DataObjectAVUContent"
        @getContent = do
            p <- bind path
            res <- bind irodsTools.readDataObjectAVU p, criteria
            ret rules.objectDeep res

    CollectionAVUContent = (obj, criteria) !->
        this <<< new AbstractObjectBackedContent obj
        path = @path
        @type = "CollectionAVUContent"
        @getContent = do
            p <- bind path
            res <- bind irodsTools.readCollectionAVU p, criteria
            ret rules.objectDeep res

    ###########################
    # essential operators     #
    ###########################

    typeOf = (sval) -> if ! sval? then "_" else if ! sval.type? then typeof sval else sval.type

    move = (src, sprop, sval, tgt, tprop, tval) -> lazy ->
        try
            (rules.lookupOperRule [
                src.type
                sprop
                typeOf sval
                tgt.type
                tprop
                typeOf tval
            ], "move") src, sprop, sval, tgt, tprop, tval
        catch e
            throwm {error: -1, errormsg: "cannot find move rule for object type #{sval.type} and #{tval.type}" + e}

    copy = (src, sprop, sc, tgt, tprop, tc) -> lazy ->
        sval <- bind if src?
            src.readProp2 sprop, sc
        else
            ret sc
        tval <- bind if tgt?
            tgt.sink tprop, tc
        else
            ret tc
        try 
            (rules.lookupOperRule [sval.type, tval.type], "transfer") sval, tval
        catch e
            throwm {error: -1, errormsg: "cannot find transfer rule for object type #{sval.type} and #{tval.type}" + e}

    read = (obj, prop, val) -> lazy ->
        (rules.getPropRule obj, prop).read obj, prop, val
    ###########################
    # transfer utilities      #
    ###########################

    retargetContent = (cnt, base1, base2) -> new RetargetedContent cnt, base1, base2

    transferContent = (srcCnt, prop, tgt) ->
        copy null, null, srcCnt, tgt, prop, null

    moveObj = (sval, tval) ->
        src = tgt =
            type: "CollectiveObject"
        move src, "content", sval, tgt, "content", tval

    # An adaptive object changes its type based on refobj.protocol and content.type
    changePath = (base1, base2) -> (o) ->
        {path} <- bind o.readProp "refobj"
        n = path.substring base1.length
        tgtPath = base2 + n
        if tgtPath == path # if path is already set properly, then don't create new object
            ret o
        else
            content <- bind o.readProp "content"
            metadata <- bind o.readProp "metadata"
            ret rules.object (({} <<< o) <<< {
                ref: tgtPath
                refobj: null
                type: "AdaptiveObject"
                content: content
                metadata: metadata # this is null for files but it's ok since array properties treat null as []
            })

    # retarget objs as a part of objt
    retargetUnder = (src, tgt) ->
        {path:srcPath} <- bind src.readProp "refobj"
        base1 = srcPath.substring 0, (srcPath.lastIndexOf "/")
        base2 = tgt.ref
        (changePath base1, base2) src

    retargetAt = (src, tgt) ->
        {path:srcPath} <- bind src.readProp "refobj"
        base1 = srcPath
        base2 = tgt.ref
        (changePath base1, base2) src

    ################################
    #  rules                       #
    ################################


    pathObjRule = Obj rules, StructuralObj {
        eq: (obja, objb) -> obja.type == objb.type && obja.ref == objb.ref
        exists: (obj) -> lazy ->
            (catchm (
                refobj <- bind obj.readProp "refobj"
                switch refobj.protocol
                | "file:" =>
                    stat <- bind fs.stat refobj.path
                    obj <<< stat
                    obj.type = if stat.isFile() then
                        "File"
                    else if stat.isDirectory() then
                        "Directory"
                    else
                        console.log "warning: unrecognized obj type for " + obj.ref
                        obj.type # unrecognized object type
                    ret true
                | "irods:" =>
                    stat <- bind irods.stat refobj.path
                    obj <<< stat
                    obj.type = switch parseInt stat.objType
                    | ObjType.DATA_OBJECT.id => "DataObject"
                    | ObjType.COLLECTION.id => "Collection"
                    | _ =>
                        console.log "warning: unrecognized obj type for " + obj.ref
                        obj.type # do not change type if we don't recognized the objType
                    ret true
                | _ =>
                    throwm {error: -1, errormsg: "unsupported protocol " + that}
            )) (e) ->
                if e.error == E.OBJ_PATH_DOES_NOT_EXIST || e.error == E.USER_FILE_DOES_NOT_EXIST
                    ret false
                else
                    throwm e
    }

    pathObjectRefParsePropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null # create data object, if the content property exists, then do an iput instead
        del: (obj, prop, val) ->  ret null # remove data object
        read: (obj, prop, criteria) ->
            obj[prop] = url.parse (encodeURI obj.ref), true # ref should always exist
            obj[prop].path = decodeURI obj[prop].pathname
            if !obj[prop].protocol
                obj[prop].protocol = "irods:" # default to irods
            ret obj[prop]
        update: (obj, prop, diff) -> ret null # move data object to a new collection
    }
    
    
    pathObjectStatPropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null # stat field cannot be created
        del: (obj, prop, val) -> ret null # stat field cannot be deleted
        read: (obj, prop, criteria) -> do
            _ <- bind obj.exists # obj.exists should fill in all the stats
            ret obj[prop]
        update: (obj, prop, diff) -> # some stat field can be modified others can't
    }

    adaptiveObjectRefPropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret val # create data object, if the content property exists, then do an iput instead
        del: (obj, prop, val) -> ret null # remove data object
        read: (obj, prop, criteria) -> ret null
        update: (obj, prop, diff) -> # move data object to a new collection
            tgt = rules.object {
                ref: diff.nval
                type: "AdaptiveObject"
            }
            tgt.type <- bind determineAdaptiveObjectTypeFromSrc obj, tgt
            moveObj obj, tgt
    }

    extract1By1MatrixElement = (arr) ->
        if arr.length != 1 
            throwm {error: -1, errormsg: "wrong number of results"}
        else
            ret arr[0][0]

    dataObjectQueryPropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null # stat field cannot be created
        del: (obj, prop, val) -> ret null # stat field cannot be deleted
        read: (obj, prop, criteria) ->
            arr <- bind irodsTools.readDataObjectProp obj, prop, criteria
            extract1By1MatrixElement arr
        update: (obj, prop, diff) -> # some stat field can be modified others can't
    }

    collectionQueryPropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null # stat field cannot be created
        del: (obj, prop, val) -> ret null # stat field cannot be deleted
        read: (obj, prop, criteria) ->
            arr <- bind irodsTools.readCollectionProp obj, prop, criteria
            extract1By1MatrixElement arr
        update: (obj, prop, diff) -> # some stat field can be modified others can't
    }

    pathObjectContentPropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null # throwm {error:-1, errormsg: "unsupported object type for create: " + obj.type + "." + prop + " " + obj.ref}
        del: (obj, prop, val) -> ret null # throwm {error:-1, errormsg: "unsupported object type for del: " + obj.type + "." + prop + " " + obj.ref}
        read: (obj, prop, criteria) -> ret null #throwm {error:-1, errormsg: "unsupported object type for read: " + obj.type + "." + prop + " " + obj.ref}
        update: (obj, prop, diff) -> ret null #throwm {error:-1, errormsg: "unsupported object type for update: " + obj.type + "." + prop + " " + obj.ref}
        sink: (obj, prop, criteria = null) -> ret null #throwm {error:-1, errormsg: "unsupported object type for sink: " + obj.type + "." + prop + " " + obj.ref}
    }

    dataObjectContentPropRule = Prop CacheProp {
        create: (obj, prop, val) -> transferContent val, prop, obj
        del: (obj, prop, val) -> ret null # stat field cannot be deleted
        read: (obj, prop, criteria) -> 
            doc = rules.object new DataObjectContent obj
            if criteria?
                ret rules.object new RangeContent doc, criteria.start, criteria.end
            else
                ret doc
        update: (obj, prop, diff) -> 
            buf <- bind diff.nval.readFully
            diff.oval.writeFully buf # some stat field can be modified others can't
        sink: (obj, prop, criteria = null) -> 
            doc = rules.object new DataObjectContent obj
            if criteria?
                ret rules.object new RangeContent doc, criteria.start, criteria.end
            else
                ret doc


    }

    fileContentPropRule = Prop CacheProp {
        create: (obj, prop, val) -> transferContent val, prop, obj
        del: (obj, prop, val) -> ret val # stat field cannot be deleted
        read: (obj, prop, criteria) ->
            fc = rules.object new FileContent obj
            if criteria?
                ret rules.object new RangeContent fc, criteria.start, criteria.end
            else
                ret fc
        update: (obj, prop, diff) -> 
            buf <- bind diff.nval.readFully
            diff.oval.writeFully buf
            
        sink: (obj, prop, criteria = null) -> 
            fc = rules.object new FileContent obj
            if criteria?
                ret rules.object new RangeContent fc, criteria.start, criteria.end
            else
                ret fc
    }

    collectionHasPartPropRule = Prop CacheProp ArrayProp ArrayStructuralProp CompositeProp {
        create: (obj, prop, val) -> retargetUnder val, obj
        del: (obj, prop, val) -> ret val # don't delete files/dirs, CascadingProp
        read: (obj, prop, criteria) -> ret rules.object new CollectionContent obj, criteria
        update: (obj, prop, diff) -> ret null
        sink: (obj) -> ret rules.object new CollectionContent obj
    }

    directoryHasPartPropRule = Prop CacheProp ArrayProp ArrayStructuralProp CompositeProp {
        create: (obj, prop, val) -> retargetUnder val, obj
        del: (obj, prop, val) -> ret val # don't delete files/dirs, CascadingProp
        read: (obj, prop, criteria) -> ret rules.object new DirectoryContent obj, criteria
        update: (obj, prop, diff) -> ret null
        sink: (obj, prop, criteria = null) -> ret rules.object new DirectoryContent obj
    }

    dataObjectMetadataPropRule = Prop CacheProp ArrayProp {
        create: (obj, prop, val) -> irods.modAVU obj, "add", "-d", val
        del: (obj, prop, val) -> irods.modAVU obj, "rm", "-d", val # stat field cannot be deleted
        read: (obj, prop, criteria) -> ret rules.object new DataObjectAVUContent obj, criteria
        update: (obj, prop, diff) -> irods.modAVU obj, "mod", "-d", diff.oval, diff.nval# some stat field can be modified others can't
        sink: (obj, prop, criteria = null) -> ret rules.object new DataObjectAVUContent obj, criteria
    }

    dataObjectReplicasPropRule = Prop CacheProp ArrayProp {
        create: (obj, prop, val) ->
            rescName <- bind val.readProp "rescName"
            (rules.object {
                type: "Replica"
                ref: "#{obj.ref}?rescName=#rescName"
            }).create
        del: (obj, prop, val) ->
            rescName <- bind val.readProp "rescName"
            (rules.object {
                type: "Replica"
                ref: "#{obj.ref}?rescName=#rescName"
            }).del
        read: (obj, prop, criteria) ->
            {path} <- bind obj.readProp "refobj"
            replicas <- bind irodsTools.listReplicas path, criteria
            ret new ArrayBackedContent rules.objectDeep replicas
        update: (obj, prop, diff) -> ret null # some stat field can be modified others can't
        sink: (obj, prop, criteria = null) -> ret null
    }

    collectionMetadataPropRule = Prop CacheProp ArrayProp ArrayStructuralProp CompositeProp {
        create: (obj, prop, val) -> irods.modAVU obj, "add", "-C", val
        del: (obj, prop, val) -> irods.modAVU obj, "rm", "-C", val # stat field cannot be deleted
        read: (obj, prop, criteria) -> ret rules.object new CollectionAVUContent obj, criteria
        update: (obj, prop, diff) -> irods.modAVU obj, "mod", "-C", diff.oval, diff.nval # some stat field can be modified others can't
        sink: (obj, prop, criteria = null) -> ret rules.object new CollectionAVUContent obj, criteria
    }

    avuAvuTransferOperRule = (srcCnt, tgtCnt) ->
        tgt <- bind tgtCnt.getObj
        tgt.createProp "metadata", srcCnt

    fileContentFileContentTransferOperRule = (srcCnt, tgtCnt) ->
        tgtWS <- bind tgtCnt.writeStream
        srcRS <- bind srcCnt.readStream
        _ <- bind srcRS `fs.pipe` tgtWS
        ret tgtCnt

    fileContentDataObjectContentTransferOperRule = (srcCnt, tgtCnt) ->
        tgtPath <- bind tgtCnt.path
        srcBuf <- bind srcCnt.readFully
        o <- bind srcCnt.getObj
        ext <- bind if o.uid? && o.gid? && o.mode? && o.ctime? && o.mtime?
            ret true
        else
            o.exists
        path <- bind srcCnt.path
        _ <- bind irods.put srcBuf, path, o, tgtPath
        ret tgtCnt

    dataObjectContentDataObjectContentTransferOperRule = (srcCnt, tgtCnt) -> lazy ->
        tgtPath <- bind tgtCnt.path
        srcPath <- bind srcCnt.path
        _ <- bind irods.cp srcPath, tgtPath
        ret tgtCnt

    dataObjectContentFileContentTransferOperRule = (srcCnt, tgtCnt) -> lazy ->
        srcPath <- bind srcCnt.path
        src <- bind srcCnt.getObj
        objSize <- bind src.readProp "objSize"
        {bs} <- bind irods.get srcPath, objSize
        _ <- bind tgtCnt.writeFully bs
        ret tgtCnt

    ioContentTransferOperRule = (srcCnt, tgtCnt) -> lazy ->
        bs <- bind srcCnt.readFully
        _ <- bind tgtCnt.writeFully bs
        ret tgtCnt

    collectiveObjectContentCollectiveObjectContentTransferOperRule = (srcCnt, tgtCnt) ->
        tgtref <- bind tgtCnt.objref
        srcpath <- bind srcCnt.path
        
        arr <- bind (retargetContent srcCnt, srcpath, tgtref).getContent
        rec arr, (a) -> 
            a.create

    adaptiveObjectCreateOperRule = (obj) ->
        if obj.type == "AdaptiveObject"
            cnt <- bind obj.readProp "content"
            o <- bind cnt.getObj
            obj.type <- bind determineAdaptiveObjectTypeFromSrc o, obj
            (obj.oper "create")()
        else
            (obj.lookupSuperOper "AdaptiveObject", "create")()

    collectionCreateOperRule = (obj) ->
        {path} <- bind obj.readProp "refobj"
        _ <- bind irods.mkdir path
        (obj.superOper "create")()

    fileCreateOperRule = (obj) ->
        {path} <- bind obj.readProp "refobj"
        _ <- bind fs.create path
        (obj.superOper "create")()

    dataObjectCreateOperRule = (obj) ->
        {path} <- bind obj.readProp "refobj"
        
        _ <- bind if obj.content? then # don't create if content is provided
            obj.__ORDER = ["content"]
            ret null
        else
            # we need a check that says obj.content != null
            obj.__ORDER = [] # make sure it's empty so that residuals don't get called
            irods.create path
            
        (obj.superOper "create")()

    collectionDelOperRule = (obj) ->
        {path} <- bind obj.readProp "refobj"
        # console.log "deleting " + obj.ref + " " + path
        _   <- bind irods.rmdir path
        ret null # (obj.superOper "del")()

    dataObjectDelOperRule  = (obj) ->
        {path} <- bind obj.readProp "refobj"
        _ <- bind dataObjectManager.unlink path
        ret null # (obj.superOper "del")()
        # according to unlink semantics, it will unlink everything, so need to recursively go down
        # however if we have custom links we may want to do this, but we need to set properties like metadata to null to avoid errors deleting from nonexisting objects

    fileDelOperRule = (obj) ->
        {path} <- bind obj.readProp "refobj"
        _ <- bind fileManager.unlink path
        ret null # (obj.superOper "del")()


    directoryDelOperRule = (obj) ->
        {path} <- bind obj.readProp "refobj"
        _ <- bind fs.rmdir path
        ret null # (obj.superOper "del")()

    directoryCreateOperRule = (obj) ->
        {path} <- bind obj.readProp "refobj"
        _ <- bind fs.mkdir path
        (obj.superOper "create")()

    objectObjectMoveOperRule = (s, sp, sval, t, tp, tval) ->
        tval_newly_created <- bind t.createProp tp, sval
        _ <- bind s.delProp sp, sval
        ret tval_newly_created

    fileFileMoveOperRule = (s, sp, src, t, tp, tgt) ->
        {path:srcPath} <- bind src.readProp "refobj"
        {path:tgtPath} <- bind tgt.readProp "refobj"
        _ <- bind fs.rename srcPath, tgtPath
        ret tgt

    contentMoveOperRule = (s, sp, src, t, tp, tgt) ->
        srcArr <- bind src.getContent
        mapM srcArr, (sval) -> move s, sp, sval, t, tp, null # ignore tgt for now

    dataObjectDataObjectMoveOperRule = (s, sp, src, t, tp, tgt) ->
        _ <- bind irods.rename src, tgt, 11
        ret tgt

    collectionCollectionMoveOperRule = (s, sp, src, t, tp, tgt) ->
        _ <- bind irods.rename src, tgt, 12
        ret tgt

    ####################
    #   Resource       #
    ####################
    resourceRule = Obj rules, StructuralObj {
        eq: (obja, objb) -> obja.type == objb.type && obja.rescName == objb.rescName
        exists: (obj) -> lazy ->
            rescName <- bind obj.readProp "rescName"
            [r] <- bind irodsTools.listResc do
                rescName:
                    o: "="
                    v: rescName
            ret r?
    }

    resourceCreateOperRule = (obj) ->  # create resource
        irods.createResource obj.rescName, obj.rescType, obj.rescHost, obj.rescPath, obj.contextString

    resourceDelOperRule = (obj) -> # delete resource
        irods.deleteResource obj.rescName

    resourceRescNamePropRule = Prop CacheProp {
        create: (obj, prop, val) ->
            ret null # throwm "cannot create property #prop of a Resource"
        del: (obj, prop, val) ->
            ret null # throwm "cannot delete property #prop of a Resource"
        read: (obj, prop, criteria) ->
            ret obj[prop]
        update: (obj, prop, diff) -> # rename resource
            _ <- bind irods.modifyResource obj[prop], "name", diff.nval
            obj[prop] = diff.nval
            ret null
    }

    mapToAdmin = (prop) -> 
        if (prop.substring 0, 4) == "resc"
            (prop.substring 4).toLowerCase()
        else
            prop
    resourceGeneralAdminQueryPropRule = Prop CacheProp {
        create: (obj, prop, val) ->
            ret null # throwm "cannot create property #prop of a Resource"
        del: (obj, prop, val) ->
            ret null # throwm "cannot delete property #prop of a Resource"
        read: (obj, prop, criteria) ->
            arr <- bind irodsTools.readResourceProp obj, prop, criteria
            # console.log arr
            extract1By1MatrixElement arr
        update: (obj, prop, diff) ->
            name <- bind obj.readProp "rescName"
            irods.modifyResource name, (mapToAdmin prop), diff.nval
    }

    #########################################################################
    #    Replica                                                            #
    #    Replica is created and deleted from DataObject's replicas property #
    #########################################################################
    replicaRule = Obj rules, StructuralObj {
        eq: (obja, objb) -> obja.type == objb.type && obja.ref == objb.ref
        exists: (obj) -> lazy ->
            {path} <- bind obj.readProp "refobj"
            rescName <- bind obj.readProp "rescName"
            [r] <- bind irodsTools.listReplicas path, do
                rescName:
                    o: "="
                    v: rescName
            ret r?
    }
    
    ReplicaPathPropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null # create data object, if the content property exists, then do an iput instead
        del: (obj, prop, val) ->  ret null # remove data object
        read: (obj, prop, criteria) ->
            {path} <- bind obj.readProp "refobj"
            ret path
        update: (obj, prop, diff) -> ret null # move data object to a new collection
    }

    replicaQueryPropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null
        del: (obj, prop, val) -> ret null
        read: (obj, prop, criteria) ->
            arr <- bind irodsTools.readReplicaProp obj, prop, criteria
            extract1By1MatrixElement arr
        update: (obj, prop, diff) -> ret null
    }

    replicaRescNamePropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null
        del: (obj, prop, val) -> ret null
        read: (obj, prop, criteria) ->
            {query:{rescName}} <- bind obj.readProp "refobj"
            ret rescName
        update: (obj, prop, diff) -> ret null
    }

    replicaResourcePropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null
        del: (obj, prop, val) -> ret null
        read: (obj, prop, criteria) ->
            rescName <- bind obj.readProp "rescName"
            ret rules.object {
                type: "Resource"
                rescName: rescName
            }
        update: (obj, prop, diff) -> ret null
    }

    replicaCreateOperRule = (obj) ->
        {path} <- bind obj.readProp "refobj"
        rescName <- bind obj.readProp "rescName"
        irods.repl path, rescName # no more to create don't call super

    replicaDelOperRule = (obj) ->
        {path} <- bind obj.readProp "refobj"
        rescName <- bind obj.readProp "rescName"
        irods.trim path, rescName
        # no more to delete don't call super

        
    #########################################################################
    #    User                                                               #
    #    User currently doesn't support changing password                   #
    #########################################################################
    userRule = Obj rules, StructuralObj {
        eq: (obja, objb) -> obja.type == objb.type && obja.ref == objb.ref
        exists: (obj) -> lazy ->
            name <- bind obj.readProp "userName"
            zone <- bind obj.readProp "userZone"
            password <- bind obj.readProp "password"
            if password?
                log "authenticating #name #zone"
                # if password exists we need to test if password is correct
                userEnv = {} <<< rodsEnv <<< proxyUser: name, clientUser: name, proxyRcatZone: zone, clientRcatZone: zone
                userCred = {} <<< rodsCred <<< password: password
                log "create a new connection manager"
                ConnectionManager = require "./connectionManager_l"
                cm = new ConnectionManager prot, trans, userEnv, auth, userCred
                retval <- bind (catchm (
                    log "test connection"
                    cm.connectionDo (conn) -> ret true
                )) (e) ->
                    ret false
                _ <- bind cm.closeAllConnections
                ret retval
                
            else
                # just do a list users
                [u] <- bind irodsTools.listUsers { userName: { o: "=", v: name }, userZone: { o: "=", v: zone } }
                ret u?
    }

    userRefPropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null
        del: (obj, prop, val) -> ret null
        read: (obj, prop, criteria) -> ret obj.ref
        update: (obj, prop, diff) -> throwm "unsupported operation User ref update"
    }

    userCreateOperRule = (obj) ->
        type <- bind obj.readProp "userType"
        name <- bind obj.readProp "canonicalUserName"
        irods.createUser name, type

    userDelOperRule = (obj) ->
        name <- bind obj.readProp "canonicalUserName"
        irods.deleteUser name

    userUserZonePropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null
        del: (obj, prop, val) -> ret null
        read: (obj, prop, criteria) ->
            if obj.ref.match /#/
                ret (obj.ref.split "#")[1]
            else
                ret rodsEnv.clientRcatZone
        update: (obj, prop, diff) -> throwm "cannot modify User zone"
        /*    name <- bind obj.readProp "userName"
            zone <- bind obj.readProp "userZone"
            irodsMonad.modifyUser "#name##zone", "zone", diff.nval*/
    }

    userCanonicalUserNamePropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null
        del: (obj, prop, val) -> ret null
        read: (obj, prop, criteria) ->
            name <- bind obj.readProp "userName"
            zone <- bind obj.readProp "userZone"
            ret if zone == rodsEnv.clientRcatZone then name else "#name##zone"
            
        update: (obj, prop, diff) -> throwm "cannot modify User canonicalUserName"
    }
    
    userUserTypePropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null
        del: (obj, prop, val) -> ret null
        read: (obj, prop, criteria) ->
            name <- bind obj.readProp "userName"
            zone <- bind obj.readProp "userZone"
            [user] <- bind irodsTools.listUsers { userName: { o: "=", v: name }, userZone: { o: "=", v: zone } }
            if user?
                ret user.userType
            else
                throwm "user #name##zone does not exists, therefore cannot get its type"
        update: (obj, prop, diff) ->
            name <- bind obj.readProp "canonicalUserName"
            # console.log "modify user #name #{diff.nval}"
            irods.modifyUser name, "type", diff.nval
    }

    userUserNamePropRule = Prop CacheProp {
        create: (obj, prop, val) -> ret null
        del: (obj, prop, val) -> ret null
        read: (obj, prop, criteria) -> ret (obj.ref.split "#")[0]
        update: (obj, prop, diff) -> throwm "unsupported operation User zone update"
    }

    userPasswordPropRule = Prop CacheProp {
        create: (obj, prop, val) -> throwm "unsupported operation User password create"
        del: (obj, prop, val) -> throwm "unsupported operation User password del"
        read: (obj, prop, criteria) -> ret null
        update: (obj, prop, diff) -> throwm "unsupported operation User password update"
    }

    hier =
        property:
            subtype:
                content: {}
        AllObject:
            opers:
                transfer:
                  * pt: <[ CollectiveObjectContent CollectiveObjectContent ]>
                    run: collectiveObjectContentCollectiveObjectContentTransferOperRule
                  * pt: <[ DataObjectContent FileContent ]>
                    run: dataObjectContentFileContentTransferOperRule
                  * pt: <[ DataObjectContent DataObjectContent ]>
                    run: dataObjectContentDataObjectContentTransferOperRule
                  * pt: <[ FileContent FileContent ]>
                    run: fileContentFileContentTransferOperRule
                  * pt: <[ FileContent DataObjectContent ]>
                    run: fileContentDataObjectContentTransferOperRule
                  * pt: <[ IOContent IOContent ]>
                    run: ioContentTransferOperRule
                  * pt: <[ AVUContent AVUContent ]>
                    run: avuAvuTransferOperRule
                move:
                  * pt: <[ CollectiveObject content File CollectiveObject content File  ]>
                    run: fileFileMoveOperRule
                  * pt: <[ CollectiveObject content DataObject CollectiveObject content DataObject ]>
                    run: dataObjectDataObjectMoveOperRule
                  * pt: <[ CollectiveObject content Collection CollectiveObject content Collection ]>
                    run: collectionCollectionMoveOperRule
                  * pt: <[ CollectiveObject content Directory CollectiveObject content Directory ]>
                    run: fileFileMoveOperRule
                  * pt: <[ CollectiveObject content Content _ _ _ ]>
                    run: contentMoveOperRule
                  * pt: <[ _ _ _ _ _ _ ]> # catch all rule
                    run: objectObjectMoveOperRule
            props:
                type: ImmutableVolatilePropRule # the type of the object
            subtypes:
                OtherObject:
                    rule: ImmutableVolatileObjRule
                    subtypes:
                        CollectionAVU: {}
                        DataObjectAVU: {}
                        Content:
                            subtypes:
                                CollectiveContent:
                                    subtypes:
                                        CollectiveObjectContent:
                                            subtypes:
                                                DirectoryContent: {}
                                                CollectionContent: {}
                                        AVUContent:
                                            subtypes:
                                                CollectionAVUContent: {}
                                                DataObjectAVUContent: {}
                                IOContent:
                                    subtypes:
                                        BufferContent: {}
                                        FileContent: {}
                                        RangeContent: {}
                                        DataObjectContent: {}
                PathObject:
                    rule: pathObjRule
                    props:
                        objType: pathObjectStatPropRule
                        ref: ImmutableVolatilePropRule # the scr of the object
                        refobj: pathObjectRefParsePropRule
                        flags: ImmutableVolatilePropRule # the flags of the object
                        objSize: pathObjectStatPropRule
                        content: pathObjectContentPropRule
                    subtypes:
                        AdaptiveObject:
                            props:
                                ref: adaptiveObjectRefPropRule
                                create: adaptiveObjectCreateOperRule
                            subtypes:
                                IndividualObject:
                                    subtypes:
                                        DataObject:
                                            props:
                                                createTime: dataObjectQueryPropRule
                                                modifyTime: dataObjectQueryPropRule
                                                checksum: dataObjectQueryPropRule
                                                ownerName: dataObjectQueryPropRule
                                                ownerZone: dataObjectQueryPropRule
                                                name: dataObjectQueryPropRule
                                                content: dataObjectContentPropRule
                                                metadata: dataObjectMetadataPropRule
                                                replicas: dataObjectReplicasPropRule
                                                del: dataObjectDelOperRule
                                                create: dataObjectCreateOperRule
                                        File:
                                            props:
                                                content: fileContentPropRule
                                                del: fileDelOperRule
                                                create: fileCreateOperRule
                                CollectiveObject:
                                    subtypes:
                                        Directory:
                                            props:
                                                content: directoryHasPartPropRule
                                                create: directoryCreateOperRule
                                                del: directoryDelOperRule
                                        Collection:
                                            props:
                                                name: collectionQueryPropRule
                                                ownerName: collectionQueryPropRule
                                                ownerZone: collectionQueryPropRule
                                                content: collectionHasPartPropRule
                                                metadata: collectionMetadataPropRule
                                                create: collectionCreateOperRule
                                                del: collectionDelOperRule


    rules
      ..addRules hier
      ..addRules {
        Resource:
            rule: resourceRule
            props:
                rescName: resourceRescNamePropRule # the name of the resource
                rescPath: resourceGeneralAdminQueryPropRule
                rescHost: resourceGeneralAdminQueryPropRule
                contextString: resourceGeneralAdminQueryPropRule
                status: resourceGeneralAdminQueryPropRule
                # replNum: resourceGeneralAdminQueryPropRule
                rescClass: resourceGeneralAdminQueryPropRule
                zone: resourceGeneralAdminQueryPropRule
                rescType: resourceGeneralAdminQueryPropRule
                create: resourceCreateOperRule
                del: resourceDelOperRule

      }, "AllObject"

      ..addRules {
          Replica:
              rule: replicaRule
              props:
                  replNum: replicaQueryPropRule
                  replStatus: replicaQueryPropRule
                  rescName: replicaRescNamePropRule
                  resource: replicaResourcePropRule
                  create: replicaCreateOperRule
                  del: replicaDelOperRule
                  path: ReplicaPathPropRule
      }, "PathObject"
      ..addRules {
          User:
              rule: userRule
              props:
                  ref: userRefPropRule
                  userName: userUserNamePropRule
                  userType: userUserTypePropRule
                  canonicalUserName: userCanonicalUserNamePropRule
                  userZone: userUserZonePropRule
                  password: userPasswordPropRule
                  create: userCreateOperRule
                  del: userDelOperRule

      }, "AllObject"

    rules <<< {
        cleanup: do
            _ <- bind fileManager.closeAllFds
            _ <- bind dataObjectManager.closeAllFds
            irods.cleanup
            
        retargetUnder: retargetUnder
        retargetAt: retargetAt
        retargetContent: retargetContent
        transferContent: transferContent
        moveObj: moveObj
        move: move
        copy: copy
        read: read
        BufferContent: BufferContent
        RangeContent: RangeContent
    }

module.exports = {model}
