{bind, bind2, ret, throwm} = require "../monad_l"
{ConnectionConstants} = require "../const_l"
crypto = require "crypto"
log = (require "debug")("auth")

# converted from jargon
challengeResponse = (challenge, password) ->
    # Convert base64 string to a byte array
    chal = null
    temp = new Buffer challenge, "base64"
    if temp.length != ConnectionConstants.CHALLENGE_LENGTH
        throwm "challenge length error expected " + ConnectionConstants.CHALLENGE_LENGTH + " encountered " + temp.length

    if password.length < ConnectionConstants.MAX_PASSWORD_LENGTH
        # pad the end with zeros to MAX_PASSWORD_LENGTH
        chal = new Buffer (ConnectionConstants.CHALLENGE_LENGTH + ConnectionConstants.MAX_PASSWORD_LENGTH)
        chal.fill 0
    else
        throwm "password is too long"

    # add the password to the end
    temp.copy chal, 0, 0, ConnectionConstants.CHALLENGE_LENGTH
    temp = new Buffer password, "ascii"
    temp.copy chal, ConnectionConstants.CHALLENGE_LENGTH
    # get the md5 of the challenge+password
    hash = crypto.createHash "md5"
    hash.end chal
    chal = hash.read()
    # after md5 turn any 0 into 1
    for i from 0 til chal.length
        if chal[i] == 0
            chal[i] = 1
    # return to Base64
    ret (chal.toString "base64")


module.exports =
    name : "basic_auth",
    plugin : -> {  
        authenticate: (transport, cred, env) ->
            msgObj <- bind transport.api 703
            log "authenticate: " + msgObj
            challenge = msgObj.msg.authRequestOut_PI.challenge.toString()
            resp <- bind challengeResponse challenge, cred.password
            _ <- bind transport.api 704, do
                authResponseInp_PI:
                    response : resp
                    username : env.clientUser + "#" + env.clientRcatZone

            if parseInt msgObj.header.MsgHeader_PI.intInfo != 0
                throwm "error: authentication error " + msgObj.header.MsgHeader_PI.intInfo
            else
                log "authenticated"
                ret null
    }
