ServerState = {
		CONNECT: "connect",
		STARTUP: "startedup",
		NEGOTIATE: "negotiate",
		REQUEST: "request",
		ERROR: "error",
		DISCONNECT: "disconnect",
		EXIT: "exti"
}

MsgType = {
		RODS_API_REQ_T : "RODS_API_REQ",
		RODS_CONNECT_T : "RODS_CONNECT",
		RODS_DISCONNECT_T : "RODS_DISCONNECT",
		RODS_CS_NEG_T : "RODS_CS_NEG_T",
		RODS_API_REPLY_T : "RODS_API_REPLY"

}

Negotiation = {
		CS_NEG_DONT_CARE: "CS_NEG_DONT_CARE",
		CS_NEG_USE_TCP: "CS_NEG_USE_TCP",
		CS_NEG_USE_SSL: "CS_NEG_USE_SSL"
}

Keywords = {
		CS_NEG_RESULT_KW: "cs_neg_result_kw"
}

ConnectionConstants = {
		MAX_PASSWORD_LENGTH:50,
		CHALLENGE_LENGTH:64,
}

ObjType = {
		DATA_OBJECT: {
				name: "data object",
				id: 1
		},
		COLLECTION: {
				name: "collection",
				id: 2
		}
}



module.exports = {
		ServerState: ServerState,
		MsgType: MsgType,
		Negotiation: Negotiation,
		Keywords: Keywords,
		ConnectionConstants: ConnectionConstants,
		ObjType: ObjType
}
