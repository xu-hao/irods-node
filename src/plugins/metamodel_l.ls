/*
  The metamodel object
*/

{bind, ret, throwm, catchm, lazy, e, cont, runCont} = require "./monad_l"
{a, x, arrow, type, str, bool, nul, obj, U, array, typeCheck} = require "./type_l"
{setDiff, setUnion, rec, recM, IN, UNION} = require "./arrays_l"

obj_rule_type =
    eq_type: obj! x obj! -> bool
    props_type: obj! -> cont a, e array str
    read_type: obj! x array! -> cont a, e obj!
    exists_type: obj! -> cont a, e bool
    create_type: obj! -> cont a, e nul
    del_type: obj! -> cont a, e nul
    update_type: obj! x obj! -> cont a, e nul

prop_rule_type =
    read_type: obj! x str x a -> cont a, e a
    create_type: obj! x str x a -> cont a, e a
    del_type: obj! x str x a -> cont a, e nul
    update_type: obj! x str x obj! -> cont a, e nul

obj_type =
    eq_type: obj! -> bool
    props_type: cont a, e array str
    read_type: str U array! -> cont a, e a
    exists_type: cont a, e bool
    create_type: cont a, e nul
    del_type: cont a, e nul
    createProp_type: str x a -> cont a, e nul
    delProp_type: str x a -> cont a, e nul
    updateProp_type: str x obj! -> cont a, e nul
    readProp_type: str -> cont a, e a
    readProp2_type: str x a -> cont a, e a
    update_type: obj! -> cont a, e nul
    setSynched_type: (array str) x str -> cont a, e nul

##########################################
#   Rule-based object Logic Subtyping    #
##########################################
Rules = ->
    Subtyping = !->
        @table = table = {
            Top: {is:"Top"}
        }

        @rep = rep = (type) ->
            if table[type]?
                table[type]
            else
                table[type] = (rep "Top") with {
                    is : type
                }

        @sub = (subtype, type) ->
            table[subtype] = (rep type) with {
                is: subtype
            }
#            console.log subtype + " <: " + type

        @supertypeOf = (type) ->
            p = Object.getPrototypeOf rep type
            if p?
                p.is
            else
                throw new Error type.is + " has no supertype!"

    subtyping = new Subtyping
    {rep} = subtyping

    operGroupTable = {}

    subtypeOf = (a, b) ->
        ra = rep a
        rb = rep b
        subtypeOfR ra, rb

    subtypeOfR = (ra, rb) ->
        if ra == rb
            true
        else if (pa = Object.getPrototypeOf ra) != Object.prototype
            subtypeOfR pa, rb
        else
            false

    (rep "Top") <<< ImmutableVolatileObjRule
    (rep "Top") <<< { _: ImmutableVolatilePropRule }

    lookupObjRule = (type) -> rep type

    lookupPropRuleR = (r, prop) ->
        rule = r[prop]
        if rule?
          #  console.log rule
            rule
        else
            #console.log "warning: cannot find rule for " + r.is + "." + prop + " using Top._"
            (rep "Top")._

    lookupOperRuleRG = (g, pt) ->
        if pt.length == 0
            g
        else
            [hd, ...tl] = pt
            found = null
            for t, subg of g
                if (t == "_" || hd == "_" || hd `subtypeOf` t) && (r = lookupOperRuleRG subg, tl)? # _ for match all
                    found := r
                    break
            found

    lookupPropRule = (type, prop) -> lookupPropRuleR (rep type), prop

    # find the first matching rule, not the most specific rule
    lookupOperRule = (pt, oper) ->
        g = operGroupTable[oper]
        if (rr = lookupOperRuleRG g, pt)?
            rr
        else
            throw "error: cannot find oper rule for #oper " + (pt.join " ")

    addObjRule = (type, r) ->
        (rep type) <<< r

    addPropRule = (type, prop, r) ->
        rt = rep type
        if r instanceof Function
            rt[prop] = r
        else if (pp = (Object.getPrototypeOf rt)[prop])?
            rt[prop] = pp with r
        else
            rt[prop] = r

    addOperRule = (pt, oper, r) ->
        if pt.length == 0
            operGroupTable[oper] = r
        else
            a = pt[0 til pt.length-1]
            b = pt[pt.length - 1]
            g = operGroupTable.{}[oper]
            for t in a
                g = g.{}[t]
            g[b] = r
        #console.log "###################"+JSON.stringify operRules

    # {
    #   type : {
    #     rule: <rule object>
    #     props: {
    #         prop: <rule object>
    #     }
    #     opers: {
    #         oper: <rule object>
    #     }
    #     subtypes: {
    #
    #     }
    #   }
    # }
    #
    addRules = (rules, supertype = "Top") ->
        for type, val of rules
            type `subtyping.sub` supertype
            if val.rule?
                addObjRule type, val.rule
            if val.props?
                # console.log "########## props" + val.props
                for prop, rule of val.props
                    # console.log "add rule #type.#prop = " + rule
                    addPropRule type, prop, rule
            if val.opers?
                # console.log "########## opers" + val.opers
                for oper, pack of val.opers
                    for {pt, run} in pack
                        addOperRule pt, oper, run
            if val.subtypes?
                addRules val.subtypes, type

    getPropRule = (obj, prop) -> lookupPropRule obj.type, prop

    getObjRule = (obj) -> lookupObjRule obj.type

    object = (oobj) ->

        exists = lazy -> (getPropRule obj, "exists") obj

        eq = (obj2) -> (getPropRule obj, "eq") obj, obj2

        filter = lazy -> (getPropRule obj, "read") obj, []

        read = (props) ->
            if Array.isArray props
                (getPropRule obj, "read") obj, props
            else if typeof props == "string" || props instanceof String
                (getPropRule obj, props).read obj, props
            else
                throwm {error: -1, errormsg: "unreadable properties: " + props}

        props = lazy -> (getPropRule obj, "props") obj

        readProp2 = (prop, criteria) -> (getPropRule obj, prop).read obj, prop, criteria

        readProp = (prop) -> readProp2 prop, null

        create = lazy -> (getPropRule obj, "create") obj

        del = lazy -> (getPropRule obj, "del") obj

        update = (diff) -> (getPropRule obj, "update") obj, diff

        updateProp = (prop, diff) -> (getPropRule obj, prop).update obj, prop, diff

        createProp = (prop, nval) -> (getPropRule obj, prop).create obj, prop, nval

        delProp = (prop, oval) -> (getPropRule obj, prop).del obj, prop, oval
        # only used by CacheProp, status can be, "synched", "unsynched", or "notset"
        setSynched = (props, status) -> (cont) !->
            for prop in props
                obj.__synched[prop] = str
            (runCont ret null) cont

        instanceOf = (type) -> subtypeOf obj.type, type

        superOper = (oper) -> (...p) ->
            (Object.getPrototypeOf rep obj.type)[oper].apply null, [obj] ++ p

        lookupSuperOper = (type, oper) -> (...p) ->
            (Object.getPrototypeOf rep type)[oper].apply null, [obj] ++ p

        oper = (oper) -> (...p) ->
            (rep obj.type)[oper].apply null, [obj] ++ p

        sink = (prop, propc_criteria = null) -> (getPropRule obj, prop).sink obj, prop, propc_criteria

        __MAGIC_PROP__ = "__MAGIC_PROP__"
        methods = typeCheck {
            sink, instanceOf, lookupSuperOper, superOper, oper,/*o*/
            exists, eq, read, props, readProp, readProp2,
            create, del, update, updateProp, createProp, delProp, setSynched, __MAGIC_PROP__,
            filter
        } <<< obj_type
        obj = methods with oobj

    objectDeep = (obj, visited = []) ->
        if Array.isArray obj
            [ objectDeep o, visited for o in obj ]
        else if (! (obj instanceof Object)) || (obj.__MAGIC_PROP__?) || obj `IN` visited
            obj
        else
            visited.push obj
            for own p, v of obj
                obj[p] = objectDeep v, visited
            object obj

    {lookupObjRule, lookupPropRule, lookupOperRule, addObjRule, addPropRule, getPropRule, addRules, object, objectDeep}

ImmutableVolatileObjRule =
    eq:     (obja, objb) -> obja == objb
    read:   (obj, props) -> ret obj
    exists: (obj) -> ret true
    del:    (obj) -> ret null
    create: (obj) -> ret null
    update: (obj, diff) -> ret null
    props:  (obj) -> ret [prop for own prop of obj]

ImmutableVolatilePropRule =
    read:   (obj, prop, criteria) -> ret obj[prop] # ignore criteria
    update: (obj, prop, diff) -> ret null
    create: (obj, prop, val) -> ret null
    del:    (obj, prop, val) -> ret null

checkExists = (obj) -> do
    b <- bind obj.exists
    if !b
        throwm {error:-1, errormsg: "obj " + obj.ref + " does not exist"}
    else
        ret r

checkNotExist = (obj) -> do
    b <- bind obj.exists
    if b
        throwm {error:-1, errormsg: "obj " + obj.ref + " already exists"}
    else
        ret r

Obj = (rules, core) ->
    core.read = (oobj, props) -> do
        obj = rules.object {
            type: oobj.type
            ref: oobj.ref
        }
        # we have to skip ref and type here because their values in oobj are not filters
        # using them would cause undefined behaviors
        _ <- bind rec [p for own p of oobj when p != "ref" && p != "type" && (props.length == 0 || p `IN` props) ], (h) ->
            #console.log obj[h]
            obj[h] <- bind obj.readProp2 h, oobj[h]
            #console.log h
            #console.log obj[h]
            ret null
        ret obj

    core.props = (obj) -> lazy ->
        ret [prop for own prop of obj]

    typeCheck core <<< obj_rule_type

all = {all:{}} # all values
none = {none:{}} # none of the values

PropDiff = (oobj, nobj) !->
    @oval= oobj
    @nval= nobj
    @props= ret [prop for own prop of nobj when !(prop in ["ref", "type"])]

StructuralObj = (core) ->
    # oval\nval    all       none         X
    #  all         nop       delete all   modify
    #  none        nop       nop          create/union
    #  X           nop       delete X     modify

    core.create = (obj) -> lazy ->
        createFunc = (h) ->
            # console.log "create #h, #{obj[h]?.type}";
            obj.createProp h, obj[h]
        _ <- bind if obj.__ORDER?
            rec obj.__ORDER, createFunc # __ORDER indicate create order
        else
            ret null
        recM obj.props, (h) -> if !obj.__ORDER? or !(h in obj.__ORDER)
            createFunc h
        else
            ret null # create the rest

    core.del = (obj) -> lazy ->
        recM obj.props, ((h) -> obj.delProp h, all)

    # update_type = obj! x (obj {oobj: a, nobj: a}) arrow cont a, e nul
    core.update =(obj, diff) -> lazy ->
        pp <- bind diff.props

        recM diff.props, ((h) ->
            console.log "updating prop #h with " + JSON.stringify diff
            obj.updateProp h, (new PropDiff diff.oval[h], diff.nval[h]))

    typeCheck core <<< obj_rule_type


decorate = (obj, oper, deco) ->
    if (p = obj[oper])?
        obj[oper] = deco p

Prop = (core) ->
    decorate core, "update", (p) -> (obj, prop, diff) ->
        oval = diff.oval ? none
        nval = diff.nval ? none
        #console.log "updating prop #prop"
        switch
        | oval.none? =>
            switch
            | nval.none? => # nop
                ret null
            | _ => # union
                # console.log "creating prop #prop"
                obj.createProp prop, nval
        | oval.all? =>
            switch
            | nval.none? =>
                obj.delProp prop, all
            | _ =>
                p obj, prop, diff
        | _ =>
            switch
            | nval.none? =>
                obj.delProp prop, oval
            | _ =>
                p obj, prop, diff


    typeCheck core <<< prop_rule_type
# by default exists = synched nonexist = unsynched

CacheProp = (core) ->
    decorate core, "create", (p) -> (obj, prop, val) -> do
        obj[prop] <- bind p obj, prop, val
        ret obj[prop]

    decorate core, "read", (p) -> (obj, prop, criteria) -> do
      #  console.log "reading prop " + prop
        if !obj.__synched
            obj.__synched = {}

        if criteria? # do not cache filtered results
            p obj, prop, criteria
        else
            synch = (setSynch) ->
                v <- bind p obj, prop, criteria
                # console.log "#{obj.ref}.#prop synch'ed to #v"
                obj[prop] = v
                if setSynch
                    obj.__synched[prop] = "synched"
          #      console.log "original " + obj[prop]
                ret obj[prop]

            switch obj.__synched[prop]
            | "synched" => ret obj[prop]
            | "unsynched" => synch true
            | _ =>
                if obj[prop]?
                    ret obj[prop]
                else
                    synch false

    typeCheck core <<< prop_rule_type

# the array property discretize the structure of the val into items that can be create/delete/modified individually
# this is an example of discritized value, the value can also be other complex structures
ArrayProp = (core)->
        # here we allow some sloppiness by allowing val to be an array or a simple object
    getContent = (val) ->
        if ! val? # if this comes from structural object
            ret []
        else if Array.isArray val
            console.error "warning: array content is not of ArrayContent type, but an array"
            ret val
        else if val.getContent?
            val.getContent
        else
            console.error "warning: array content is not of ArrayContent type, but an object"
            ret [val]

    decorate core, "create", (p) -> (obj,prop,val) -> do
        content <- bind getContent val
        rec content, (h) -> p obj, prop, h

    decorate core, "del", (p) -> (obj,prop,val) -> do
        deleteSelected = (val) -> do
            # console.log "deleting " + val
            content <- bind getContent val
            rec content, (h) -> p obj, prop, h
        if val?.all?
            v <- bind obj.readProp prop
            deleteSelected v
        else
            deleteSelected val

    decorate core, "update", (p) ->
        # assert (core.create? && core.del?), "update is present without both create and del in ArrayProp core"
        (obj, prop, diff) -> do
            modifySelected = (diff) -> do
                #console.log diff
                oc <- bind getContent diff.oval
                nc <- bind getContent diff.nval
                [dval, mval, cval] = setDiff oc, nc
                /* console.log "oval  = " + JSON.stringify diff.oval
                console.log "nval  = " + JSON.stringify diff.nval
                console.log "del   = " + JSON.stringify dval
                console.log "mod   = " + JSON.stringify mval
                console.log "creat = " + JSON.stringify cval */
                _ <- bind obj.delProp prop, dval
                _ <- bind obj.createProp prop, cval
                rec mval, (h) -> p obj, prop, new PropDiff h[0], h[1]
            if diff.oval?.all?
                v <- bind obj.readProp prop
                modifySelected new PropDiff v, diff.nval
            else if diff.oval?.none?
                modifySelected new PropDiff (new ArrayBackedContent []), diff.nval
            else
                modifySelected diff

    typeCheck core <<< prop_rule_type

/*CheckProp = (core) ->
    create = (obj, prop, nval) -> do
        v <- bind obj.readProp prop
        if v? # disjoint union, partial create, to be consistent with array property semantics
            core.update obj, prop, {oval:none, nval:val}
        else
            if nval == none
                thrown {error: -1, "error: create a property with no value"}
            else
                core.create obj, prop, val

    del = (obj, prop, oval, test = false) -> do
        v <- bind obj.readProp prop
        if v? # diff
            if oval == all
                core.del obj, prop, oval
            else
                core.update obj, prop, {oval:oval, nval:none}
        else # nop?
            throwm {error:-1, "error: delete a nonexistent property"}

    update = (obj, prop, diff) -> do
        v <- bind obj.readProp prop
        if v? # modify
            if oval == all && nval == none
                core.del obj, prop, diff.oval
            else
                core.update obj, prop, diff
        else # create?
            if nval == none
                ret null
            else
                core.create obj, prop, diff.nval

    typeCheck core with {create, del, update} <<< prop_rule_type*/

ArrayStructuralProp = (core) ->
    decorate core, "update", (p) -> (obj, prop, diff) ->
        diff.oval.update diff

    typeCheck core <<< prop_rule_type

StructuralProp = (core) ->
    decorate core, "update", (p) -> (obj, prop, diff) ->
        v <- bind obj.readProp prop
        # console.log "updating #prop " + JSON.stringify v
        v.update diff

    typeCheck core <<< prop_rule_type

CascadingProp = (core)->
    decorate core, "create", (p) -> (obj, prop, val) ->
        # console.log "create " + val.ref
        v <- bind val.create
        p obj, prop, v

    decorate core, "del", (p) -> (obj, prop, val) ->
        _ <- bind p obj, prop, val
        val.del

    typeCheck core <<< prop_rule_type

CompositeProp = (core)->
    decorate core, "create", (p) -> (obj, prop, val) ->
        # console.log "create " + val.ref
        v <- bind p obj, prop, val
        v.create

    decorate core, "del", (p) -> (obj, prop, val) ->
        console.log "del " + val.ref
        _ <- bind val.del
        p obj, prop, val

    typeCheck core <<< prop_rule_type

instanceOf = (obja, objb) ->
    p = Object.getPrototypeOf obja
    if p == null
        false
    else if p
        true
    else
        instanceOf p, objb
        
ArrayBackedContent = (arr) !->
    @type = "ArrayBackedContent"
    @getContent = ret arr


module.exports = {
    Rules, all, none,
    ArrayBackedContent,
    ImmutableVolatileObjRule, ImmutableVolatilePropRule,
    Obj, StructuralObj, Prop, ArrayProp, ArrayStructuralProp,
    CacheProp, PropDiff, StructuralProp, CompositeProp, CascadingProp,
    instanceOf
}
