
xml2js = require "xml2js"

{bind,ret,throwm,runCont} = require "../monad_l"

{MsgType, Negotiation, Keywords} = require "../const_l"

log = (require "debug")("protocol")

apply = (streamParser, rodsEnv) ->

    {readUInt32,readBuf,writeBuf,destroy, establish, isConnected} = streamParser

    builder =
        new xml2js.Builder do
            renderOpts:
                pretty: true
                indent: ""
                newline: "\n"
            headless: true

    pack = (type, msg, intInfo, err, bs) ->
        msgstr = if msg? then builder.buildObject msg else ""
        errstr = if err? then builder.buildObject err else ""
        msgHeader =
          MsgHeader_PI:
            type: type
            msgLen: msgstr.length
            errorLen: errstr.length
            bsLen: if bs then bs.length else 0
            intInfo: intInfo
        headerstr = builder.buildObject msgHeader
        {headerstr, msgstr, errstr}


    msgToBuffer = (type, msg, intInfo, err, bs) ->
        {headerstr, msgstr, errstr} = pack type, msg, intInfo, err, bs
        buf = new Buffer(4 + headerstr.length + msgstr.length + errstr.length + (if bs? then bs.length else 0))
        buf.writeInt32BE headerstr.length, 0, true
        buf.write headerstr, 4, headerstr.length, "ascii"
        buf.write msgstr, 4 + headerstr.length, msgstr.length, "ascii"
        buf.write errstr, 4 + headerstr.length + msgstr.length, errstr.length, "ascii"
        if bs?
            bs.copy buf, 4 + headerstr.length + msgstr.length + errstr.length, 0, bs.length
        buf

    extractMsgLen = (obj) ->
        parseInt obj.MsgHeader_PI.msgLen

    extractErrLen = (obj) ->
        parseInt obj.MsgHeader_PI.errorLen

    extractByteStreamLen = (obj) ->
        parseInt obj.MsgHeader_PI.bsLen

    parseObj = (buf) -> (cont) !->
        log "parse: " + buf.toString "ascii"
        xml2js.parseString buf.toString("ascii"), (err, obj)->
            if err
                throwm err
            else
                log("buf = " + buf)
                log("parser continuation" + cont)
                (runCont (ret obj)) cont

    readMsg = do # without do it won't compile
        header <- bind (readUInt32 `bind` readBuf `bind` parseObj)
        log header
        msglen = extractMsgLen header
        errorlen = extractErrLen header
        bsLen = extractByteStreamLen header
        m =
            if msglen == 0
                ret null
            else
                (ret msglen) `bind` readBuf `bind` parseObj
        e =
            if errorlen == 0
                ret null
            else
                (ret errorlen) `bind` readBuf `bind` parseObj
        b =
            if bsLen == 0
                ret null
            else
                (ret bsLen) `bind` readBuf
        msg <- bind m
        err <- bind e
        bs <- bind b
        retval =
            header: header
            msg: msg
            err: err
            bs: bs
        ret retval

    # optional params must be declared
    # or the compiler will generate partial application
    writeMsg = (msgType, msg = null, intInfo = 0, err = null, bs = null) ->
        try
            buf = msgToBuffer msgType, msg, intInfo, err, bs
            writeBuf buf
        catch exp
            throwm {error: -1, errormsg: "write message error " + exp, msg: msg}

    apiAsync = (apiNumber, msg = null, bs = null) -> 
        writeMsg MsgType.RODS_API_REQ_T, msg, apiNumber, null, bs
        
    api = (apiNumber, msg = null, bs = null) -> do
        _ <- bind apiAsync apiNumber, msg, bs
        msg <- bind readMsg
        # console.log parseInt msg.header.MsgHeader_PI.intInfo
        if (err = parseInt msg.header.MsgHeader_PI.intInfo) < 0
            throwm do
                error: err
                errormsg: "server returns error code #err"
                msg: msg
        else
            ret msg
        
    connect = establish

    disconnect = do
        b <- bind isConnected
        if b
            _ <- bind writeMsg MsgType.RODS_DISCONNECT_T
            destroy
        else
            destroy

    sendStartupPack =
        writeMsg MsgType.RODS_CONNECT_T, {
            StartupPack_PI:
                irodsProt: 1
                reconnFlag: 0
                connectCnt: 0
                proxyUser: rodsEnv.proxyUser
                proxyRcatZone: rodsEnv.proxyRcatZone
                clientUser: rodsEnv.clientUser
                clientRcatZone: rodsEnv.clientRcatZone
                relVersion: "rods4.0.3"
                apiVersion: "d"
                option: "request_server_negotiation"
        }

    negotiate = do
        envelop <- bind readMsg
        res = envelop.msg.CS_NEG_PI.result.toString();
        if res != Negotiation.CS_NEG_DONT_CARE && res != Negotiation.CS_NEG_USE_TCP
            throwm "error: only tcp is currently supported, but " + envelop.msg.CS_NEG_PI.result + " is requrested"
        else
            _ <- bind writeMsg MsgType.RODS_CS_NEG_T, {
                CS_NEG_PI:
                    status: 1
                    result: Keywords.CS_NEG_RESULT_KW+"="+Negotiation.CS_NEG_USE_TCP
            }
            readMsg  # ignore this VERSION_PI message

    prot = {connect, sendStartupPack, negotiate, readMsg, writeMsg, api, apiAsync, disconnect}

    prot

module.exports =
  name: "prot-xml"
  plugin: apply
