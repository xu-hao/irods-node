{bind, ret, throwm, catchm, e, cont} = require "./monad_l"
{arrow, x, a, type, str, bool, obj, U, array, typeCheck} = require "./type_l"

transpose = (arr) ->
    # console.log arr
    mat = []
    for i from 0 til arr[0].length
        row = []
        for j from 0 til arr.length
            row[j] = arr[j][i]
        mat[i] = row
    # console.log mat
    mat

toObjects = (arr, cols) ->
    mat = []
    for i from 0 til arr.length
        row = {}
        for j from 0 til arr[i].length
            row[cols[j]] = arr[i][j]
        mat[i] = row
    mat

foldl = (oper, init, list) ->
    #console.log list
    if list.length == 0
        init
    else
        [h, ...t] = list
        foldl oper, (init `oper` h), t

arrayIn = (e, a) ->
    retval = null
    for ea in a
        if e .eq ea
            retval = ea
            break
    retval

IN = (e, a) ->
    foldl ((b, e2) -> b || e == e2), false, a

UNION = (a, b) ->
    [e for e in a when ! e `IN` b] ++ b

# given set a, b return [a-b, a-(a-b), b-a]
setDiff = (a, b, eq) ->
    a_b = [e for e in a when !(arrayIn e, b, eq)]
    b_a = [e for e in b when !(arrayIn e, a, eq)]
    a_a_b = [[e, eb] for e in a when eb = arrayIn e, b, eq]
    return [a_b, a_a_b, b_a]

setUnion = (a, b, eq) ->
    [a for e in a when !(arrayIn e, b, eq)] ++ b
    
rec_type = array! x (a -> cont a, e a) arrow cont a, e a
rec = (props, action) ->
    # console.log "rec" + props + action
    if props.length == 0
        ret null
    else
        [h, ...t] = props
        _ <- bind action h
        rec t, action
rec = rec `type` rec_type

recM_type = (cont a, e array!) x (a -> cont a, e a) arrow cont a, e a
recM = (ext, extOp) ->
    e <- bind ext
    rec e, extOp
recM = recM `type` recM_type

mapM = (arr, action) ->
    # console.log "rec" + props + action
    if arr.length == 0
        ret []
    else
        [h, ...t] = arr
        hret <- bind action h
        tret <- bind mapM t, action
        ret [hret] ++ tret
        
module.exports = {transpose, toObjects, IN, UNION, arrayIn, setDiff, setUnion, rec, recM, mapM, foldl}
