/*
  The model object
*/

{bind, ret, throwm, catchm, lazy, e, cont} = require "./monad_l"
{a, typeCheck, obj, nul, arrow} = require "./type_l"
{rec} = require "./arrays_l"

ConnectionManager = (protocol, transport, rodsEnv, auth, rodsCred) !->
    connections = []
    free = []

    @newConnection = newConnection = lazy ->
        conn = protocol (transport rodsEnv), rodsEnv
        (catchm (
            _ <- bind conn.connect
            _ <- bind conn.sendStartupPack
            _ <- bind conn.negotiate
            _ <- bind auth().authenticate conn, rodsCred, rodsEnv
            connections.push conn
            ret conn)) (e) ->
            _ <- bind conn.disconnect
            throwm e

    @acquireConnection = acquireConnection = lazy ->
        if free.length != 0
            ret free.pop()
        else
            newConnection
    @releaseConnection = releaseConnection = (conn) -> lazy ->
        free.push conn
        ret null

    @connectionDo = (action) ->
        conn <- bind acquireConnection
        actions = do
            retval <- bind action conn
            _ <- bind releaseConnection conn
            ret retval
        (catchm actions) (e) ->
            _ <- bind releaseConnection conn
            throwm e

    @closeAllConnections = lazy ->
        c = connections
        _ <- bind rec c, (conn) -> conn.disconnect
        free.length = 0
        connections.length = 0
        ret null

    typeCheck this <<< {
        newConnection_type: cont a, e obj!
        acquireConnection_type: cont a, e obj!
        releaseConnnection_type: obj! -> cont a, e nul
        closeAllConnections_type: obj! -> cont a, e nul
        connectionDo_type: (a -> cont a, e nul) arrow cont a, e nul
    }

module.exports = ConnectionManager
