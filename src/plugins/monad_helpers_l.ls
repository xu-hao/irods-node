# traverse objs apply f and generate objt
traverse = (objs, f, getContent, setContent)->
    objto <- bind f objs
    c <- bind getContent objs
    arr <- bind mapM c, (o) -> traverse o, f, getContent, setContent
    setContent objs, objto, arr

ite = (block) ->
    t <- bind block.i
    if t
        block.t
    else
        block.e

fmap = (f) -> (ma) ->
    a <- bind ma
    ret f a

app = (mf, ma) ->
    f <- bind mf
    (fmap f) ma
    
bind2 = (f) -> (ma) ->
    _ <- bind ma
    f

module.exports = {
    traverse, ite, fmap, app, bind2
}

