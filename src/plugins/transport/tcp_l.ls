buffers = require("buffers")
{bind, throwm, ret, runCont, Success, e, cont} = require("../monad_l")
{arrow, a, x, type, str, U, array, num} = require("../type_l")
net = require("net")
log = require("debug")("transport")
apply = (rodsEnv) ->

    pending = null
    bufs = buffers()
    client = null
    connected = false

    # here we may be tempted to call
    #     cont null
    # but this is incorrect because we don't know what
    # the monad transformer may require for the return value of cont
    # for example with ExeceptionalT, cont except
    # either Success or Exception, and this will produce a runtime exception
    # the correct way is
    #     (runCont ret null) cont
    # or
    #     (ret null) cont
    establish = (cont) !->
        log("connect to " + rodsEnv.port + " " + rodsEnv.host)

        client := net.connect rodsEnv.port, rodsEnv.host, ->
            #console.log "connection event"
            dispatch = ->
                log("dispatching curr = " + bufs.length)

                if pending
                    log("pending found")
                    p = pending
                    pending := null
                    p()

            do
                buf <- client.on "data"
                #console.log "data event"
                bufs.push buf
                log("incoming curr = " + bufs.length)
                dispatch()
                log("returning from data handler curr = " + bufs.length)
                connected := true
            (runCont ret null) cont

        client.on "error", (err) ->
            connected := false
            log("error")
            log(JSON.stringify err)
            (runCont throwm {error:-1, errormsg: err, msg: null}) cont

    # wait: int -> M Buffer
    wait = (size) ->
        log("size = " + size)

        (cont) !->
            log("read " + size)
            if pending
                throwm "streamParser: some other task is already pending"
            else
                p = ->
                    if size <= bufs.length
                        slice = bufs.splice(0,size);
                        log("slice = " + slice.toString() + " : " + slice.length + " == " + size + " rem = " + bufs.length)
                        log("continueing with "+cont + " and " + (ret slice))
                        (runCont ret slice) cont
                    else
                        pending := p
                        log("waiting for size = " + size + " curr = " + bufs.length)
                p()

    parser =
        readUInt32: do
            buf <- bind wait 4
            if !buf
                throwm "error: readBuf returns invalid value"
            log(buf)
            ret (buf.toBuffer().readUInt32BE 0, true)
        readBuf: wait `type` num -> cont a, e a
        writeBuf : (buf) -> (cont) !->
            log("write" + buf)
            <- client.write buf
            (runCont (ret null)) cont
        establish : establish
        destroy : (cont) !->
            log "disconnect"
            if connected
                client.destroy()
            (runCont (ret null)) cont
        isConnected: (cont) !->
            (runCont (ret connected)) cont

    parser

module.exports =
    name: "conn-tcp"
    plugin: apply
