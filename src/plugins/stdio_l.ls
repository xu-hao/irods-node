{ret, bind, runCont} = require "./monad_l"

stdout= (msg) -> (cont) ->
    console.log(msg)
    (runCont (ret null)) cont

stderr= (msg) -> (cont) ->
    # console.log "err start"
    console.error(msg)
    # console.log "err start"
    (runCont (ret null)) cont
    
display = (arr, header, cols) ->
    # console.log "display" + arr
    arr2 = [{} for i from 0 til arr.length]
    action = ([elem, i, col]) ->
        val <- bind elem.readProp col
        arr2[i][col] = val
        ret null
    mapM [[elem, i, col] for elem, i in arr
                         for col in cols], action
    display2 arr, header, cols

display2 = (arr2, header, cols) ->
    width = {}
    totalwidth = 0
    for col in cols
        maxlen = col.length
#            console.log arr2
        for elem in arr2
            ec = elem[col]
            if ec? then
                len = (if ec?.name? then ec.name else "#ec").length
                if maxlen < len then
                    maxlen = len
        width[col] = maxlen
    for col in cols
        totalwidth += width[col]

    totalwidth += cols.length - 1
    headerlen = header.length
    if totalwidth < headerlen
        totalwidth = headerlen

    out = ""
    out += "=" * totalwidth + "\n"
    out += header + "\n"

    out += "-" * totalwidth + "\n"
    out += [col + (" " * (width[col] - col.length)) for col in cols].join " "
    out += "\n"

    out += "-" * totalwidth + "\n"

    formatCol = (elem, col) ->
        ec = elem[col]
        if ec?
            str = "#ec"
            str += " " * (width[col] - str.length)
        else
            " " * width[col]
    out += [[formatCol elem, col for col in cols].join " " for elem in arr2].join "\n"
    out += "\n"
    out += "=" * totalwidth + "\n"
    stdout out

module.exports = {stdout, stderr, display, display2}
