{runCont, bind, ret} = require "./plugins/monad_l"
{model} = require "./plugins/model_l"
trans = (require "./plugins/transport/tcp_l").plugin
prot = (require "./plugins/protocol/xml_l").plugin
auth = (require "./plugins/auth/basic-auth_l").plugin
{rodsEnv, rodsCred} = require "./config.js"

mod = model prot, trans, rodsEnv, auth, rodsCred

dataObj = mod.object {
    type: "DataObject"
    ref: "irods:/#{rodsEnv.clientRcatZone}/home/#{rodsEnv.clientUser}/helloworld"
    content: new mod.BufferContent new Buffer "HelloWorld"
}

cont = (x) -> console.log x
(runCont dataObj.create) cont
