DEBUG = false
INFO = false

if DEBUG
    console.log process.argv
    console.log process.argv.slice(2)

module.exports =
    info: (msg) ->
        if INFO
            console.log msg

    log: (msg) ->
        if DEBUG
            console.log msg
