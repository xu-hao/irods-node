start = new Date().getTime()
Error.stackTraceLimit = Infinity

fs = require("fs");
path = require("path");
{bind,bind2,ret,throwm,catchm,runCont} = require("./plugins/monad_l");
{ServerState,MsgType,Negotiation,Keywords} = require("./plugins/const_l");
{rodsCred,rodsEnv} = require("./config.js");
{stdout, stderr} = require("./plugins/stdio_l")
{model} = require "./plugins/model_l"
{info} = require "./debug_l"
{parse} = require("./plugins/cmd_l")

if process.argv.length < 3
    console.error "insufficient number of arguments"
    process.exit()

cmd = process.argv[2]
args = process.argv.slice 3
arg = parse args

apiTable = []
apiNameTable = {}

transportTable = {}
authTable = {}
protocolTable = {}

loadPlugins = (type, nameTable, numTable) ->
    apiPluginsPath = "./plugins/"+type
    info ("looking for plugins: " + apiPluginsPath)
    for file in fs.readdirSync(apiPluginsPath)
        # console.log("found file" + file)
        if file.match(".*[.]ls") && ! file.match("_test_")
            apiPlugin = require(apiPluginsPath+"/"+(file.substring 0, file.length - 3))
            if numTable
                numTable[apiPlugin.num] = apiPlugin.func
            nameTable[apiPlugin.name] = apiPlugin
            # console.dir(apiPlugin)
            info ("found plugin: "+apiPlugin.name)

loadPlugins("api", apiNameTable, apiTable)
loadPlugins("transport", transportTable)
loadPlugins("protocol", protocolTable)
loadPlugins("auth", authTable)

auth = authTable[rodsCred.auth].plugin
trans = transportTable[rodsEnv.transport].plugin
prot = protocolTable[rodsEnv.protocol].plugin
mod = model(prot, trans, rodsEnv, auth, rodsCred)


main = do
    api = apiNameTable[cmd]
    if ! api?
        throwm "error: command #cmd"
    else
        _ <- bind (api.playbook mod) arg
        mod.cleanup
errhdlr = (msg) -> do
    _ <- bind stderr msg
    mod.cleanup

init = new Date().getTime()
console.log "init time " + (init - start) / 1000 + "s"
(runCont (catchm main) errhdlr) (x) ->
    finish = new Date().getTime()
    console.log "run time " + (finish - init) / 1000 + "s"
    info ("finalize")

/*
 stateHdlr : str -> cont a, e str


ServerStateHandler = {}

ServerStateHandler[ServerState.CONNECT] = do
    info ("initialize")
    <- bind connect
    info ("connected")
    ret ServerState.STARTUP

ServerStateHandler[ServerState.STARTUP] = do
    <- bind sendStartupPack
    ret ServerState.NEGOTIATE

ServerStateHandler[ServerState.NEGOTIATE] = do
    <- bind negotiate
    ret ServerState.REQUEST

ServerStateHandler[ServerState.REQUEST] = do
    _ <- bind auth.authenticate prot, rodsCred, rodsEnv
    api = apiNameTable[cmd]
    retVal <- bind ((api.playbook mod).apply null, args)
    # console.dir(retVal)
    ret ServerState.DISCONNECT

ServerStateHandler[ServerState.DISCONNECT] = do
    <- bind disconnect
    ret ServerState.EXIT

ServerStateHandler[ServerState.EXIT] =
    # console.log("exit")
    ret null

stateHdlr = (state) ->
    info ("entering state " + state)
    hdlr = ServerStateHandler[state]
    if !hdlr
        throwm "error: state handler for " + state + " not found!"
    else
        hdlr

errorHdlr = (msg) -> do
    <- bind stderr msg
    ServerStateHandler[ServerState.DISCONNECT]

serverLoop = (state) ->
    (catchm (
        newState <- bind stateHdlr state
        if newState != ServerState.EXIT
            serverLoop newState
        else
            ret ServerState.EXIT
    )) errorHdlr

(serverLoop ServerState.CONNECT) (x) -> info ("finalize")
*/
