/**
 * New node file
 */
module.exports = {
	rodsEnv : {
		port:1247,
		host:"127.0.0.1",
		clientUser:"rods",
		clientRcatZone:"tempZone",
		proxyUser:"rods",
		proxyRcatZone:"tempZone",
		icat: {
			host: "127.0.0.1",
			port: "1247"
		},
		transport:"conn-tcp",
		protocol:"prot-xml",
		defaultResc:"demoResc"
	},
	rodsCred : {
		auth: "basic_auth",
		password:"rods"
	}
};
